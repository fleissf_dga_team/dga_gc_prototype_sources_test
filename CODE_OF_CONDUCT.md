# FLEISS Software Foundation

## Code of Conduct

### Introduction

The FLEISS Software Foundation establishes this Code of Conduct to provide a set of basic rules regulating human relations between people participating in any form in the Foundation's projects and initiatives. The FLEISS Software Foundation is strongly committed to the concept of "peopleware" as it was formulated by Tom DeMarco and Timothy Lister which defined that people working in information technologies make up altogether an essential factor equipollent to hardware or software. Furthermore, the FLEISS Software Foundation affirms that peopleware is the most essential factor of information technologies; finally, hardware and software are made by people, people make hardware and software work, and hardware and software shall work for people.

On the other side, the mission of the FLEISS Software Foundation is to advance development of free and open-source software of enterprise quality and to extend its usage in everyday life. In other words, we proclaim that we consider quality of free and open-source software as the key measure of its practical usefulness. Development of enterprise software assumes application of special practices assuring quality of results. Not all enterprise practices are efficient, some of them are not even effective, however, some of them are quite rational albeit specific. In this sense the development of software of enterprise quality is similar to the Linux kernel development effort that is a very personal process compared to "traditional" ways of developing software. It means that your code and ideas behind it will be carefully reviewed, often resulting in critique and criticism. The review will almost always require improvements to the code before it can be included in the release. You should know that this happens because everyone involved in wants to see the best possible solution for the overall result.

The FLEISS Software Foundation welcomes and encourages participation by everyone. We are committed to being a community that everyone feels good about joining. Although we may not be able to satisfy everyone, we will always work to treat everyone well.

### Scope

This Code of Conduct applies to all activities managed by the FLEISS Software Foundation, including public and private mailing lists, issue trackers, wikis, blogs, and any other communications used by the Foundation's communities.

We expect this Code of Conduct to be honored by everyone who participates in the FLEISS community formally or informally, or claims any affiliation with the Foundation, in any Foundation-related activities and especially when representing the FLEISS Software Foundation, in any role.

This Code of Conduct is not exhaustive or complete. It serves to distill our common understanding of a collaborative, shared environment and goals. We expect it to be followed in spirit as much as in the letter, so that it can enrich all of us and the technical communities in which we participate.

### Guidelines

We do all our best to:

* **Be open.** We invite anyone to participate in our community. We preferably use public methods of communication for project-related messages, unless discussing something sensitive. This applies to messages for help or project-related support, too; not only is a public support request much more likely to result in an answer to a question, it also makes sure that any inadvertent mistakes made by people answering will be more easily detected and corrected.

* **Be empathetic, welcoming, friendly, and patient.** We work together to resolve conflict, assume good intentions, and bend all our efforts to act in an empathetic fashion. We may all experience some frustration from time to time, but we do not allow frustration to turn into a personal attack. A community where people feel uncomfortable or threatened is not a productive one. We should be respectful when dealing with other community members as well as with people outside our community.

* **Be collaborative.** Our work will be used by other people, and in turn we will depend on the work of others. When we make something for the benefit of the project, we are willing to explain to others how it works, so that they can build on the work to make it even better. Any decision we make will affect users and colleagues, and we take those consequences seriously when making decisions.

* **Be inquisitive.** Nobody knows everything! Asking questions early avoids many problems later, so questions are encouraged, though they may be directed to the appropriate forum. Those who are asked should be responsive and helpful, within the context of our shared goal of improving the Foundation's project code.

* **Be careful in the words that we choose.** Whether we are participating as professionals or volunteers, we value professionalism in all interactions, and take responsibility for our own speech. Be kind to others. Do not insult or put down other participants. Harassment and other exclusionary behaviour are not acceptable. This includes, but is not limited to:
    * Violent threats or language directed against another person.
    * Sexist, racist, or otherwise discriminatory jokes and language.
    * Posting sexually explicit or violent material.
    * Posting (or threatening to post) other people's personally identifying information ("doxing").
    * Sharing private content, such as emails sent privately or non-publicly, or unlogged forums such as chat channel history.
    * Personal insults, especially those using racist or sexist terms.
    * Unwelcome sexual attention.
    * Excessive or unnecessary profanity.
    * Repeated harassment of others. In general, if someone asks you to stop, then stop.
    * Advocating for, or encouraging, any of the above behaviour.

* **Be concise.** Keep in mind that what you write once will be read by hundreds of persons. Writing a short email means people can understand the conversation as efficiently as possible. Short emails should always strive to be empathetic, welcoming, friendly and patient. When a long explanation is necessary, consider adding a summary.

* **Be positive.** Try to bring new ideas to a conversation so that each mail adds something unique to the thread, keeping in mind that the rest of the thread still contains the other messages with arguments that have already been made.

* **Be accurate.** Try to stay on topic, especially in discussions that are already fairly large.

* **Step down considerately.** Members of every project come and go. When somebody leaves or disengages from the project they should tell people they are leaving and take the proper steps to ensure that others can pick up where they left off. In doing so, they should remain respectful of those who continue to participate in the project and should not misrepresent the project's goals or achievements. Likewise, community members should respect any individual's choice to leave the project.

### Reporting

While this code of conduct should be adhered to by participants, we recognize that sometimes people may have a bad day, or be unaware of some of the guidelines in this code of conduct. When that happens, you may reply to them and point out this code of conduct. Such messages may be in public or in private, whatever is most appropriate. However, regardless of whether the message is public or not, it should still adhere to the relevant parts of this code of conduct; in particular, it should not be abusive or disrespectful.

If you believe someone is violating this code of conduct, you may reply to them and point out this code of conduct. Such messages may be in public or in private, whatever is most appropriate. Assume good faith; it is more likely that participants are unaware of their bad behaviour than that they intentionally try to degrade the quality of the discussion. Should there be difficulties in dealing with the situation, you may report your compliance issues in confidence at the Foundation (fleissf _at_ fleissf.org).

If the violation is in documentation or code, for example inappropriate pronoun usage or word choice within official documentation, we ask that people report these privately to the project leaders, and, if they have sufficient ability within the project, to resolve or remove the concerning material, being mindful of the perspective of the person originally reporting the issue.

### References

This statement thanks the following, on which it draws for content and inspiration:  
* [The Apache Software Foundation's Code of Conduct](https://www.apache.org/foundation/policies/conduct.html)  
* [The Linux Foundation's Code of Conduct](https://events.linuxfoundation.org/code-of-conduct/)  
* [The Linux Kernel's Code of Conflict](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=b0bc65729070b9cbdbb53ff042984a3c545a0e34)  
* [Django Code of Conduct](https://www.djangoproject.com/conduct/)  
