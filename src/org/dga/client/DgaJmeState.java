/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client;

import com.jme3.app.Application;
import com.jme3.app.state.AppState;
import com.jme3.app.state.AppStateManager;

/**
 * The interface of the application state used within the game client.
 *
 * @extends DgaJmeSpatial
 * @extends com.jme3.app.state.AppState
 * @pattern Adapter
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 11.5.2018
 */
public interface DgaJmeState extends AppState {
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method is called by {@link AppStateManager} when transitioning this
     * {@code AppState} from <i>initializing</i> to <i>running</i>.<br>
     * This will happen on the next iteration through the update loop after
     * {@link AppStateManager#attach(com.jme3.app.state.AppState) } was called.
     * <p>
     * The <code>AppStateManager</code> will call this only from the update loop
     * inside the rendering thread. This means it is safe to modify the scene 
     * graph from this method.
     *
     * @param stateManager The state manager.
     * @param app The application.
     */
    @Override
    public void initialize(final AppStateManager stateManager, final Application app);
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
