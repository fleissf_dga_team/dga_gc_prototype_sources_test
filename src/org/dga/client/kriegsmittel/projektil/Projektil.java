/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsmittel.projektil;

import org.dga.client.kraftwerk.Schaden;
import org.dga.client.kriegsmittel.controller.KriegsmittelPhysicsControl;
import org.dga.client.kriegsmittel.Munition;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.PhysicsTickListener;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;
import java.io.IOException;
import org.dga.client.effekt.ExplosionEffect;

/**
 * The interface of a projectile (arrow, shell, bullet, rocket, etc).
 * 
 * @extends Kriegsmittel
 * @extends KriegsmittelPhysicsControl
 * @extends com.jme3.bullet.collision.PhysicsCollisionListener
 * @extends com.jme3.bullet.PhysicsTickListener
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 24.4.2018
 */
public interface Projektil extends Munition, KriegsmittelPhysicsControl, 
    PhysicsCollisionListener, PhysicsTickListener {
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the effects provided by the projectile in time of 
     * impact on the obstacle.
     * 
     * @return ExplosionEffect The effects provided by the projectile in time of 
     * impact on the obstacle.
     */
    public ExplosionEffect getExplosionEffect();
    
    /**
     * The method returns the effect radius of the projectile.
     * 
     * @return float The effect radius of the projectile.
     */
    public float getEffectRadius();

    /**
     * The method sets the effect radius of the projectile.
     * 
     * @param effectRadius The effect radius of the projectile.
     */
    public void setEffectRadius(final float effectRadius);
    
    /**
     * The method returns the explosion radius of the projectile.
     * 
     * @return float The explosion radius of the projectile.
     */
    public float getExplosionRadius();

    /**
     * The method sets the explosion radius of the projectile.
     * 
     * @param explosionRadius The explosion radius of the projectile.
     */
    public void setExplosionRadius(final float explosionRadius);

    /**
     * The method returns the emitter radius of the projectile.
     * 
     * @return float The emitter radius of the projectile.
     */
    public float getEmitterRadius();

    /**
     * The method sets the emitter radius of the projectile.
     * 
     * @param emitterRadius The emitter radius of the projectile.
     */
    public void setEmitterRadius(final float emitterRadius);
    
    /**
     * The method returns the force factor of the projectile.
     * 
     * @return float The force factor of the projectile.
     */
    public float getForceFactor();

    /**
     * The method sets the force factor of the projectile.
     * 
     * @param forceFactor The force factor of the projectile.
     */
    public void setForceFactor(final float forceFactor);
    
    /**
     * The method returns the damage which the projectile can apply to an armored 
     * weapon and by that diminish its fighting capacity.
     * 
     * @return Panzerungschaden The damage which the projectile can apply to an 
     * armored weapon and by that diminish its fighting capacity.
     */
    public Schaden getSchaden();
    
    /**
     * The method returns the projectile's velocity.
     * 
     * @return float The projectile's velocity.
     */
    public float getVelocity();
    
    /**
     * The method returns the maximum time of visibility in seconds for a 
     * projectile flying out of the battlefield.
     * 
     * @return float The maximum time of visibility in seconds for a projectile 
     * flying out of the battlefield.
     */
    public float getFlyingOutMaxTime();
    
    /**
     * The method returns the maximum time in seconds the effect works out.
     * 
     * @return float The maximum time in seconds the effect works out.
     */
    public float getEffectMaxTime();
    //
    // *************************************************************************
    //
    /**
     * The method is called when a collision happened in the physics space, and 
     * <i>called from the render thread</i>. Do not store the event object as it 
     * will be cleared after the method has finished.
     * 
     * If you do not implement the Collision Listener interface 
     * (com.jme3.bullet.collision.PhysicsCollisionListener), a collisions will 
     * just mean that physical forces between solid objects are applied 
     * automatically. If you just want “balls rolling, bricks falling" you do not 
     * need a listener. If however you want to respond to a collision event 
     * (com.jme3.bullet.collision.PhysicsCollisionEvent) with a custom action, 
     * then you need to implement the PhysicsCollisionListener interface.
     * Typical actions triggered by collisions include:
     * 1) Increasing a counter (e.g. score points);
     * 2) Decreasing a counter (e.g. health points);
     * 3) Triggering an effect (e.g. explosion);
     * 4) Playing a sound (e.g. explosion, ouch) etc.
     * 
     * @param event The collision event.
     */
    @Override
    public void collision(final PhysicsCollisionEvent event);
    
    /**
     * The method is called before the physics is actually stepped, use to apply 
     * forces etc.
     * 
     * The jBullet Physics implementation is stepped at a constant 60 physics 
     * ticks per second frame rate. Applying forces or checking for overlaps 
     * only has an effect right at a physics update cycle, which is not every 
     * frame. If you do physics interactions at arbitrary spots in the 
     * simpleUpdate() loop, calls will be dropped at irregular intervals, 
     * because they happen out of cycle. This method is called before each step, 
     * and here you can apply forces (change the state).
     * 
     * @param space The physics space.
     * @param tpf The time per frame in seconds.
     */
    @Override
    public void prePhysicsTick(final PhysicsSpace space, float tpf);
    
    /**
     * The method is called after the physics has been stepped, and can be used 
     * to check for forces etc.
     * 
     * The jBullet Physics implementation is stepped at a constant 60 physics 
     * ticks per second frame rate. Applying forces or checking for overlaps 
     * only has an effect right at a physics update cycle, which is not every 
     * frame. If you do physics interactions at arbitrary spots in the 
     * simpleUpdate() loop, calls will be dropped at irregular intervals, 
     * because they happen out of cycle. This method is called after each step, 
     * and here you can poll the results (get the current state).
     * 
     * @param space The physics space.
     * @param tpf The time per frame in seconds.
     */
    @Override
    public void physicsTick(final PhysicsSpace space, final float tpf);
    //
    // *************************************************************************
    //
    /**
     * The method updates the control. This should not be called from the user code.
     *
     * @param tpf Time per frame.
     */
    @Override
    public void update(final float tpf);
    
    /**
     * The method reads the object from the importer.
     * 
     * @param im The importer.
     * 
     * @throws IOException If an input/output exceptions has occurred.
     */
    @Override
    public void read(final JmeImporter im) throws IOException;
    
    /**
     * The method writes the object to the exporter.
     * 
     * @param ex The exporter.
     * 
     * @throws IOException If an input/output exceptions has occurred.
     */
    @Override
    public void write(final JmeExporter ex) throws IOException;
    
    /**
     * The method creates a clone of the control, the given spatial is the
     * cloned version of the spatial to which this control is attached to.
     *
     * @param spatial The spatial.
     *
     * @return Control A clone of this control for the spatial.
     */
    @Override
    public Control cloneForSpatial(final Spatial spatial);
    
    /**
     * The method performs a regular shallow clone of the object. Some fields
     * may also be cloned but generally only if they will never be shared with
     * other objects. (For example, local Vector3fs and so on.)
     * <p>
     * <p>
     * This method is separate from the regular clone() method so that objects
     * might still maintain their own regular java clone() semantics (perhaps
     * even using Cloner for those methods). However, because Java's clone() has
     * specific features in the sense of Object's clone() implementation, it's
     * usually best to have some path for subclasses to bypass the public
     * clone() method that might be cloning fields and instead get at the
     * superclass protected clone() methods. For example, through
     * super.jmeClone() or another protected clone method that some base class
     * eventually calls super.clone() in.</p>
     *
     * @return Object The clone of the object.
     */
    @Override
    public Object jmeClone();
    //
    // *************************************************************************
    //    
    /**
     * The method returns the physics space of this control.
     *
     * @return PhysicsSpace The physics space of this control.
     */
    @Override
    public PhysicsSpace getPhysicsSpace();

    /**
     * The method sets the physics space for this control. The method is only
     * used internally, should not be called from user code.
     *
     * @param space The physics space for this control.
     */
    @Override
    public void setPhysicsSpace(final PhysicsSpace space);
    
    /**
     * The method sets the linear velocity for this control.
     * 
     * @param vec The linear velocity to be set for this control.
     */
    public void setLinearVelocity(final Vector3f vec);
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
