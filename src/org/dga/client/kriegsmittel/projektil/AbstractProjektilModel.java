/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsmittel.projektil;

import com.jme3.asset.AssetManager;
import org.dga.client.DgaJmeUtils;
import org.dga.client.kombattant.Kombattant;
import org.dga.client.kriegsmittel.AbstractKriegsmittelModel;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.scene.Spatial;
import org.dga.client.DgaJmeScale;

/**
 * The abstract implementation of a model for a projectile.
 * 
 * @extends AbstractKriegsmittelModel
 * @implements ProjektilModel
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 23.07.2018
 */
public abstract class AbstractProjektilModel extends AbstractKriegsmittelModel 
    implements ProjektilModel {
    private AssetManager assetManager_ = null;
    private float projektilMass_ = 0f;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new model with the supplied properties.
     *
     * @param assetManager The game application's asset manager.
     * @param kombattant The combat participator the projectile is created for.
     * @param projektilSpatial The projectile's spatial.
     * @param projektilScale The projectile's scale.
     * @param projektilShape The projectile's collision shape.
     * @param projektilMass The projectile's mass.
     */
    public AbstractProjektilModel(final AssetManager assetManager, 
        final Kombattant kombattant, final Spatial projektilSpatial, 
        final DgaJmeScale projektilScale, final CollisionShape projektilShape, 
        final float projektilMass) {
        super(kombattant, projektilSpatial, projektilScale, projektilShape);
        assetManager_ = assetManager;
        projektilMass_ = projektilMass;
    }
    
    /**
     * The constructor creates a new model with the supplied properties.
     *
     * @param assetManager The game application's asset manager.
     * @param kombattant The combat participator the projectile is created for.
     * @param projektilSpatial The projectile's spatial.
     * @param projektilScale The projectile's scale.
     * @param projektilMass The projectile's mass.
     */
    public AbstractProjektilModel(final AssetManager assetManager, 
        final Kombattant kombattant, final Spatial projektilSpatial, 
        final DgaJmeScale projektilScale, final float projektilMass) {
        super(kombattant, projektilSpatial, projektilScale, 
            DgaJmeUtils.createBoxCollisionShape(projektilSpatial));
        assetManager_ = assetManager;
        projektilMass_ = projektilMass;
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the game application's asset manager.
     * 
     * @return AssetManager The game application's asset manager.
     */
    @Override
    public AssetManager getAssetManager() {
        return assetManager_;
    }
    
    /**
     * The method returns the projectile's mass in kilograms.
     * 
     * @return float The projectile's mass in kilograms.
     */
    @Override
    public float getMass() {
        return projektilMass_;
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}