/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsmittel.projektil;

import org.dga.client.DefaultDgaJmeScale;
import org.dga.client.kriegsmittel.controller.panzerwagen.PanzerwanneControl;
import org.dga.client.kriegsmittel.controller.panzerwagen.TurmControl;
import org.dga.client.kriegsmittel.controller.AbstractKriegsmittelPhysicsControl;
import org.dga.client.kriegsmittel.panzerung.PanzerungControl;
import org.dga.client.kriegsmittel.panzerung.PanzerungModel;
import org.dga.client.kraftwerk.DefaultSchaden;
import org.dga.client.kraftwerk.Schaden;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionObject;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.objects.PhysicsGhostObject;
import com.jme3.effect.ParticleEmitter;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import org.dga.client.effekt.ExplosionEffect;
import org.dga.client.DgaJmeScale;

/**
 * The abstract implementation of a projectile shot from a firearm, e.g. shells, 
 * bullets, rockets, etc.
 *
 * @extends AbstractKriegsmittelPhysicsControl
 * @implements Feuerprojektil
 * @implements com.jme3.bullet.control.PhysicsControl
 * @implements com.jme3.util.clone.JmeCloneable
 * @implements com.jme3.bullet.collision.PhysicsCollisionListener
 * @implements com.jme3.bullet.PhysicsTickListener
 * @pattern Adapter
 * @pattern Template Method
 * @pattern Inversion of Control (IoC)
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 3.7.2018
 */
public abstract class AbstractFeuerprojektil extends AbstractKriegsmittelPhysicsControl 
    implements Feuerprojektil {
    private FeuerprojektilModel modelProjektil_ = null;
    private AssetManager assetManager_ = null;
    private CollisionShape cshProjektil_ = null;
    private Spatial sptlProjektil_ = null;
    private Node nodeParent_ = null;
    private float projektilMass_ = 0f;
    private PhysicsSpace physicsSpace_ = null;
    private PhysicsGhostObject ghostObject_ = null;
    private ExplosionEffect exEffect_ = null;
    private final Vector3f VEC1_ = new Vector3f();
    private final Vector3f VEC2_ = new Vector3f();
    private float effectMaxTime_ = 0.5f;
    private float flyOutMaxTime_ = 4.0f;
    private float curTime_ = -1.0f;
    private float timer_ = 0f;
    private DgaJmeScale scaleProjektil_ = DefaultDgaJmeScale.DEFAULT_PROJEKTIL_SCALE;

    private static long projektilId_ = 0; // TEMP CODE
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new control with the supplied properties.
     * 
     * @param projektilModel The projectile's model.
     */
    public AbstractFeuerprojektil(final FeuerprojektilModel projektilModel) {
        super(projektilModel);
        modelProjektil_ = projektilModel;
        assetManager_ = modelProjektil_.getAssetManager();
        cshProjektil_ = modelProjektil_.getCollisionShape();
        sptlProjektil_ = modelProjektil_.getSpatial();
        projektilMass_ = modelProjektil_.getMass();
        // The jMonkeyEngine v.3.2 has a strange bug with scaling that appears
        // as scaling is applying twice for spatials however only once for 
        // collision shapes. Because of this bug the following method call is 
        // used at the moment in place of the call in the superclass.
        scaleProjektil_ = modelProjektil_.getScale();
        this.applyScale();
        
        projektilId_++; // TEMP CODE
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the effects provided by the projectile in time of 
     * impact on the obstacle.
     * 
     * @return ExplosionEffect The effects provided by the projectile in time of 
     * impact on the obstacle.
     */
    @Override
    public abstract ExplosionEffect getExplosionEffect();
    
    /**
     * The method returns the effect radius of the projectile.
     * 
     * @return float The effect radius of the projectile.
     */
    @Override
    public abstract float getEffectRadius();

    /**
     * The method sets the effect radius of the projectile.
     * 
     * @param effectRadius The effect radius of the projectile.
     */
    @Override
    public abstract void setEffectRadius(final float effectRadius);
    
    /**
     * The method returns the explosion radius of the projectile.
     * 
     * @return float The explosion radius of the projectile.
     */
    @Override
    public abstract float getExplosionRadius();

    /**
     * The method sets the explosion radius of the projectile.
     * 
     * @param explosionRadius The explosion radius of the projectile.
     */
    @Override
    public abstract void setExplosionRadius(final float explosionRadius);

    /**
     * The method returns the emitter radius of the projectile.
     * 
     * @return float The emitter radius of the projectile.
     */
    @Override
    public abstract float getEmitterRadius();

    /**
     * The method sets the emitter radius of the projectile.
     * 
     * @param emitterRadius The emitter radius of the projectile.
     */
    @Override
    public abstract void setEmitterRadius(final float emitterRadius);
    
    /**
     * The method returns the force factor of the projectile.
     * 
     * @return float The force factor of the projectile.
     */
    @Override
    public abstract float getForceFactor();

    /**
     * The method sets the force factor of the projectile.
     * 
     * @param forceFactor The force factor of the projectile.
     */
    @Override
    public abstract void setForceFactor(final float forceFactor);
    
    /**
     * The method returns the damage which the projectile can apply to an armored 
     * weapon and by that diminish its fighting capacity.
     * 
     * @return Panzerungschaden The damage which the projectile can apply to an 
     * armored weapon and by that diminish its fighting capacity.
     */
    @Override
    public abstract Schaden getSchaden();
    
    /**
     * The method returns the projectile's velocity.
     * 
     * @return float The projectile's velocity.
     */
    @Override
    public abstract float getVelocity();
    
    /**
     * The method returns the maximum time of visibility in seconds for a 
     * projectile flying out of the battlefield.
     * 
     * @return float The maximum time of visibility in seconds for a projectile 
     * flying out of the battlefield.
     */
    @Override
    public abstract float getFlyingOutMaxTime();
    
    /**
     * The method returns the maximum time in seconds the effect works out.
     * 
     * @return float The maximum time in seconds the effect works out.
     */
    @Override
    public abstract float getEffectMaxTime();
    //
    // *************************************************************************
    //
    /**
     * The method is called when a collision happened in the physics space, and 
     * <i>called from the render thread</i>. Do not store the event object as it 
     * will be cleared after the method has finished.
     * 
     * If you do not implement the Collision Listener interface 
     * (com.jme3.bullet.collision.PhysicsCollisionListener), a collisions will 
     * just mean that physical forces between solid objects are applied 
     * automatically. If you just want “balls rolling, bricks falling" you do not 
     * need a listener. If however you want to respond to a collision event 
     * (com.jme3.bullet.collision.PhysicsCollisionEvent) with a custom action, 
     * then you need to implement the PhysicsCollisionListener interface.
     * Typical actions triggered by collisions include:
     * 1) Increasing a counter (e.g. score points);
     * 2) Decreasing a counter (e.g. health points);
     * 3) Triggering an effect (e.g. explosion);
     * 4) Playing a sound (e.g. explosion, ouch) etc.
     * 
     * @param event The collision event.
     */
    @Override
    public void collision(final PhysicsCollisionEvent event) {
        if (this.getPhysicsSpace() == null) {
            return;
        }
        if (event != null) {
            if (event.getObjectA() == this || event.getObjectB() == this) {
                physicsSpace_ = this.getPhysicsSpace();
                exEffect_ = this.getExplosionEffect();
                ghostObject_ = exEffect_.getExplosionObject();
                physicsSpace_.add(ghostObject_);
                ghostObject_.setPhysicsLocation(this.getPhysicsLocation(VEC1_));
                physicsSpace_.addTickListener(this);
                nodeParent_ = sptlProjektil_.getParent();
                if (exEffect_ != null && nodeParent_ != null) {
                    curTime_ = 0f;
                    Vector3f lt_ = sptlProjektil_.getLocalTranslation();
                    for (ParticleEmitter pe_ : exEffect_.getVisualEffectEmitList()) {
                        pe_.setLocalTranslation(lt_);
                        nodeParent_.attachChild(pe_);
                        pe_.emitAllParticles();
                    }
                }
                physicsSpace_.remove(this);
                sptlProjektil_.removeFromParent();
            }
            
//            Spatial nodeA_ = event.getNodeA();
//            Spatial nodeB_ = event.getNodeB();
//            if (nodeA_ != null || nodeB_ != null) {
//                String nameA_ = (nodeA_ != null ? nodeA_.getName() : "");
//                String nameB_ = (nodeB_ != null ? nodeB_.getName() : "");
//                System.out.println(granateId_ + ": nameA: " + nameA_);
//                System.out.println(granateId_ + ": nameB: " + nameB_);
//            }

//            System.out.println("------------------------------");
//            System.out.println("event.getNodeA() == " + 
//                (event.getNodeA() != null ? ((Spatial) event.getNodeA()).getName() : "null"));
//            System.out.println("event.getNodeB() == " + 
//                (event.getNodeB() != null ? ((Spatial) event.getNodeA()).getName() : "null"));

//            space_ = this.getPhysicsSpace();
//            ghostObject_ = this.getCollisionObject();
//            effect_ = this.getEffect();
//            if (event.getObjectA() == this || event.getObjectB() == this) {
//                space_.add(ghostObject_);
//                ghostObject_.setPhysicsLocation(this.getPhysicsLocation(VEC1_));
//                space_.addTickListener(this);
//                if (effect_ != null && spatialProjektil_.getParent() != null) {
//                    curTime_ = 0f;
//                    effect_.setLocalTranslation(spatialProjektil_.getLocalTranslation());
//                    spatialProjektil_.getParent().attachChild(effect_);
//                    effect_.emitAllParticles();
//                }
//                space_.remove(this);
//                spatialProjektil_.removeFromParent();
//            }
        }
    }

    /**
     * The method is called before the physics is actually stepped, use to apply 
     * forces etc.
     * 
     * The jBullet Physics implementation is stepped at a constant 60 physics 
     * ticks per second frame rate. Applying forces or checking for overlaps 
     * only has an effect right at a physics update cycle, which is not every 
     * frame. If you do physics interactions at arbitrary spots in the 
     * simpleUpdate() loop, calls will be dropped at irregular intervals, 
     * because they happen out of cycle. This method is called before each step, 
     * and here you can apply forces (change the state).
     * 
     * @param space The physics space.
     * @param tpf The time per frame in seconds.
     */
    @Override
    public void prePhysicsTick(final PhysicsSpace space, float tpf) {
        space.removeCollisionListener(this);
    }

    /**
     * The method is called after the physics has been stepped, and can be used 
     * to check for forces etc.
     * 
     * The jBullet Physics implementation is stepped at a constant 60 physics 
     * ticks per second frame rate. Applying forces or checking for overlaps 
     * only has an effect right at a physics update cycle, which is not every 
     * frame. If you do physics interactions at arbitrary spots in the 
     * simpleUpdate() loop, calls will be dropped at irregular intervals, 
     * because they happen out of cycle. This method is called after each step, 
     * and here you can poll the results (get the current state).
     * 
     * @param space The physics space.
     * @param tpf The time per frame in seconds.
     */
    @Override
    public void physicsTick(final PhysicsSpace space, final float tpf) {
        // Get all overlapping objects and apply impulse to them.
        for (PhysicsCollisionObject physicsCollisionObject_ : ghostObject_.getOverlappingObjects()) {

//            PhysicsRigidBody rBody_ = (PhysicsRigidBody) physicsCollisionObject_;
//            rBody_.getPhysicsLocation(VEC2_);
//            VEC2_.subtractLocal(VEC1_);
//            float force_ = explosionRadius_ - VEC2_.length();
//            force_ *= forceFactor_;
//            force_ = force_ > 0 ? force_ : 0;
//            VEC2_.normalizeLocal();
//            VEC2_.multLocal(force_);
//            ((PhysicsRigidBody) physicsCollisionObject_).applyImpulse(VEC2_, Vector3f.ZERO);

//            BoundingBox bbGranate_ = (BoundingBox) spatialGranate_.getWorldBound();
//            BoundingBox bbPanzerung_ = (BoundingBox) spatialPanzerung_.getWorldBound();

//            System.out.println("ColObject: " + physicsCollisionObject_.getCollisionShape().getObjectId());

//            if (physicsCollisionObject_ instanceof PanzerungControl) {
//                //
//                //System.out.println(granateId_ + ": granate.isApplyPhysicsLocal(): " + this.isApplyPhysicsLocal());
//                //
//                boolean isFront_ = false;
//                boolean isRear_ = false;
//                boolean isLeft_ = false;
//                boolean isRight_ = false;
//                boolean isTop_ = false;
//                boolean isBottom_ = false;
//                float offsetX_ = 0f;
//                float offsetY_ = 0f;
//                float offsetZ_ = 0f;
//                float grX_ = 0f;
//                float grY_ = 0f;
//                float grZ_ = 0f;
//                float vpX_ = 0f;
//                float vpY_ = 0f;
//                float ctZ_ = 0f;
//                Vector3f grTrans_ = sptlProjektil_.getWorldTranslation();
//                PanzerungControl pc_ = (PanzerungControl) physicsCollisionObject_;
//                //
//                Spatial pzSpatial_ = pc_.getSpatial();
//                Vector3f pzTrans_ = pzSpatial_.getWorldTranslation();
//                //
//                //System.out.println(granateId_ + ": granateTranlsation: " + grTrans_.toString());
//                //System.out.println(granateId_ + ": panzerungTranslation: " + pzTrans_.toString());
//                System.out.println(projektilId_ + ": panzerungClass: " + pc_.getClass().getSimpleName());
//                //
//                //
//                PanzerungModel pm_ = pc_.getModel();
//                float schaden_ = 0f;
//                if (physicsCollisionObject_ instanceof PanzerwanneControl || 
//                    physicsCollisionObject_ instanceof TurmControl) {
//                    if (pm_.hasVorpunkt() == true && pm_.hasMeasures() == true) {
//                        Spatial vp_ = pm_.getVorpunkt();
//                        Vector3f vorpunktTrans_ = vp_.getWorldTranslation();
//                        //
//                        //System.out.println(granateId_ + ": vorpunktTranslation: " + vorpunktTrans_.toString());
//                        //
//                        Vector3f centerTrans_ = pm_.getCenter();
//                        //
//                        //System.out.println(granateId_ + ": centerTranslation: " + centerTrans_.toString());
//                        //System.out.println(granateId_ + ": length: " + pm_.getLength());
//                        //System.out.println(granateId_ + ": width: " + pm_.getWidth());
//                        //System.out.println(granateId_ + ": height: " + pm_.getHeight());
//                        //System.out.println(granateId_ + ": half-length: " + pm_.getHalfLength());
//                        //System.out.println(granateId_ + ": half-width: " + pm_.getHalfWidth());
//                        //System.out.println(granateId_ + ": half-height: " + pm_.getHalfHeight());
//                        //System.out.println("------------------------------");
//                        //
//                        grX_ = grTrans_.getX();
//                        grY_ = grTrans_.getY();
//                        grZ_ = grTrans_.getZ();
//                        vpX_ = vorpunktTrans_.getX();
//                        vpY_ = vorpunktTrans_.getY();
//                        ctZ_ = centerTrans_.getZ();
//                        //offsetX_ = grTrans_.getX() - vorpunktTrans_.getX();
//                        //offsetY_ = grTrans_.getY() - vorpunktTrans_.getY();
//                        //offsetZ_ = grTrans_.getZ() - centerTrans_.getZ();
//                        offsetX_ = grX_ < 0 && vpX_ < 0 ? grX_ - vpX_ : grX_ - vpX_;
//                        offsetY_ = grY_ < 0 && vpY_ < 0 ? grY_ - vpY_ : grY_ - vpY_;
//                        offsetZ_ = ctZ_ < 0 ? grZ_ + ctZ_ : grZ_ - ctZ_;
//                        //
//                        //System.out.println(granateId_ + ": offsetX: " + offsetX_);
//                        //System.out.println(granateId_ + ": offsetY: " + offsetY_);
//                        //System.out.println(granateId_ + ": offsetZ: " + offsetZ_);
//                        //System.out.println("------------------------------");
//                        //
//                        if (offsetX_ <= 0) { // Right
//                            isRight_ = true;
//                        } else if (offsetX_ > 0) { // Left
//                            isLeft_ = true;
//                        }
//                        if (offsetZ_ <= 0) { // Front
//                            isFront_ = true;
//                        } else if (offsetZ_ > 0) { // Rear
//                            isRear_ = true;
//                        }
//                        if (offsetY_ <= 0) { // Bottom
//                            isBottom_ = true;
//                        } else if (offsetY_ > 0) { // Top
//                            isTop_ = true;
//                        }
//                        if (isRight_ == true) {
//                            if (isFront_ == true) {
//                                if (isTop_ == true) {
//                                    offsetY_ = centerTrans_.getY() + pm_.getHalfHeight() - grTrans_.getY();
//                                    offsetX_ = centerTrans_.getX() + pm_.getHalfWidth() - grTrans_.getX();
//                                    if (FastMath.abs(offsetY_) == 0f) { // Right-Front-Top
//                                        System.out.println(projektilId_ + ": Penetration: Right-Front-Top");
//                                    } else if (FastMath.abs(offsetX_) == 0f) { // Right-Front-Side-Top
//                                        System.out.println(projektilId_ + ": Penetration: Right-Front-Side-Top");
//                                    } else { // Right-Front-Top-Front
//                                        System.out.println(projektilId_ + ": Penetration: Right-Front-Top-Front");
//                                    }
//                                } else if (isBottom_ == true) {
//                                    offsetY_ = centerTrans_.getY() - pm_.getHalfHeight() - grTrans_.getY();
//                                    offsetX_ = centerTrans_.getX() + pm_.getHalfWidth() - grTrans_.getX();
//                                    if (FastMath.abs(offsetY_) == 0f) { // Right-Front-Bottom
//                                        System.out.println(projektilId_ + ": Penetration: Right-Front-Bottom");
//                                    } else if (FastMath.abs(offsetX_) == 0f) { // Right-Front-Side-Bottom
//                                        System.out.println(projektilId_ + ": Penetration: Right-Front-Side-Bottom");
//                                    } else { // Right-Front-Bottom-Front
//                                        System.out.println(projektilId_ + ": Penetration: Right-Front-Bottom-Front");
//                                    }
//                                }
//                            } else if (isRear_ == true) {
//                                if (isTop_ == true) {
//                                    offsetY_ = centerTrans_.getY() + pm_.getHalfHeight() - grTrans_.getY();
//                                    offsetX_ = centerTrans_.getX() + pm_.getHalfWidth() - grTrans_.getX();
//                                    if (FastMath.abs(offsetY_) == 0f) { // Right-Rear-Top
//                                        System.out.println(projektilId_ + ": Penetration: Right-Rear-Top");
//                                    } else if (FastMath.abs(offsetX_) == 0f) { // Right-Rear-Side-Top
//                                        System.out.println(projektilId_ + ": Penetration: Right-Rear-Side-Top");
//                                    } else { // Right-Rear-Top-Rear
//                                        System.out.println(projektilId_ + ": Penetration: Right-Rear-Top-Rear");
//                                    }
//                                } else if (isBottom_ == true) {
//                                    offsetY_ = centerTrans_.getY() - pm_.getHalfHeight() - grTrans_.getY();
//                                    offsetX_ = centerTrans_.getX() + pm_.getHalfWidth() - grTrans_.getX();
//                                    if (FastMath.abs(offsetY_) == 0f) { // Right-Rear-Bottom
//                                        System.out.println(projektilId_ + ": Penetration: Right-Rear-Bottom");
//                                    } else if (FastMath.abs(offsetX_) == 0f) { // Right-Rear-Side-Bottom
//                                        System.out.println(projektilId_ + ": Penetration: Right-Rear-Side-Bottom");
//                                    } else { // Right-Rear-Bottom-Rear
//                                        System.out.println(projektilId_ + ": Penetration: Right-Rear-Bottom-Rear");
//                                    }
//                                }
//                            }
//                        } else if (isLeft_ == true) {
//                            if (isFront_ == true) {
//                                if (isTop_ == true) {
//                                    offsetY_ = centerTrans_.getY() + pm_.getHalfHeight() - grTrans_.getY();
//                                    offsetX_ = centerTrans_.getX() - pm_.getHalfWidth() - grTrans_.getX();
//                                    if (FastMath.abs(offsetY_) == 0f) { // Left-Front-Top
//                                        System.out.println(projektilId_ + ": Penetration: Left-Front-Top");
//                                    } else if (FastMath.abs(offsetX_) == 0f) { // Left-Front-Side-Top
//                                        System.out.println(projektilId_ + ": Penetration: Left-Front-Side-Top");
//                                    } else { // Left-Front-Top-Front
//                                        System.out.println(projektilId_ + ": Penetration: Left-Front-Top-Front");
//                                    }
//                                } else if (isBottom_ == true) {
//                                    offsetY_ = centerTrans_.getY() - pm_.getHalfHeight() - grTrans_.getY();
//                                    offsetX_ = centerTrans_.getX() - pm_.getHalfWidth() - grTrans_.getX();
//                                    if (FastMath.abs(offsetY_) == 0f) { // Left-Front-Bottom
//                                        System.out.println(projektilId_ + ": Penetration: Left-Front-Bottom");
//                                    } else if (FastMath.abs(offsetX_) == 0f) { // Left-Front-Side-Bottom
//                                        System.out.println(projektilId_ + ": Penetration: Left-Front-Side-Bottom");
//                                    } else { // Left-Front-Bottom-Front
//                                        System.out.println(projektilId_ + ": Penetration: Left-Front-Bottom-Front");
//                                    }
//                                }
//                            } else if (isRear_ == true) {
//                                if (isTop_ == true) {
//                                    offsetY_ = centerTrans_.getY() + pm_.getHalfHeight() - grTrans_.getY();
//                                    offsetX_ = centerTrans_.getX() - pm_.getHalfWidth() - grTrans_.getX();
//                                    if (FastMath.abs(offsetY_) == 0f) { // Left-Rear-Top
//                                        System.out.println(projektilId_ + ": Penetration: Left-Rear-Top");
//                                    } else if (FastMath.abs(offsetX_) == 0f) { // Left-Rear-Side-Top
//                                        System.out.println(projektilId_ + ": Penetration: Left-Rear-Side-Top");
//                                    } else { // Left-Rear-Top-Rear
//                                        System.out.println(projektilId_ + ": Penetration: Left-Rear-Top-Rear");
//                                    }
//                                } else if (isBottom_ == true) {
//                                    offsetY_ = centerTrans_.getY() - pm_.getHalfHeight() - grTrans_.getY();
//                                    offsetX_ = centerTrans_.getX() - pm_.getHalfWidth() - grTrans_.getX();
//                                    if (FastMath.abs(offsetY_) == 0f) { // Left-Rear-Bottom
//                                        System.out.println(projektilId_ + ": Penetration: Left-Rear-Bottom");
//                                    } else if (FastMath.abs(offsetX_) == 0f) { // Left-Rear-Side-Bottom
//                                        System.out.println(projektilId_ + ": Penetration: Left-Rear-Side-Bottom");
//                                    } else { // Left-Rear-Bottom-Rear
//                                        System.out.println(projektilId_ + ": Penetration: Left-Rear-Bottom-Rear");
//                                    }
//                                }
//                            }
//                        }
//                    }
//                    schaden_ = this.getSchaden().getFloatValue(); // TEMP CODE
//                } else {
//                    schaden_ = this.getSchaden().getFloatValue(); // TEMP CODE
//                }
//                Schaden prSchaden_ = new DefaultSchaden(schaden_);
//                pm_.getKombattant().setDamage(prSchaden_);
//                //
//                System.out.println("------------------------------");
//            }

        }
        // Remove the tick listener from the physics space.
        space.removeTickListener(this);
        // Remove the ghost object from the physics space.
        space.remove(ghostObject_);
    }
    
    /**
     * The method updates the control. This should not be called from the user code.
     *
     * @param tpf Time per frame.
     */
    @Override
    public void update(final float tpf) {
        super.update(tpf);
        if (this.isEnabled() == true) {
            effectMaxTime_ = this.getEffectMaxTime();
            flyOutMaxTime_ = this.getFlyingOutMaxTime();
            if (this.getPhysicsSpace() != null) {
                physicsSpace_ = this.getPhysicsSpace();
                timer_ += tpf;
                // Limits the time for a projectile flying out from the battlefield.
                if (timer_ > flyOutMaxTime_) {
                    if (sptlProjektil_.getParent() != null) {
                        physicsSpace_.removeCollisionListener(this);
                        physicsSpace_.remove(this);
                        sptlProjektil_.removeFromParent();
                    }
                }
            }
            // Limits the effect's action time.
            if (curTime_ >= 0) {
                curTime_ += tpf;
                if (curTime_ > effectMaxTime_) {
                    curTime_ = -1;
                    for (ParticleEmitter pe_ : exEffect_.getVisualEffectKillList()) {
                        pe_.removeFromParent();
                    }
                }
            }
        }
    }
    //
    // *************************************************************************
    //    
    /**
     * The method returns the physics space of this control.
     *
     * @return PhysicsSpace The physics space of this control.
     */
    @Override
    public PhysicsSpace getPhysicsSpace() {
        return physicsSpace_;
    }

    /**
     * The method sets the physics space for this control. The method is only
     * used internally, should not be called from user code.
     *
     * @param space The physics space for this control.
     */
    @Override
    public void setPhysicsSpace(final PhysicsSpace space) {
        super.setPhysicsSpace(space);
        if (space != null) {
            space.addCollisionListener(this);
        }
        physicsSpace_ = space;
    }
    
    /**
     * The method sets the linear velocity for this control.
     * 
     * @param vec The linear velocity to be set for this control.
     */
    @Override
    public void setLinearVelocity(final Vector3f vec) {
        super.setLinearVelocity(vec);
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    /**
     * The method applies the saved scale to the models.
     */
    private void applyScale() {
        float scale_ = scaleProjektil_.getFloatValue();
        sptlProjektil_.scale(scale_);
        cshProjektil_.setScale(new Vector3f(scale_, scale_, scale_));
    }
//    
//        private void prepareEffect(AssetManager assetManager) {
//        int COUNT_FACTOR_ = 1;
//        float COUNT_FACTOR_F_ = 1f;
//        effect_ = new ParticleEmitter("Flame", Type.Triangle, 32 * COUNT_FACTOR_);
//        effect_.setSelectRandomImage(true);
//        effect_.setStartColor(new ColorRGBA(1f, 0.4f, 0.05f, (float) (1f / COUNT_FACTOR_F_)));
//        effect_.setEndColor(new ColorRGBA(.4f, .22f, .12f, 0f));
//        effect_.setStartSize(1.3f);
//        effect_.setEndSize(2f);
//        effect_.setShape(new EmitterSphereShape(Vector3f.ZERO, 1f));
//        effect_.setParticlesPerSec(0);
//        effect_.setGravity(0, -5f, 0);
//        effect_.setLowLife(.4f);
//        effect_.setHighLife(.5f);
//        effect_.getParticleInfluencer()
//            .setInitialVelocity(new Vector3f(0, 7, 0));
//        effect_.getParticleInfluencer().setVelocityVariation(1f);
//        effect_.setImagesX(2);
//        effect_.setImagesY(2);
//        Material mat_ = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
//        mat_.setTexture("Texture", assetManager.loadTexture("Effects/Explosion/flame.png"));
//        effect_.setMaterial(mat_);
//    }
//
//    private void createGhostObject() {
//        ghostObject_ = new PhysicsGhostObject(new SphereCollisionShape(explosionRadius_));
//    }
//
//
//
//    /**
//     * Physics collision occurs.
//     *
//     * @param event
//     */
//    public void collision(PhysicsCollisionEvent event) {
//        if (isEnabled() && event.getObjectA() instanceof PhysicsRigidBody && 
//            event.getObjectB() instanceof PhysicsRigidBody) {
//            //System.out.println(&quot;collision started&quot;);
//            if (null != event.getNodeA() && null != event.getNodeA().getControl(DamageControl.class)) {
//                doDamage(
//                    event.getNodeA(),
//                    calculateKineticEnergy((PhysicsRigidBody) event.getObjectA(), (PhysicsRigidBody) event.getObjectB()));
//            }
//            if (null != event.getNodeB() && null != event.getNodeB().getControl(DamageControl.class)) {
//                doDamage(
//                    event.getNodeB(), 
//                    calculateKineticEnergy((PhysicsRigidBody) event.getObjectB(), (PhysicsRigidBody) event.getObjectA()));
//            }
//            //System.out.println(&quot;collision ended&quot;);
//        }
//    }
//
//    private void doDamage(Spatial node, final float kineticEnergy) {
//        if (kineticEnergy & gt;20.1f) {
//        final DamageControl damageControl = node.getControl(DamageControl.class);
//            //System.out.println(
//            //    &quot;damaging &quot; + node.getUserData(&quot;entityName&quot;) + &quot; for &quot; + kineticEnergy + &quot; damage&quot;);
//            app.enqueue(new Callable & lt;Void & gt;() {
//                public Void call() throws Exception {
//                    damageControl.doDamage(kineticEnergy);
//                return null;
//            }
//       });
//    }
//
//    private float calculateKineticEnergy(PhysicsRigidBody rigidBodyA, PhysicsRigidBody rigidBodyB) {
//        //the kinetic energy of a non-rotating object
//        //of mass m traveling at a speed v is 1/2mv^2
//        Vector3f relativeVelocity = rigidBodyA.getLinearVelocity().subtract(rigidBodyB.getLinearVelocity());
//        float relativeVelocityLength = relativeVelocity.length();
//        float impulse = FasterMath.fixedTpf * 0.5f * rigidBodyA.getMass() * (relativeVelocityLength * relativeVelocityLength);
//        return impulse * FasterMath.nextRandomFloat(0.95f, 1.05f);
//    }
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
