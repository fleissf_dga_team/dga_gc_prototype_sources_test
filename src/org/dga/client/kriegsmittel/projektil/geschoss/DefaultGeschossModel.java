/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsmittel.projektil.geschoss;

import com.jme3.asset.AssetManager;
import org.dga.client.kombattant.Kombattant;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.scene.Spatial;
import org.dga.client.DgaJmeScale;

/**
 * The default implementation of a model for a bullet.
 * 
 * @extends AbstractGeschossModel
 * @implements GeschossModel
 * @pattern Adapter
 * @pattern Template Method
 * @pattern Inversion of Control (IoC)
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 6.7.2018
 */
public final class DefaultGeschossModel extends AbstractGeschossModel 
    implements GeschossModel {
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new model with the supplied properties.
     *
     * @param assetManager The game application's asset manager.
     * @param kombattant The combat participator the shell is created for.
     * @param geschossSpatial The bullet's spatial.
     * @param geschossScale The bullet's scale.
     * @param geschossShape The bullet's collision shape.
     * @param geschossMass The bullet's mass.
     */
    public DefaultGeschossModel(final AssetManager assetManager, 
        final Kombattant kombattant, final Spatial geschossSpatial, 
        final DgaJmeScale geschossScale, final CollisionShape geschossShape, 
        final float geschossMass) {
        super(assetManager, kombattant, geschossSpatial, geschossScale, geschossShape, geschossMass);
    }
    
    /**
     * The constructor creates a new model with the supplied properties.
     *
     * @param assetManager The game application's asset manager.
     * @param kombattant The combat participator the shell is created for.
     * @param geschossSpatial The bullet's spatial.
     * @param geschossScale The bullet's scale.
     * @param geschossMass The bullet's mass.
     */
    public DefaultGeschossModel(final AssetManager assetManager, 
        final Kombattant kombattant, final Spatial geschossSpatial, 
        final DgaJmeScale geschossScale, final float geschossMass) {
        super(assetManager, kombattant, geschossSpatial, geschossScale, geschossMass);
    }
    //
    // *************************** Public Methods ******************************
    //
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
