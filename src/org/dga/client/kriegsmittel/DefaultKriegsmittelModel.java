/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsmittel;

import org.dga.client.kombattant.Kombattant;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.scene.Spatial;
import org.dga.client.DgaJmeScale;

/**
 * The default model for a weapon.
 * 
 * @extends AbstractKriegsmittelModel
 * @implements KriegsmittelModel
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 5.7.2018
 */
public final class DefaultKriegsmittelModel extends AbstractKriegsmittelModel 
    implements KriegsmittelModel {
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new model with the supplied properties.
     *
     * @param kombattant The combat participator the weapon is created for.
     * @param kriegsmittelSpatial The weapon's spatial.
     * @param kriegsmittelScale The weapon's scale.
     * @param kriegsmittelShape The weapon's collision shape.
     */
    public DefaultKriegsmittelModel(final Kombattant kombattant, 
        final Spatial kriegsmittelSpatial, final DgaJmeScale kriegsmittelScale, 
        final CollisionShape kriegsmittelShape) {
        super(kombattant, kriegsmittelSpatial, kriegsmittelScale, kriegsmittelShape);
    }
    //
    // *************************** Public Methods ******************************
    //
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
