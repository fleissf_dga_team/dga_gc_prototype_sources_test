/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsmittel;

import org.dga.client.kriegsmittel.controller.AbstractKriegsmittelGhostFreeControl;
import org.dga.client.DgaJmeAxis;
import org.dga.client.DgaJmeUtils;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import org.dga.client.DefaultDgaJmeScale;
import org.dga.client.DgaJmeScale;
import org.dga.client.DgaJmeCollisionableModel;

/**
 * The abstract implementation of a shooting armament.
 *
 * @extends AbstractKriegsmittelGhostFreeControl
 * @implements Schusswaffe
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 11.6.2018
 */
public abstract class AbstractSchusswaffe
    extends AbstractKriegsmittelGhostFreeControl implements Schusswaffe {
    private static final float DEFAULT_FIRE_VERTICAL_ANGLE_FLOAT_ = 
        KriegsmittelCommonConstant.DEFAULT_FIRE_VERTICAL_ANGLE.getFloatValue();
    private static final int DEFAULT_FIRE_VERTICAL_ANGLE_INT_ = 
        KriegsmittelCommonConstant.DEFAULT_FIRE_VERTICAL_ANGLE.getIntValue();
    private static final float DEFAULT_FIRE_SECTOR_ANGLE_FLOAT_ = 
        KriegsmittelCommonConstant.DEFAULT_FIRE_SECTOR_ANGLE.getFloatValue();
    private static final int DEFAULT_FIRE_SECTOR_ANGLE_INT_ = 
        KriegsmittelCommonConstant.DEFAULT_FIRE_SECTOR_ANGLE.getIntValue();
    private SchusswaffeModel modelSchusswaffe_ = null;
    private CollisionShape cshSchusswaffenfpkt_ = null;
    private Spatial spatialSchusswaffenfpkt_ = null;
    private Node nodeSchusswaffePivotPoint_ = null;
    private DgaJmeScale schusswaffeScale_ = DefaultDgaJmeScale.ORIGINAL_MODEL_SCALE;
    private DgaJmeScale munitionScale_ = DefaultDgaJmeScale.ORIGINAL_MODEL_SCALE;
    private boolean enabled_ = true;
    private float verticalAngle_ = 0f * FastMath.DEG_TO_RAD;
    private float horizontalAngle_ = 0f * FastMath.DEG_TO_RAD;
    private float maxFireSectorAngle_ = 0f * FastMath.DEG_TO_RAD;
    private float maxVerticalAngle_ = 
        KriegsmittelCommonConstant.DEFAULT_SCHUSSWAFFE_MAX_ELEVATION_ANGLE.getRadiansValue();
    private float minVerticalAngle_ = 
        KriegsmittelCommonConstant.DEFAULT_SCHUSSWAFFE_MAX_ELEVATION_ANGLE.getRadiansValue();
    private boolean isFireSectorLimited_ = false;
    private float schusswaffenfpktOffset_ = 0f;
    private float schusswaffeLoadTime_ = 
        KriegsmittelCommonConstant.DEFAULT_SCHUSSWAFFE_LOAD_TIME.getFloatValue();
    private float loadTimer_ = 0f;
    private boolean isFireReady_ = true;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new control with the supplied properties.
     *
     * @param schusswaffeModel The shooting armament's model.
     */
    public AbstractSchusswaffe(final SchusswaffeModel schusswaffeModel) {
        super(schusswaffeModel, schusswaffeModel.getPivotPoint());
        modelSchusswaffe_ = schusswaffeModel;
        spatialSchusswaffenfpkt_ = schusswaffeModel.getSpatial();
        cshSchusswaffenfpkt_ = schusswaffeModel.getCollisionShape();
        nodeSchusswaffePivotPoint_ = schusswaffeModel.getPivotPoint();
        schusswaffenfpktOffset_ = DgaJmeUtils.calculateOffsetZ(
            spatialSchusswaffenfpkt_, nodeSchusswaffePivotPoint_);
        schusswaffeScale_ = schusswaffeModel.getScale();
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method adds attached equipment mounted on the game object. Attached 
     * equipment includes all supplementary objects immediately adjoined to the 
     * main object.
     * 
     * @param attachedEquipment Supplementary equipment attached to the game object.
     */
    @Override
    public void addAttachedEquipment(final DgaJmeCollisionableModel... attachedEquipment) {
        if (attachedEquipment != null && attachedEquipment.length > 0) {
            float offset_ = 0f;
            for (DgaJmeCollisionableModel m_ : attachedEquipment) {
                offset_ = DgaJmeUtils.calculateOffsetZ(m_.getSpatial(), nodeSchusswaffePivotPoint_);
                m_.setOffset(offset_);
            }
            super.addAttachedEquipment(attachedEquipment);
        }
    }
    
    /**
     * The method adds distant equipment tied to the game object. Distant 
     * equipment includes all supplementary objects not attached to the main 
     * object but moving with it and rotating around it.
     *
     * @param distantEquipment Supplementary equipment tied to the game object.
     */
    @Override
    public void addDistantEquipment(final DgaJmeCollisionableModel... distantEquipment) {
        if (distantEquipment != null && distantEquipment.length > 0) {
            float offset_ = 0f;
            for (DgaJmeCollisionableModel m_ : distantEquipment) {
                offset_ = DgaJmeUtils.calculateOffsetZ(m_.getSpatial(), nodeSchusswaffePivotPoint_);
                m_.setOffset(offset_);
            }
            super.addDistantEquipment(distantEquipment);
        }
    }
    //
    // *************************************************************************
    //
    /**
     * The method returns the pivot point about which the game object can be rotated.
     * 
     * @return Node The pivot point about which the game object can be rotated.
     */
    @Override
    public Node getPivotPoint() {
        return nodeSchusswaffePivotPoint_;
    }
    
    /**
     * The method returns the shooting armament's world translation that is the 
     * translation of the shooting armament's barrel.
     * 
     * @return Vector3f The shooting armament's world translation that is the 
     * translation of the shooting armament's barrel.
     */
    @Override
    public Vector3f getWaffeWorldTranslation() {
        return spatialSchusswaffenfpkt_.getWorldTranslation().clone();
    }
    
    /**
     * The method returns the shooting armament's world rotation that is the 
     * rotation of the shooting armament's barrel.
     * 
     * @return Quaternion The shooting armament's world rotation that is the 
     * rotation of the shooting armament's barrel.
     */
    @Override
    public Quaternion getWaffeWorldRotation() {
        return spatialSchusswaffenfpkt_.getWorldRotation().clone();
    }
    //
    // *************************************************************************
    //
    /**
     * The method sets the maximum horizontal angle of the shooting armament 's 
     * fire sector in degrees.
     *
     * @param angle The maximum horizontal angle of the shooting armament's 
     * fire sector in degrees.
     */
    @Override
    public void setMaxFireSectorAngle(final int angle) {
        int angle_ = Math.abs(angle);
        if (angle_ <= DEFAULT_FIRE_SECTOR_ANGLE_INT_) {
            maxFireSectorAngle_ = angle_ * FastMath.DEG_TO_RAD;
            isFireSectorLimited_ = angle_ < DEFAULT_FIRE_SECTOR_ANGLE_INT_;
        }
    }

    /**
     * The method sets the maximum horizontal angle of the shooting armament's 
     * fire sector in radians.
     *
     * @param angle The maximum horizontal angle of the shooting armament's 
     * fire sector in radians.
     */
    @Override
    public void setMaxFireSectorAngle(final float angle) {
        float angle_ = Math.abs(angle);
        if (angle_ <= DEFAULT_FIRE_SECTOR_ANGLE_FLOAT_) {
            maxFireSectorAngle_ = angle_;
            isFireSectorLimited_ = angle_ < DEFAULT_FIRE_SECTOR_ANGLE_FLOAT_;
        }
    }
    
    /**
     * The method returns the maximum horizontal angle of the shooting armament's 
     * fire sector in radians.
     *
     * @return float The maximum horizontal angle of the shooting armament's 
     * fire sector in radians.
     */
    @Override
    public float getMaxFireSectorAngle() {
        return maxFireSectorAngle_;
    }
    
    /**
     * The method returns whether the shooting armament is ready to fire or not.
     * 
     * @return boolean The flag whether the shooting armament is ready to fire or not.
     */
    @Override
    public boolean isFireReady() {
        return isFireReady_;
    }
    
    /**
     * The method sets whether the shooting armament is ready to fire or not.
     * 
     * @param isFireReady The flag whether the shooting armament is ready to fire or not.
     */
    @Override
    public void setFireReady(final boolean isFireReady) {
        isFireReady_ = isFireReady;
    }
    
    /**
     * The method returns the time in seconds that the shooting armament needs 
     * to be loaded with a round.
     * 
     * @return float The time in seconds that the shooting armament needs to be 
     * loaded with a round.
     */
    @Override
    public float getLoadTime() {
        return schusswaffeLoadTime_;
    }
    
    /**
     * The method sets the time in seconds that the shooting armament needs to be 
     * loaded with a round (cartridge).
     * 
     * @param loadTime The time in seconds that the shooting armament needs to be 
     * loaded with a round (cartridge).
     */
    @Override
    public void setLoadTime(final float loadTime) {
        schusswaffeLoadTime_ = loadTime;
    }
    
    /**
     * The method returns the scale used for the shooting armament's ammunition.
     * 
     * @return DgaJmeScale The scale used for the shooting armament's ammunition.
     */
    @Override
    public DgaJmeScale getMunitionScale() {
        return munitionScale_;
    }
    
    /**
     * The method sets a new scale for the shooting armament's ammunition.
     * 
     * @param munitionScale A new scale for the shooting armament's ammunition.
     */
    @Override
    public void setMunitionScale(final DgaJmeScale munitionScale) {
        munitionScale_ = munitionScale;
    }
    //
    // *************************************************************************
    //
    /**
     * The method sets the maximum elevation angle for the shooting armament in degrees.
     *
     * @param angle The maximum elevation angle for the shooting armament in degrees.
     */
    @Override
    public void setMaxVerticalAngle(final int angle) {
        int angle_ = Math.abs(angle);
        if (angle_ <= DEFAULT_FIRE_VERTICAL_ANGLE_INT_) {
            maxVerticalAngle_ = angle_ * FastMath.DEG_TO_RAD;
        }
    }

    /**
     * The method sets the maximum elevation angle for the shooting armament in radians.
     *
     * @param angle The maximum elevation angle for the shooting armament in radians.
     */
    @Override
    public void setMaxVerticalAngle(final float angle) {
        float angle_ = Math.abs(angle);
        if (angle_ <= DEFAULT_FIRE_VERTICAL_ANGLE_FLOAT_) {
            maxVerticalAngle_ = angle_;
        }
    }

    /**
     * The method returns the maximum elevation angle for the shooting armament in radians.
     *
     * @return float The maximum elevation angle for the shooting armament in radians.
     */
    @Override
    public float getMaxVerticalAngle() {
        return maxVerticalAngle_;
    }

    /**
     * The method sets the minimum depression angle for the shooting armament in degrees.
     *
     * @param angle The minimum depression angle for the shooting armament in degrees.
     */
    @Override
    public void setMinVerticalAngle(final int angle) {
        int angle_ = Math.abs(angle);
        if (angle_ <= DEFAULT_FIRE_VERTICAL_ANGLE_INT_) {
            minVerticalAngle_ = -angle_ * FastMath.DEG_TO_RAD;
        }
    }

    /**
     * The method sets the minimum depression angle for the shooting armament in radians.
     *
     * @param angle The minimum depression angle for the shooting armament in radians.
     */
    @Override
    public void setMinVerticalAngle(final float angle) {
        float angle_ = Math.abs(angle);
        if (angle_ <= DEFAULT_FIRE_VERTICAL_ANGLE_FLOAT_) {
            minVerticalAngle_ = -angle_;
        }
    }

    /**
     * The method returns the minimum depression angle for the shooting armament in radians.
     *
     * @return float The minimum depression angle for the shooting armament in radians.
     */
    @Override
    public float getMinVerticalAngle() {
        return minVerticalAngle_;
    }
    
    /**
     * The method sets a vertical angle to the shooting armament in radians.
     *
     * @param angle A vertical angle for the shooting armament in radians.
     */
    @Override
    public void setVerticalRotation(final float angle) {
        verticalAngle_ += angle;
        if (verticalAngle_ > maxVerticalAngle_) {
            verticalAngle_ = maxVerticalAngle_;
        } else if (verticalAngle_ < minVerticalAngle_) {
            verticalAngle_ = minVerticalAngle_;
        }
        float diff_ = schusswaffenfpktOffset_ * (verticalAngle_ * (0.04f / FastMath.HALF_PI)); // Manual factor
        float distance_ = 0f;
        if (verticalAngle_ <= 0) {
            distance_ = schusswaffenfpktOffset_ + diff_;
        } else {
            distance_ = schusswaffenfpktOffset_ - diff_;
        }
        DgaJmeUtils.rotateAround(spatialSchusswaffenfpkt_, nodeSchusswaffePivotPoint_, true, 
            distance_, verticalAngle_, DgaJmeAxis.AXIS_X);
        Quaternion quatKaVertRot_ = new Quaternion().fromAngleAxis(
            -verticalAngle_, Vector3f.UNIT_X);
        spatialSchusswaffenfpkt_.setLocalRotation(quatKaVertRot_);
        Quaternion quatEqVertRot_ = null;
        if (this.getAttachedEquipmentList().size() > 0) {
            for (DgaJmeCollisionableModel m_ : this.getAttachedEquipmentList()) {
                quatEqVertRot_ = new Quaternion().fromAngleAxis(
                    -verticalAngle_* 1.15f, Vector3f.UNIT_X); // Manual factor
                m_.getSpatial().setLocalRotation(quatEqVertRot_);
            }
        }
        if (this.getDistantEquipmentList().size() > 0) {
            for (DgaJmeCollisionableModel m_ : this.getDistantEquipmentList()) {
                DgaJmeUtils.rotateAround(m_.getSpatial(), nodeSchusswaffePivotPoint_, true, 
                    m_.getOffset(), verticalAngle_, DgaJmeAxis.AXIS_X);
                quatEqVertRot_ = new Quaternion().fromAngleAxis(
                    -verticalAngle_ * 0.15f, Vector3f.UNIT_X); // Manual factor
                m_.getSpatial().setLocalRotation(quatEqVertRot_);
            }
        }
    }
    //
    // *************************************************************************
    //
    /**
     * The method sets a horizontal angle to the shooting armament in radians.
     *
     * @param angle A horizontal angle for the shooting armament in radians.
     */
    @Override
    public void setHorizontalRotation(final float angle) {
        horizontalAngle_ += angle;
        if (isFireSectorLimited_ == true) {
            if (horizontalAngle_ > maxFireSectorAngle_) {
                horizontalAngle_ = maxFireSectorAngle_;
            }
        }
        //quatKanoneHorizontalRotation_ = new Quaternion().fromAngleAxis(horizontalAngle_, Vector3f.UNIT_Y);
        //spatialKanonenaustritt_.setLocalRotation(quatKanoneHorizontalRotation_);
        //if (ATTACHED_EQUIPMENT_.size() > 0) {
        //    for (Spatial s_ : ATTACHED_EQUIPMENT_) {
        //        s_.setLocalRotation(quatKanoneHorizontalRotation_);
        //    }
        //}
    }
    //
    // *************************************************************************
    //
    /**
     * The method updates the control. This should not be called from the user code.
     *
     * @param tpf Time per frame.
     */
    @Override
    public void update(final float tpf) {
        if (enabled_ == true && spatialSchusswaffenfpkt_ != null) {
            // Measure the rate of fire
            if (isFireReady_ == false) {
                loadTimer_ += tpf;
                if (loadTimer_ > schusswaffeLoadTime_) {
                    isFireReady_ = true;
                    loadTimer_ = 0f;
                }
            }
            //Quaternion quatRotation_ = new Quaternion();
            //quatRotation_.lookAt(VIEW_DIRECTION_, Vector3f.UNIT_Y); // Horizontal rotation
            //quatRotation_.lookAt(VIEW_DIRECTION_, Vector3f.UNIT_X); // Vertical rotation
            //spatialKanonenaustritt_.setLocalRotation(quatRotation_);
            //if (ATTACHED_EQUIPMENT_.size() > 0) {
            //    for (Spatial s_ : ATTACHED_EQUIPMENT_) {
            //        s_.setLocalRotation(quatRotation_);
            //    }
            //}
            super.setPhysicsLocation(this.getSpatialTranslation());
            super.setPhysicsRotation(this.getSpatialRotation());
        }
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
