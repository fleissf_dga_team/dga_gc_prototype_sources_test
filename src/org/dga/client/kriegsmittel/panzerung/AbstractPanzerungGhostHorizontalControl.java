/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsmittel.panzerung;

import org.dga.client.kriegsmittel.controller.AbstractKriegsmittelGhostHorizontalControl;
import com.jme3.scene.Node;

/**
 * The abstract implementation of a control for a horizontally rotatable armored weapon.
 *
 * @extends AbstractKriegsmittelGhostHorizontalControl
 * @implements PanzerungGhostControl
 * @implements PanzerungHorizontallyRotatableControl
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 26.6.2018
 */
public abstract class AbstractPanzerungGhostHorizontalControl 
    extends AbstractKriegsmittelGhostHorizontalControl 
    implements PanzerungGhostControl, PanzerungHorizontallyRotatableControl {
    private PanzerungModel panzerungModel_ = null;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new control with the supplied properties.
     * 
     * @param panzerungModel The armored weapon's model.
     * @param panzerungPivotPoint The armored weapon's pivot point.
     */
    public AbstractPanzerungGhostHorizontalControl(
        final PanzerungModel panzerungModel, final Node panzerungPivotPoint) {
        super(panzerungModel, panzerungPivotPoint);
        panzerungModel_ = panzerungModel;
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the model attributed to the control.
     * 
     * @return PanzerungModel The model attributed to the control.
     */
    @Override
    public PanzerungModel getModel() {
        return panzerungModel_;
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
