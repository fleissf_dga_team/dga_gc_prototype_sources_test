/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsmittel.controller;

import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import org.dga.client.kriegsmittel.KriegsmittelModel;
import com.jme3.math.Quaternion;
import org.dga.client.DgaJmeCollisionableModel;

/**
 * The abstract implementation of a control for a horizontally rotatable weapon.
 *
 * @extends AbstractKriegsmittelGhostControl
 * @implements KriegsmittelGhostControl
 * @implements KriegsmittelHorizontallyRotatableControl
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 26.6.2018
 */
public abstract class AbstractKriegsmittelGhostHorizontalControl 
    extends AbstractKriegsmittelGhostControl 
    implements KriegsmittelGhostControl, KriegsmittelHorizontallyRotatableControl {
    private Quaternion quatHorizontalRotation_ = null;
    private Node nodePivotPoint_ = null;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new control with the supplied properties.
     * 
     * @param kriegsmittelModel The weapon's model.
     * @param kriegsmittelPivotPoint The weapon's pivot point.
     */
    public AbstractKriegsmittelGhostHorizontalControl(
        final KriegsmittelModel kriegsmittelModel, final Node kriegsmittelPivotPoint) {
        super(kriegsmittelModel);
        nodePivotPoint_ = kriegsmittelPivotPoint;
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the pivot point about which the weapon can be rotated.
     * 
     * @return Node The pivot point about which the weapon can be rotated.
     */
    @Override
    public Node getPivotPoint() {
        return nodePivotPoint_;
    }
    
    /**
     * The method updates the control. This should not be called from the user code.
     *
     * @param tpf Time per frame.
     */
    @Override
    public void update(final float tpf) {
        if (this.isEnabled() == true && this.getSpatial() != null) {
            Vector3f viewDirection_ = this.getViewDirection();
            // Turret follows the camera view.
            quatHorizontalRotation_ = new Quaternion();
            quatHorizontalRotation_.lookAt(viewDirection_, Vector3f.UNIT_Y);
            nodePivotPoint_.setLocalRotation(quatHorizontalRotation_);
            if (this.getAttachedEquipmentList().size() > 0) {
                for (DgaJmeCollisionableModel m_ : this.getAttachedEquipmentList()) {
                    m_.getSpatial().setLocalRotation(quatHorizontalRotation_);
                }
            }
            if (this.getDistantEquipmentList().size() > 0) {
                for (DgaJmeCollisionableModel m_ : this.getDistantEquipmentList()) {
                    m_.getSpatial().setLocalRotation(quatHorizontalRotation_);
                }
            }
            this.setPhysicsLocation(this.getSpatialTranslation());
            super.setPhysicsRotation(this.getSpatialRotation());
        }
    }
    
    /**
     * The method sets a horizontal angle for the game object in radians.
     *
     * @param angle A horizontal angle for the game object in radians.
     */
    @Override
    public void setHorizontalRotation(final float angle) {
        // TODO
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
