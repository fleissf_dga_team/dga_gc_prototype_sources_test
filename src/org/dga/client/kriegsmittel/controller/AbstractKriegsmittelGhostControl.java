/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsmittel.controller;

import org.dga.client.DefaultDgaJmeScale;
import org.dga.client.kriegsmittel.KriegsmittelModel;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.GhostControl;
import com.jme3.export.InputCapsule;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.OutputCapsule;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.util.clone.Cloner;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.dga.client.DgaJmeScale;
import org.dga.client.DgaJmeCollisionableModel;

/**
 * The abstract implementation of a ghost control for a weapon.
 *
 * @extends GhostControl
 * @implements KriegsmittelGhostControl
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 3.7.2018
 */
public abstract class AbstractKriegsmittelGhostControl extends GhostControl 
    implements KriegsmittelGhostControl {
    private KriegsmittelModel modelKm_ = null;
    private Spatial spatialKm_ = null;
    private CollisionShape cshKm_ = null;
    private boolean enabled_ = true;
    private boolean added_ = false;
    private PhysicsSpace physicsSpace_ = null;
    private final Vector3f VIEW_DIRECTION_ = new Vector3f(Vector3f.UNIT_Z);
    private final Map<DgaJmeCollisionableModel, Float> ATTACHED_EQUIPMENT_ = 
        Collections.synchronizedMap(new HashMap<>());
    private final Map<DgaJmeCollisionableModel, Float> DISTANT_EQUIPMENT_ = 
        Collections.synchronizedMap(new HashMap<>());
    private DgaJmeScale scaleKm_ = DefaultDgaJmeScale.ORIGINAL_MODEL_SCALE;
    private boolean isApplyPhysicsLocal_ = false;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new control with the supplied properties.
     * 
     * @param kriegsmittelModel The weapon's model.
     */
    public AbstractKriegsmittelGhostControl(final KriegsmittelModel kriegsmittelModel) {
        super(kriegsmittelModel.getCollisionShape());
        modelKm_ = kriegsmittelModel;
        spatialKm_ = kriegsmittelModel.getSpatial();
        cshKm_ = modelKm_.getCollisionShape();
        scaleKm_ = modelKm_.getScale();
        // The jMonkeyEngine v.3.2 has a strange bug with scaling that appears
        // as scaling is applying twice for spatials however only once for 
        // collision shapes. Because of this bug the following method call is 
        // disabled at the moment.
        //this.applyScale();
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the spatial of the control.
     * 
     * @return Spatial The spatial of the control.
     */
    @Override
    public Spatial getSpatial() {
        return spatialKm_;
    }
   
    /**
     * The method returns the weapon's scale.
     * 
     * @return DgaJmeScale The weapon's scale.
     */
    @Override
    public DgaJmeScale getScale() {
        return scaleKm_;
    }
    
//    /**
//     * The method sets the weapon's scale.
//     * 
//     * @param modelScale The weapon's scale.
//     */
//    @Override
//    public void setScale(final DgaJmeScale modelScale) {
//        if (modelScale != null && panzerungScale_.equals(modelScale) == false) {
//            panzerungScale_ = modelScale;
//            this.applyScale();
//        }
//    }
    
    /**
     * The method returns the model attributed to the control.
     * 
     * @return PanzerungModel The model attributed to the control.
     */
    @Override
    public KriegsmittelModel getModel() {
        return modelKm_;
    }
    
    /**
     * The method returns the spatial's translation.
     * 
     * @return Vector3f The spatial's translation.
     */
    @Override
    public Vector3f getSpatialTranslation() {
        if (isApplyPhysicsLocal_ == true) {
            return spatialKm_.getLocalTranslation();
        }
        return spatialKm_.getWorldTranslation();
    }

    /**
     * The method returns the spatial's rotation.
     * 
     * @return Quaternion The spatial's rotation.
     */
    @Override
    public Quaternion getSpatialRotation() {
        if (isApplyPhysicsLocal_ == true) {
            return spatialKm_.getLocalRotation();
        }
        return spatialKm_.getWorldRotation();
    }
    
    /**
     * The method returns the weapon's collision shape.
     * 
     * @return CollisionShape The weapon's collision shape.
     */
    @Override
    public CollisionShape getCollisionShape() {
        return cshKm_;
    }
    
    /**
     * The method sets view direction of the weapon.
     *
     * @param vec The view direction of the weapon.
     */
    public void setViewDirection(final Vector3f vec) {
        VIEW_DIRECTION_.set(vec);
    }

    /**
     * The method returns view direction of the weapon.
     *
     * @return Vector3f The view direction of the weapon.
     */
    public Vector3f getViewDirection() {
        return VIEW_DIRECTION_;
    }
    //
    // *************************************************************************
    //
    /**
     * The method adds attached equipment mounted on the game object. Attached 
     * equipment includes all supplementary objects immediately adjoined to the 
     * main object.
     * 
     * @param attachedEquipment Supplementary equipment attached to the game object.
     */
    @Override
    public void addAttachedEquipment(final DgaJmeCollisionableModel... attachedEquipment) {
        if (attachedEquipment.length > 0) {
            for (DgaJmeCollisionableModel e_ : attachedEquipment) {
                ATTACHED_EQUIPMENT_.put(e_, new Float(0f));
            }
        }
    }
    
//    /**
//     * The method adds attached equipment mounted on the game object with the 
//     * distance between the game object and the equipment. Attached equipment 
//     * includes all supplementary objects immediately adjoined to the main object.
//     * 
//     * @param attachedEquipment Supplementary equipment attached to the game 
//     * object with the distance between the game object and the equipment.
//     */
//    @Override
//    public void addAttachedEquipment(
//        final Map.Entry<DgaJmeCollisionableModel, Float>... attachedEquipment) {
//        if (attachedEquipment.length > 0) {
//            for (Map.Entry<DgaJmeCollisionableModel, Float> e_ : attachedEquipment) {
//                ATTACHED_EQUIPMENT_.put(e_.getKey(), e_.getValue());
//            }
//        }
//    }
    
    /**
     * The method adds distant equipment tied to the game object. Distant 
     * equipment includes all supplementary objects not attached to the main 
     * object but moving with it and rotating around it.
     *
     * @param distantEquipment Supplementary equipment tied to the game object.
     */
    @Override
    public void addDistantEquipment(final DgaJmeCollisionableModel... distantEquipment) {
        if (distantEquipment.length > 0) {
            for (DgaJmeCollisionableModel e_ : distantEquipment) {
                DISTANT_EQUIPMENT_.put(e_, new Float(0f));
            }
        }
    }
    
//    /**
//     * The method adds distant equipment tied to the game object with the 
//     * distance between the game object and the equipment. Distant equipment 
//     * includes all supplementary objects not attached to the main object but 
//     * moving with it and rotating around it.
//     *
//     * @param distantEquipment Supplementary equipment tied to the game object 
//     * with the distance between the game object and the equipment.
//     */
//    @Override
//    public void addDistantEquipment(
//        final Map.Entry<DgaJmeCollisionableModel, Float>... distantEquipment) {
//        if (distantEquipment.length > 0) {
//            for (Map.Entry<DgaJmeCollisionableModel, Float> e_ : distantEquipment) {
//                DISTANT_EQUIPMENT_.put(e_.getKey(), e_.getValue());
//            }
//            
//        }
//    }
    
    /**
     * The method returns attached equipment mounted on the game object. Attached 
     * equipment includes all supplementary objects immediately adjoined to the 
     * main object.
     * 
     * @return List<DgaJmeModel> Supplementary equipment attached to the game object.
     */
    @Override
    public List<DgaJmeCollisionableModel> getAttachedEquipmentList() {
        return new ArrayList<>(ATTACHED_EQUIPMENT_.keySet());
    }
    
//    /**
//     * The method returns attached equipment mounted on the game object with the 
//     * distance between the game object and the equipment. Attached equipment 
//     * includes all supplementary objects immediately adjoined to the main object.
//     * 
//     * @return Map<M, Float> Supplementary equipment attached to the game object 
//     * with the distance between the game object and the equipment.
//     */
//    @Override
//    public Map<DgaJmeCollisionableModel, Float> getAttachedEquipmentMap() {
//        return ATTACHED_EQUIPMENT_;
//    }
    
    /**
     * The method returns distant equipment tied to the game object. Distant 
     * equipment includes all supplementary objects not attached to the main 
     * object but moving with it and rotating around it.
     *
     * @return List<DgaJmeModel> Supplementary equipment tied to the game object.
     */
    @Override
    public List<DgaJmeCollisionableModel> getDistantEquipmentList() {
        return new ArrayList<>(DISTANT_EQUIPMENT_.keySet());
    }
    
//    /**
//     * The method returns distant equipment tied to the game object with the 
//     * distance between the game object and the equipment. Distant equipment 
//     * includes all supplementary objects not attached to the main object but 
//     * moving with it and rotating around it.
//     *
//     * @return Map<M, Float> Supplementary equipment tied to the game object 
//     * with the distance between the game object and the equipment.
//     */
//    @Override
//    public Map<DgaJmeCollisionableModel, Float> getDistantEquipmentMap() {
//        return DISTANT_EQUIPMENT_;
//    }
    //
    // *************************************************************************
    //
    /**
     * The method returns whether the physics of the control is applied locally.
     * 
     * @return boolean The apply of apply physics locally.
     */
    @Override
    public boolean isApplyPhysicsLocal() {
        return isApplyPhysicsLocal_;
    }

    /**
     * The method when set to true defines that the physics coordinates will be
     * applied to the local translation of the spatial instead of the world
     * translation.
     *
     * @param applyPhysicsLocal The flag of apply physics local translation.
     */
    @Override
    public void setApplyPhysicsLocal(final boolean applyPhysicsLocal) {
        isApplyPhysicsLocal_ = applyPhysicsLocal;
    }
    
    /**
     * The method returns the physics space of this control.
     *
     * @return PhysicsSpace The physics space of this control.
     */
    @Override
    public PhysicsSpace getPhysicsSpace() {
        return physicsSpace_;
    }

    /**
     * The method sets the physics space for this control. The method is only
     * used internally, should not be called from user code.
     *
     * @param space The physics space for this control.
     */
    @Override
    public void setPhysicsSpace(final PhysicsSpace space) {
        if (space == null) {
            if (physicsSpace_ != null) {
                physicsSpace_.removeCollisionObject(this);
                added_ = false;
            }
        } else {
            if (physicsSpace_ == space) {
                return;
            }
            if (this.isEnabled() == true) {
                space.addCollisionObject(this);
                added_ = true;
            }
        }
        physicsSpace_ = space;
    }
    
    /**
     * The method sets the spatial for this control. This should not be called
     * from user code.
     * 
     * @param spatial The spatial to be controlled.
     */
    @Override
    public void setSpatial(final Spatial spatial) {
        spatialKm_ = spatial;
        this.setUserObject(spatial);
        if (spatial == null) {
            return;
        }
        this.setPhysicsLocation(this.getSpatialTranslation());
        this.setPhysicsRotation(this.getSpatialRotation());
    }
    
    /**
     * The method sets whether the physics object is enabled or not. The physics
     * object is removed from the physics space when the control is disabled.
     * When the control is enabled again the physics object is moved to the
     * current location of the spatial and then added to the physics space. This
     * allows disabling/enabling physics to move the spatial freely.
     *
     * @param enabled The flag whether the physics object is enabled or not.
     */
    @Override
    public void setEnabled(final boolean enabled) {
        enabled_ = enabled;
        if (physicsSpace_ != null) {
            if (enabled == true && added_ == false) {
                if (spatialKm_ != null) {
                    this.setPhysicsLocation(getSpatialTranslation());
                    this.setPhysicsRotation(getSpatialRotation());
                }
                physicsSpace_.addCollisionObject(this);
                added_ = true;
            } else if (enabled == false && added_ == true) {
                physicsSpace_.removeCollisionObject(this);
                added_ = false;
            }
        }
    }

    /**
     * The method returns the current enabled state of the physics control.
     *
     * @return boolean The current enabled state.
     */
    @Override
    public boolean isEnabled() {
        return enabled_;
    }
    
    /**
     * The method updates the control. This should not be called from the user code.
     *
     * @param tpf Time per frame.
     */
    @Override
    public void update(final float tpf) {
        if (enabled_ == true && spatialKm_ != null) {
            super.setPhysicsLocation(this.getSpatialTranslation());
            super.setPhysicsRotation(this.getSpatialRotation());
        }
    }

    /**
     * The method is called by the RenderManager prior to queuing the spatial. 
     * This should not be called from user code.
     *
     * @param rm The render manager.
     * @param vp The view port.
     */
    @Override
    public void render(final RenderManager rm, final ViewPort vp) {
        // Nothing to render.
    }
    
    /**
     * The method reads the object from the importer.
     * 
     * @param im The importer.
     * 
     * @throws IOException If an input/output exceptions has occurred.
     */
    @Override
    public void read(final JmeImporter im) throws IOException {
        super.read(im);
        InputCapsule ic_ = im.getCapsule(this);
        enabled_ = ic_.readBoolean("enabled", true);
        spatialKm_ = (Spatial) ic_.readSavable("spatial", null);
        isApplyPhysicsLocal_ = ic_.readBoolean("applyLocalPhysics", false);
        this.setUserObject(spatialKm_);
    }

    /**
     * The method writes the object to the exporter.
     * 
     * @param ex The exporter.
     * 
     * @throws IOException If an input/output exceptions has occurred.
     */
    @Override
    public void write(final JmeExporter ex) throws IOException {
        super.write(ex);
        OutputCapsule oc_ = ex.getCapsule(this);
        oc_.write(enabled_, "enabled", true);
        oc_.write(isApplyPhysicsLocal_, "applyLocalPhysics", false);
        oc_.write(spatialKm_, "spatial", null);
    }
    
    /**
     * The method is implemented to perform deep cloning for this object,
     * resolving local cloned references using the specified cloner. The object
     * can call cloner.clone(fieldValue) to deep clone any of its fields.
     * <p>
     * <p>
     * Note: during normal clone operations the original object will not be
     * needed as the clone has already had all of the fields shallow copied.</p>
     *
     * @param cloner The cloner that is performing the cloning operation. The
     * cloneFields method can call back into the cloner to make clones if its
     * subordinate fields.
     * @param original The original object from which this object was cloned.
     * This is provided for the very rare case that this object needs to refer
     * to its original for some reason. In general, all of the relevant values
     * should have been transferred during the shallow clone and this object
     * need merely clone what it wants.
     */
    @Override
    public void cloneFields(final Cloner cloner, final Object original) {
        spatialKm_ = cloner.clone(spatialKm_);
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    /**
     * The method applies the saved scale to the models.
     */
    private void applyScale() {
        float scale_ = scaleKm_.getFloatValue();
        spatialKm_.scale(scale_);
        cshKm_.setScale(new Vector3f(scale_, scale_, scale_));
        for (DgaJmeCollisionableModel m_ : ATTACHED_EQUIPMENT_.keySet()) {
            m_.getSpatial().scale(scale_);
            m_.getCollisionShape().setScale(new Vector3f(scale_, scale_, scale_));
        }
        for (DgaJmeCollisionableModel m_ : DISTANT_EQUIPMENT_.keySet()) {
            m_.getSpatial().scale(scale_);
            m_.getCollisionShape().setScale(new Vector3f(scale_, scale_, scale_));
        }
    }
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
