/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsmittel.controller.panzerwagen;

import org.dga.client.kriegsmittel.panzerung.PanzerungModel;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;

/**
 * The default implementation of a physics control for an armored weapon's guided wheel.
 *
 * @extends AbstractLeitradControl
 * @implements LeitradControl
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 26.6.2018
 */
public final class DefaultLeitradControl extends AbstractLeitradControl 
    implements LeitradControl {
    private PanzerungModel modelLeitrad_ = null;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new control with the supplied properties.
     *
     * @param leitradModel The guided wheel's model.
     */
    public DefaultLeitradControl(final PanzerungModel leitradModel) {
        super(leitradModel);
        modelLeitrad_ = leitradModel;
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method creates a clone of the control, the given spatial is the
     * cloned version of the spatial to which this control is attached to.
     *
     * @param spatial The spatial.
     *
     * @return Control A clone of this control for the spatial.
     */
    @Override
    public Control cloneForSpatial(final Spatial spatial) {
        DefaultLeitradControl control_ = new DefaultLeitradControl(modelLeitrad_);
//        control_.setAngularFactor(getAngularFactor());
//        control_.setAngularSleepingThreshold(getAngularSleepingThreshold());
//        control_.setCcdMotionThreshold(getCcdMotionThreshold());
//        control_.setCcdSweptSphereRadius(getCcdSweptSphereRadius());
//        control_.setCollideWithGroups(getCollideWithGroups());
//        control_.setCollisionGroup(getCollisionGroup());
//        control_.setDamping(getLinearDamping(), getAngularDamping());
//        control_.setFriction(getFriction());
//        control_.setGravity(getGravity());
//        control_.setKinematic(isKinematic());
//        control_.setKinematicSpatial(isKinematicSpatial());
//        control_.setLinearSleepingThreshold(getLinearSleepingThreshold());
//        control_.setPhysicsLocation(getPhysicsLocation(null));
//        control_.setPhysicsRotation(getPhysicsRotationMatrix(null));
//        control_.setRestitution(getRestitution());
//        if (mass > 0) {
//            control_.setAngularVelocity(getAngularVelocity());
//            control_.setLinearVelocity(getLinearVelocity());
//        }
//        control_.setApplyPhysicsLocal(isApplyPhysicsLocal());
        return control_;
    }

    /**
     * The method performs a regular shallow clone of the object. Some fields
     * may also be cloned but generally only if they will never be shared with
     * other objects. (For example, local Vector3fs and so on.)
     * <p>
     * <p>
     * This method is separate from the regular clone() method so that objects
     * might still maintain their own regular java clone() semantics (perhaps
     * even using Cloner for those methods). However, because Java's clone() has
     * specific features in the sense of Object's clone() implementation, it's
     * usually best to have some path for subclasses to bypass the public
     * clone() method that might be cloning fields and instead get at the
     * superclass protected clone() methods. For example, through
     * super.jmeClone() or another protected clone method that some base class
     * eventually calls super.clone() in.</p>
     *
     * @return Object The clone of the object.
     */
    @Override
    public Object jmeClone() {
        DefaultLeitradControl control_ = new DefaultLeitradControl(modelLeitrad_);
//        control_.setAngularFactor(getAngularFactor());
//        control_.setAngularSleepingThreshold(getAngularSleepingThreshold());
//        control_.setCcdMotionThreshold(getCcdMotionThreshold());
//        control_.setCcdSweptSphereRadius(getCcdSweptSphereRadius());
//        control_.setCollideWithGroups(getCollideWithGroups());
//        control_.setCollisionGroup(getCollisionGroup());
//        control_.setDamping(getLinearDamping(), getAngularDamping());
//        control_.setFriction(getFriction());
//        control_.setGravity(getGravity());
//        control_.setKinematic(isKinematic());
//        control_.setKinematicSpatial(isKinematicSpatial());
//        control_.setLinearSleepingThreshold(getLinearSleepingThreshold());
//        control_.setPhysicsLocation(getPhysicsLocation(null));
//        control_.setPhysicsRotation(getPhysicsRotationMatrix(null));
//        control_.setRestitution(getRestitution());
//        if (mass > 0) {
//            control_.setAngularVelocity(getAngularVelocity());
//            control_.setLinearVelocity(getLinearVelocity());
//        }
//        control_.setApplyPhysicsLocal(isApplyPhysicsLocal());
//        control_.spatial_ = this.spatial_;
//        control_.setEnabled(isEnabled());
        return control_;
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
