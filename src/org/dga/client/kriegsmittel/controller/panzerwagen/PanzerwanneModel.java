/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsmittel.controller.panzerwagen;

import org.dga.client.kriegsmittel.panzerung.PanzerungModel;

/**
 * The interface of a standard model for an armored weapon's hull.
 * 
 * @extends PanzerungModel
 * @pattern Adapter
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 6.7.2018
 */
public interface PanzerwanneModel extends PanzerungModel {
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the front upper glacis armoring value of the armored weapon's hull.
     * 
     * @return float The front upper glacis armoring value of the armored weapon's hull.
     */
    public float getFrontUpperGlacisArmoring();

    /**
     * The method sets the front upper glacis armoring value of the armored weapon's hull.
     * 
     * @param frontUpperGlacisArmoring The front upper glacis armoring value of 
     * the armored weapon's hull.
     */
    public void setFrontUpperGlacisArmoring(final float frontUpperGlacisArmoring);

    /**
     * The method returns the front lower glacis armoring value of the armored weapon's hull.
     * 
     * @return float The front lower glacis armoring value of the armored weapon's hull.
     */
    public float getFrontLowerGlacisArmoring();

    /**
     * The method sets the front lower glacis armoring value of the armored weapon's hull.
     * 
     * @param frontLowerGlacisArmoring The front lower glacis armoring value of 
     * the armored weapon's hull.
     */
    public void setFrontLowerGlacisArmoring(final float frontLowerGlacisArmoring);
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
