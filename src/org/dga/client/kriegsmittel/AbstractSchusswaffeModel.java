/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsmittel;

import org.dga.client.kombattant.Kombattant;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import org.dga.client.DgaJmeScale;

/**
 * The default implementation of a shooting armament's model.
 * 
 * @extends AbstractKriegsmittelModel
 * @implements SchusswaffeModel
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 22.07.2018
 */
public abstract class AbstractSchusswaffeModel extends AbstractKriegsmittelModel 
    implements SchusswaffeModel {
    private Node schwPivotPoint_ = null;
    private Node schwShootingPoint_ = null;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new model with the supplied properties.
     *
     * @param kombattant The combat participator the shooting armament is created for.
     * @param schusswaffeSpatial The shooting armament's spatial.
     * @param schusswaffeScale The shooting armament's scale.
     * @param schusswaffeShape The shooting armament's collision shape.
     * @param schusswaffePivotPoint The shooting armament's pivot point.
     * @param schusswaffeShootingPoint The shooting armament's shooting point.
     */
    public AbstractSchusswaffeModel(final Kombattant kombattant, 
        final Spatial schusswaffeSpatial, final DgaJmeScale schusswaffeScale, 
        final CollisionShape schusswaffeShape, final Node schusswaffePivotPoint, 
        final Node schusswaffeShootingPoint) {
        super(kombattant, schusswaffeSpatial, schusswaffeScale, schusswaffeShape);
        schwPivotPoint_ = schusswaffePivotPoint;
        schwShootingPoint_ = schusswaffeShootingPoint;
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the pivot point about which the game object can be rotated.
     * 
     * @return Node The pivot point about which the game object can be rotated.
     */
    @Override
    public Node getPivotPoint() {
        return schwPivotPoint_;
    }
    
    /**
     * The method returns the point a projectile is coming from.
     * 
     * @return Node The point a projectile is coming from.
     */
    @Override
    public Node getShootingPoint() {
        return schwShootingPoint_;
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}