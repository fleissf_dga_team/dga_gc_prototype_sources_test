/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kombattant.kampffahrzeug.artillerie;

import org.dga.client.kriegsmittel.Munition;

/**
 * The types of cannon rounds (cartridges).
 *
 * @extends java.lang.Object
 * @implements Munition
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 12.5.2018
 */
public enum Kanonenpatrone implements Munition {
    /**
     * Armour-piercing capped ballistic cap (APCBC) shells.
     */
    PANZERGRANATE(1.0f),
    /**
     * Armour-piercing composite rigid (APCR) shells.
     */
    PANZERHARTKERNGRANATE(2.0f),
    /**
     * High explosive (HE) shells.
     */
    SPRENGGRANATE(3.0f);
    //
    private float floatValue_ = 0f;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The default constructor.
     *
     * @param value The constant's value.
     */
    private Kanonenpatrone(final float value) {
        floatValue_ = value;
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns a float value.
     *
     * @return float A float value.
     */
    public float getValue() {
        return floatValue_;
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
