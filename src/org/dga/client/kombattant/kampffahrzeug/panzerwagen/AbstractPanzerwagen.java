/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kombattant.kampffahrzeug.panzerwagen;

import static org.dga.client.kombattant.kampffahrzeug.panzerwagen.PanzerwagenControlBinding.PANZERTURM_MG_SHOOT_KEY;
import static org.dga.client.kombattant.kampffahrzeug.panzerwagen.PanzerwagenControlBinding.PANZERTURM_TURN_LEFT_KEY;
import static org.dga.client.kombattant.kampffahrzeug.panzerwagen.PanzerwagenControlBinding.PANZERTURM_TURN_LEFT_MOUSE;
import static org.dga.client.kombattant.kampffahrzeug.panzerwagen.PanzerwagenControlBinding.PANZERTURM_TURN_RIGHT_KEY;
import static org.dga.client.kombattant.kampffahrzeug.panzerwagen.PanzerwagenControlBinding.PANZERTURM_TURN_RIGHT_MOUSE;
import static org.dga.client.kombattant.kampffahrzeug.panzerwagen.PanzerwagenControlBinding.PANZER_MOVE_BACKWARD_KEY;
import static org.dga.client.kombattant.kampffahrzeug.panzerwagen.PanzerwagenControlBinding.PANZER_MOVE_BREAK_KEY;
import static org.dga.client.kombattant.kampffahrzeug.panzerwagen.PanzerwagenControlBinding.PANZER_MOVE_FORWARD_KEY;
import static org.dga.client.kombattant.kampffahrzeug.panzerwagen.PanzerwagenControlBinding.PANZER_ROTATE_LEFT_KEY;
import static org.dga.client.kombattant.kampffahrzeug.panzerwagen.PanzerwagenControlBinding.PANZER_ROTATE_RIGHT_KEY;
import static org.dga.client.kombattant.kampffahrzeug.panzerwagen.PanzerwagenControlBinding.TURMKANONE_SHOOT_KEY;
import static org.dga.client.kombattant.kampffahrzeug.panzerwagen.PanzerwagenControlBinding.TURMKANONE_SHOOT_MOUSE;
import static org.dga.client.kombattant.kampffahrzeug.panzerwagen.PanzerwagenControlBinding.TURMKANONE_TURN_DOWN_KEY;
import static org.dga.client.kombattant.kampffahrzeug.panzerwagen.PanzerwagenControlBinding.TURMKANONE_TURN_DOWN_MOUSE;
import static org.dga.client.kombattant.kampffahrzeug.panzerwagen.PanzerwagenControlBinding.TURMKANONE_TURN_UP_KEY;
import static org.dga.client.kombattant.kampffahrzeug.panzerwagen.PanzerwagenControlBinding.TURMKANONE_TURN_UP_MOUSE;

import org.dga.client.DgaJmeGameApplication;
import org.dga.client.DefaultDgaJmeScale;
import com.jme3.app.Application;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.input.FlyByCamera;
import com.jme3.input.InputManager;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Node;
import org.dga.client.kamera.GeneralChaseCamera;
import org.dga.client.kombattant.AbstractKombattant;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionObject;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.objects.PhysicsGhostObject;
import org.dga.client.kampfplatz.battlefieldobject.BattlefieldBorder;
import org.dga.client.kampfplatz.battlefieldobject.building.IndestructibleBuilding;
import org.dga.client.kamera.DgaChaseCamera;
import org.dga.client.DgaJmeScale;

/**
 * The abstract implementation of an armoured fighting carriage.
 * 
 * @extends AbstractKombattant
 * @implements Panzerwagen
 * @pattern Adapter
 * @pattern Template Method
 * @pattern Inversion of Control (IoC)
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 4.5.2018
 */
public abstract class AbstractPanzerwagen extends AbstractKombattant implements Panzerwagen {
    private DgaJmeScale pzwScale_ = DefaultDgaJmeScale.ORIGINAL_MODEL_SCALE;
    private DgaJmeGameApplication app_ = null;
    private Camera cam_ = null;
    private Node rootNode_ = null;
    private AssetManager assetManager_ = null;
    private InputManager inputManager_ = null;
    private BulletAppState bulletAppState_ = null;
    private PhysicsSpace physicsSpace_ = null;
    private FlyByCamera flyCam_ = null;
    private AppStateManager stateManager_ = null;
    private DgaChaseCamera pzwCamera_ = null;
    private Vector3f localPointA_ = null;
    private PhysicsGhostObject ghostObject_ = null;
    private final Vector3f VEC1_ = new Vector3f();
    private final Vector3f VEC2_ = new Vector3f();
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates an instance with the specified scale.
     * 
     * @param panzerwagenScale The model's scale.
     */
    public AbstractPanzerwagen(final DgaJmeScale panzerwagenScale) {
        super(panzerwagenScale);
        pzwScale_ = panzerwagenScale;
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the game character's collision shape.
     * 
     * @return CollisionShape The game character's collision shape.
     */
    @Override
    public abstract CollisionShape getCollisionShape();
    
    /**
     * The method is called by {@link AppStateManager} when transitioning this
     * {@code AppState} from <i>initializing</i> to <i>running</i>.<br>
     * This will happen on the next iteration through the update loop after
     * {@link AppStateManager#attach(com.jme3.app.state.AppState) } was called.
     * <p>
     * The <code>AppStateManager</code> will call this only from the update loop
     * inside the rendering thread. This means it is safe to modify the scene 
     * graph from this method.
     *
     * @param stateManager The state manager.
     * @param app The application.
     */
    @Override
    public void initialize(final AppStateManager stateManager, final Application app) {
        super.initialize(stateManager, app);
        app_ = (DgaJmeGameApplication) app;
        cam_ = app_.getCamera();
        rootNode_ = app_.getRootNode();
        assetManager_ = app_.getAssetManager();
        inputManager_ = app_.getInputManager();
        flyCam_ = app_.getFlyByCamera();
        stateManager_ = stateManager;
        bulletAppState_ = stateManager_.getState(BulletAppState.class);
        physicsSpace_ = bulletAppState_.getPhysicsSpace();
        //
        flyCam_.setEnabled(false);
        //this.flyCam.setMoveSpeed(30.0f);
        //
        inputManager_.setCursorVisible(false);
        //
        this.initTriggers();
        this.initModel();
        this.initTargetCursor();
        this.initSniperCursor();
        this.initCamera();
        this.addToScene();
    }
    
    /**
     * The method initializes the armoured fighting carriage's control triggers.
     */
    @Override
    public void initTriggers() {
        inputManager_.addMapping(PANZER_MOVE_FORWARD_KEY.getName(), PANZER_MOVE_FORWARD_KEY.getTrigger());
        inputManager_.addMapping(PANZER_MOVE_BACKWARD_KEY.getName(), PANZER_MOVE_BACKWARD_KEY.getTrigger());
        inputManager_.addMapping(PANZER_MOVE_BREAK_KEY.getName(), PANZER_MOVE_BREAK_KEY.getTrigger());
        inputManager_.addMapping(PANZER_ROTATE_LEFT_KEY.getName(), PANZER_ROTATE_LEFT_KEY.getTrigger());
        inputManager_.addMapping(PANZER_ROTATE_RIGHT_KEY.getName(), PANZER_ROTATE_RIGHT_KEY.getTrigger());
        inputManager_.addMapping(PANZERTURM_TURN_LEFT_KEY.getName(), PANZERTURM_TURN_LEFT_KEY.getTrigger());
        inputManager_.addMapping(PANZERTURM_TURN_RIGHT_KEY.getName(), PANZERTURM_TURN_RIGHT_KEY.getTrigger());
        inputManager_.addMapping(TURMKANONE_TURN_DOWN_KEY.getName(), TURMKANONE_TURN_DOWN_KEY.getTrigger());
        inputManager_.addMapping(TURMKANONE_TURN_UP_KEY.getName(), TURMKANONE_TURN_UP_KEY.getTrigger());
        inputManager_.addMapping(TURMKANONE_SHOOT_KEY.getName(), TURMKANONE_SHOOT_KEY.getTrigger());
        inputManager_.addMapping(PANZERTURM_MG_SHOOT_KEY.getName(), PANZERTURM_MG_SHOOT_KEY.getTrigger());
        //
        inputManager_.addMapping(PANZERTURM_TURN_LEFT_MOUSE.getName(), PANZERTURM_TURN_LEFT_MOUSE.getTrigger());
        inputManager_.addMapping(PANZERTURM_TURN_RIGHT_MOUSE.getName(), PANZERTURM_TURN_RIGHT_MOUSE.getTrigger());
        inputManager_.addMapping(TURMKANONE_TURN_DOWN_MOUSE.getName(), TURMKANONE_TURN_DOWN_MOUSE.getTrigger());
        inputManager_.addMapping(TURMKANONE_TURN_UP_MOUSE.getName(), TURMKANONE_TURN_UP_MOUSE.getTrigger());
        inputManager_.addMapping(TURMKANONE_SHOOT_MOUSE.getName(), TURMKANONE_SHOOT_MOUSE.getTrigger());
        //
        inputManager_.addListener(this, PANZER_MOVE_FORWARD_KEY.getName());
        inputManager_.addListener(this, PANZER_MOVE_BACKWARD_KEY.getName());
        inputManager_.addListener(this, PANZER_MOVE_BREAK_KEY.getName());
        inputManager_.addListener(this, PANZER_ROTATE_LEFT_KEY.getName());
        inputManager_.addListener(this, PANZER_ROTATE_RIGHT_KEY.getName());
        inputManager_.addListener(this, PANZERTURM_TURN_LEFT_KEY.getName());
        inputManager_.addListener(this, PANZERTURM_TURN_RIGHT_KEY.getName());
        inputManager_.addListener(this, TURMKANONE_TURN_DOWN_KEY.getName());
        inputManager_.addListener(this, TURMKANONE_TURN_UP_KEY.getName());
        inputManager_.addListener(this, TURMKANONE_SHOOT_KEY.getName());
        inputManager_.addListener(this, PANZERTURM_MG_SHOOT_KEY.getName());
        //
        inputManager_.addListener(this, PANZERTURM_TURN_LEFT_MOUSE.getName());
        inputManager_.addListener(this, PANZERTURM_TURN_RIGHT_MOUSE.getName());
        inputManager_.addListener(this, TURMKANONE_TURN_DOWN_MOUSE.getName());
        inputManager_.addListener(this, TURMKANONE_TURN_UP_MOUSE.getName());
        inputManager_.addListener(this, TURMKANONE_SHOOT_MOUSE.getName());
    }
    
    /**
     * The method initializes the armoured fighting carriage's spatial and controls.
     */
    @Override
    public abstract void initModel();
    
    /**
     * The method initializes the armoured fighting carriage's target cursor.
     */
    @Override
    public abstract void initTargetCursor();
    
    /**
     * The method initializes the armoured fighting carriage's sniper cursor.
     */
    @Override
    public abstract void initSniperCursor();
    
    /**
     * The method initializes the custom camera.
     */
    @Override
    public void initCamera() {
        // Look at the panzer with the standard chase camera.
        pzwCamera_ = new GeneralChaseCamera(cam_, inputManager_);
        // Uncomment this to invert the camera's vertical rotation Axis 
        //chCam_.setInvertVerticalAxis(true);
        // Uncomment this to invert the camera's horizontal rotation Axis
        //chCam_.setInvertHorizontalAxis(true);
        // Comment this to disable smooth camera motion
        pzwCamera_.setSmoothMotion(true);
        // Uncomment this to disable trailing of the camera 
        // WARNING, trailing only works with smooth motion enabled. It is true by default.
        pzwCamera_.setTrailingEnabled(true);
        // Uncomment this to look 3 world units above the target
        //chaseCamera_.setLookAtOffset(Vector3f.UNIT_Y.mult(3.0f));
        // Uncomment this to enable rotation when the middle mouse button is pressed (like Blender)
        // WARNING : setting this trigger disable the rotation on right and left mouse button click
        //chCam_.setToggleRotationTrigger(new MouseButtonTrigger(MouseInput.BUTTON_MIDDLE));
        // Uncomment this to set mutiple triggers to enable rotation of the cam
        // Here spade bar and middle mouse button
        //chCam_.setToggleRotationTrigger(new MouseButtonTrigger(MouseInput.BUTTON_MIDDLE),new KeyTrigger(KeyInput.KEY_SPACE));
        //chaseCamera_.setToggleRotationTrigger(new MouseButtonTrigger(MouseInput.BUTTON_RIGHT));
        // Sets the min zoom distance of the camera (default is 1).
        pzwCamera_.setMinDistance(10.0f);
        // Sets the max zoom distance of the camera (default is 40).
        pzwCamera_.setMaxDistance(50.0f);
        // Only has an effect if smoothMotion is set to true and trailing is enabled.
        // Sets the trailing sensitivity, the lower the value, the slower the camera
        // will go in the target trail when it moves. default is 0.5;
        pzwCamera_.setTrailingSensitivity(0.5f);
        // Sets the rotate amount when user moves his mouse, the lower the value, 
        // the slower the camera will rotate. default is 1.
        pzwCamera_.setRotationSpeed(0.2f);
        // Sets the offset from the target's position where the camera looks at.
        Vector3f LOOK_AT_OFFSET_ = new Vector3f(0, 10.0f, 0);
        pzwCamera_.setLookAtOffset(LOOK_AT_OFFSET_);
        // Sets the default distance at start of application.
        pzwCamera_.setDefaultDistance(40.0f);
        // Sets the default vertical rotation in radian of the camera at start of the application.
        pzwCamera_.setDefaultVerticalRotation(10.0f * FastMath.DEG_TO_RAD);
        // Sets the default horizontal rotation in radian of the camera at start of the application.
        pzwCamera_.setDefaultHorizontalRotation(200.0f * FastMath.DEG_TO_RAD);
        // Sets the chasing sensitivity, the lower the value the slower the camera 
        // will follow the target when it moves default is 5 Only has an effect 
        // if smoothMotion is set to true and trailing is enabled.
        pzwCamera_.setChasingSensitivity(5.0f);
        // Sets the zoom sensitivity, the lower the value, the slower the camera will zoom in and out. default is 2.
        pzwCamera_.setZoomSensitivity(2.0f);
        // Sets the minimal vertical rotation angle in radian of the camera around the target default is 0.
        pzwCamera_.setMinVerticalRotation(0f);
        // Sets the maximal vertical rotation angle in radian of the camera around the target. Default is Pi/2.
        pzwCamera_.setMaxVerticalRotation(FastMath.PI / 2.0f);
        // Sets the rotation sensitivity, the lower the value the slower the camera 
        // will rotates around the target when dragging with the mouse default is 5, 
        // values over 5 should have no effect. If you want a significant slow down 
        // try values below 1. Only has an effect if smoothMotion is set to true.
        pzwCamera_.setRotationSensitivity(0.1f);
        // Sets the rotate amount when user moves his mouse, the lower the value, the slower the camera will rotate. default is 1.
        pzwCamera_.setRotationSpeed(2.0f);
        // Sets the trailing rotation inertia : default is 0.1. This prevent the 
        // camera to roughtly stop when the target stops moving before the camera 
        // reached the trail position. Only has an effect if smoothMotion is set 
        // to true and trailing is enabled.
        pzwCamera_.setTrailingRotationInertia(0.1f);
        // Sets the up vector of the camera used for the lookAt on the target.
        pzwCamera_.setUpVector(Vector3f.UNIT_Y);
        // Hide the cursor when the spatial is rotating.
        pzwCamera_.setHideCursorOnRotate(true);
        // When this flag is set to false the chase camera will always rotate around 
        // its spatial independently of their distance to one another. If set to true, 
        // the chase camera will only be allowed to rotated below the "horizon" when 
        // the distance is smaller than minDistance + 1.0f (when fully zoomed-in).
        pzwCamera_.setDownRotateOnCloseViewOnly(false);
    }
    
    /**
     * The method initializes the scene, adds nodes, controls etc.
     */
    @Override
    public abstract void addToScene();
    
    /**
     * The method stops the forward movement.
     */
    @Override
    public abstract void stopForward();
    
    /**
     * The method stops the backward movement.
     */
    @Override
    public abstract void stopBackward();
    
    /**
     * The method stops the leftward movement.
     */
    @Override
    public abstract void stopLeftward();
    
    /**
     * The method stops the rightward movement.
     */
    @Override
    public abstract void stopRightward();
    
    /**
     * The method stops the left forward movement.
     */
    @Override
    public abstract void stopLeftForward();
    
    /**
     * The method stops the right forward movement.
     */
    @Override
    public abstract void stopRightForward();
    
    /**
     * The method stops the left backward movement.
     */
    @Override
    public abstract void stopLeftBackward();
    
    /**
     * The method stops the right backward movement.
     */
    @Override
    public abstract void stopRightBackward();
    
    /**
     * The method stops the upward movement.
     */
    @Override
    public abstract void stopUpward();
    
    /**
     * The method stops the downward movement.
     */
    @Override
    public abstract void stopDownward();
    
    /**
     * The method returns an object appearing in the time of collision between 
     * the game character and some affected obstacle, and then disappearing after 
     * the effect of the game character on the obstacle has gone.
     * 
     * @return PhysicsGhostObject The collision object.
     */
    @Override
    public abstract PhysicsGhostObject getCollisionObject();
    
    /**
     * The method is called when a collision happened in the PhysicsSpace, and
     * it is called from the render thread. Do not store the event object as it
     * will be cleared after the method has finished.
     *
     * @param event The collision event.
     */
    @Override
    public void collision(final PhysicsCollisionEvent event) {
        if (physicsSpace_ == null) {
            return;
        }
        if (event != null) {
            localPointA_ = event.getLocalPointA().clone();
            // Get the panzerwagen's collision object from the subclass.
            ghostObject_ = this.getCollisionObject();
            // Add the panzerwagen's ghost object from the physics space.
            ////physicsSpace_.add(ghostObject_);
            // Add this tick listener from the physics space.
            physicsSpace_.addTickListener(this);
        }
    }
    
    /**
     * The method is called before the physics is actually stepped, use to apply 
     * forces etc.
     * 
     * The jBullet Physics implementation is stepped at a constant 60 physics 
     * ticks per second frame rate. Applying forces or checking for overlaps 
     * only has an effect right at a physics update cycle, which is not every 
     * frame. If you do physics interactions at arbitrary spots in the 
     * simpleUpdate() loop, calls will be dropped at irregular intervals, 
     * because they happen out of cycle. This method is called before each step, 
     * and here you can apply forces (change the state).
     * 
     * @param space The physics space.
     * @param tpf The time per frame in seconds.
     */
    @Override
    public void prePhysicsTick(final PhysicsSpace space, final float tpf) {
        //System.out.println("PTVT: prePhysicsTick() is called.");
    }

    /**
     * The method is called after the physics has been stepped, use to check for 
     * forces etc.
     * 
     * The jBullet Physics implementation is stepped at a constant 60 physics 
     * ticks per second frame rate. Applying forces or checking for overlaps 
     * only has an effect right at a physics update cycle, which is not every 
     * frame. If you do physics interactions at arbitrary spots in the 
     * simpleUpdate() loop, calls will be dropped at irregular intervals, 
     * because they happen out of cycle. This method is called after each step, 
     * and here you can poll the results (get the current state).
     * 
     * @param space The physics space.
     * @param tpf The time per frame in seconds.
     */
    @Override
    public void physicsTick(final PhysicsSpace space, final float tpf) {
        // Get all overlapping objects and apply impulse to them.
        for (PhysicsCollisionObject physicsCollisionObject_ : ghostObject_.getOverlappingObjects()) {
            if (physicsCollisionObject_ instanceof IndestructibleBuilding || 
                physicsCollisionObject_ instanceof BattlefieldBorder) {
                //Vector3f pzwCenter_ = this.getSpatial().getLocalTranslation();
//                float lpaX_ = localPointA_.getX();
//                float lpaY_ = localPointA_.getY();
//                float lpaZ_ = localPointA_.getZ();
//                if (lpaZ_ > 0f) { // Forward
//                    this.stopForward();
//                } else if (lpaZ_ < 0f) { // Backward
//                    this.stopBackward();
//                }
//                if (lpaX_ < 0f) { // Rightward
//                    this.stopRightward();
//                } else if (lpaX_ > 0f) { // Leftward
//                    this.stopLeftward();
//                }
//                if (lpaY_ > 0f) { // Upward
//                    this.stopUpward();
//                } else if (lpaY_ < 0f) { // Downward
//                    this.stopDownward();
//                }
                
//                System.out.println("ColObject: " + physicsCollisionObject_.getCollisionShape().getObjectId());
//                System.out.println("localPointA: " + localPointA_);
//                System.out.println("--------------------------------------------------");
                
            }
            // Remove this tick listener from the physics space.
            space.removeTickListener(this);
            // Remove the ghost object from the physics space.
            ////space.remove(ghostObject_);
        }
    }
    
//    /**
//     * The method updates the control. This should not be called from the user code.
//     *
//     * @param tpf Time per frame.
//     */
//    @Override
//    public void update(final float tpf) {
//        super.update(tpf);
//        if (this.isEnabled() == true) {
//            if (this.getPhysicsSpace() != null) {
//                physicsSpace_ = this.getPhysicsSpace();
//                physicsSpace_.removeCollisionListener(this);
//                physicsSpace_.remove(this);
//            }
//        }
//    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    /**
     * The method returns the custom camera.
     * 
     * @return PanzerwagenChaseCamera The custom camera.
     */
    protected DgaChaseCamera getPanzerCamera() {
        return pzwCamera_;
    }
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
