/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kombattant.kampffahrzeug.panzerwagen;

import org.dga.client.kombattant.kampffahrzeug.Kampffahrzeug;
import com.jme3.bullet.PhysicsTickListener;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import org.dga.client.effekt.EngineEffect;

/**
 * The interface of an armoured fighting carriage.
 * 
 * @extends Kampffahrzeug
 * @extends com.jme3.app.state.AppState
 * @extends com.jme3.input.controls.ActionListener
 * @extends com.jme3.input.controls.AnalogListener
 * @extends com.jme3.bullet.collision.PhysicsCollisionListener
 * @extends com.jme3.bullet.PhysicsTickListener
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 4.5.2018
 */
public interface Panzerwagen extends Kampffahrzeug, ActionListener, AnalogListener, 
    PhysicsCollisionListener, PhysicsTickListener {
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method initializes the armoured fighting carriage's target cursor.
     */
    public void initTargetCursor();
    
    /**
     * The method initializes the armoured fighting carriage's sniper cursor.
     */
    public void initSniperCursor();
    
    /**
     * The method returns an effect provided by an engine or motor that is a machine 
     * designed to convert one form of energy into mechanical energy.
     * 
     * @return EngineEffect An effect provided by an engine or motor.
     */
    public EngineEffect getEngineEffect();
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
