/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kombattant.kampffahrzeug.panzerwagen;

import org.dga.client.kombattant.KombattantModel;
import org.dga.client.DgaJmeApplicationSettingsable;
import org.dga.client.DgaJmeApplicationGuiNodeable;

/**
 * The model for an armoured fighting carriage.
 *
 * @extends KombattantModel
 * @extends DgaJmeApplicationSettingsable
 * @extends DgaJmeApplicationGuiNodeable
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 21.07.2018
 */
public interface PanzerwagenModel extends KombattantModel, DgaJmeApplicationSettingsable, 
    DgaJmeApplicationGuiNodeable {
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the armoured fighting carriage's target cursor model.
     * 
     * @return PanzerwagenTargetModel The armoured fighting carriage's target 
     * cursor model.
     */
    public PanzerwagenTargetCursorModel getTargetCursorModel();
    
    /**
     * The method returns the armoured fighting carriage's sniper cursor model.
     * 
     * @return PanzerwagenSniperCursorModel The armoured fighting carriage's 
     * sniper cursor model.
     */
    public PanzerwagenSniperCursorModel getSniperCursorModel();
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************************************************************
}