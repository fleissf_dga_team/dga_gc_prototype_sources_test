/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kombattant.kampffahrzeug.panzerwagen;

import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Node;
import com.jme3.system.AppSettings;
import org.dga.client.kombattant.AbstractKombattantModel;
import org.dga.client.DgaJmeScale;

/**
 * The abstract implementation of a model for an armoured fighting carriage.
 * 
 * @extends AbstractKombattantModel
 * @implements PanzerwagenModel
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 21.08.2018
 */
public abstract class AbstractPanzerwagenModel extends AbstractKombattantModel 
    implements PanzerwagenModel {
    private AppSettings appSettings_ = null;
    private Node guiNode_ = null;
    private PanzerwagenTargetCursorModel targetCursorModel_ = null;
    private PanzerwagenSniperCursorModel sniperCursorModel_ = null;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new model with the supplied properties.
     * 
     * @param assetManager The game application's asset manager.
     * @param physicsSpace The game application's physics space.
     * @param rootNode The game application's scene root node.
     * @param stateManager The game application's state manager.
     * @param renderManager The game application's render manager.
     * @param appSettings The game application's settings.
     * @param guiNode The game application's GUI node.
     * @param panzerwagenScale The armoured fighting carriage's scale.
     */
    public AbstractPanzerwagenModel(final AssetManager assetManager, 
        final PhysicsSpace physicsSpace, final Node rootNode, 
        final AppStateManager stateManager, final RenderManager renderManager, 
        final AppSettings appSettings, final Node guiNode, 
        final DgaJmeScale panzerwagenScale) {
        super(assetManager, physicsSpace, rootNode, stateManager, renderManager, panzerwagenScale);
        appSettings_ = appSettings;
        guiNode_ = guiNode;
    }
    
    /**
     * The constructor creates a new model with the supplied properties.
     * 
     * @param assetManager The game application's asset manager.
     * @param physicsSpace The game application's physics space.
     * @param rootNode The game application's scene root node.
     * @param stateManager The game application's state manager.
     * @param renderManager The game application's render manager.
     * @param appSettings The game application's settings.
     * @param guiNode The game application's GUI node.
     * @param panzerwagenScale The armoured fighting carriage's scale.
     * @param targetCursorModel The armoured fighting carriage's target cursor model.
     */
    public AbstractPanzerwagenModel(final AssetManager assetManager, 
        final PhysicsSpace physicsSpace, final Node rootNode, 
        final AppStateManager stateManager, final RenderManager renderManager, 
        final AppSettings appSettings, final Node guiNode, 
        final DgaJmeScale panzerwagenScale, 
        final PanzerwagenTargetCursorModel targetCursorModel) {
        super(assetManager, physicsSpace, rootNode, stateManager, renderManager, panzerwagenScale);
        appSettings_ = appSettings;
        guiNode_ = guiNode;
        targetCursorModel_ = targetCursorModel;
    }
    
    /**
     * The constructor creates a new model with the supplied properties.
     * 
     * @param assetManager The game application's asset manager.
     * @param physicsSpace The game application's physics space.
     * @param rootNode The game application's scene root node.
     * @param stateManager The game application's state manager.
     * @param renderManager The game application's render manager.
     * @param appSettings The game application's settings.
     * @param guiNode The game application's GUI node.
     * @param panzerwagenScale The armoured fighting carriage's scale.
     * @param targetCursorModel The armoured fighting carriage's target cursor model.
     * @param sniperCursorModel The armoured fighting carriage's sniper cursor model.
     */
    public AbstractPanzerwagenModel(final AssetManager assetManager, 
        final PhysicsSpace physicsSpace, final Node rootNode, 
        final AppStateManager stateManager, final RenderManager renderManager, 
        final AppSettings appSettings, final Node guiNode, 
        final DgaJmeScale panzerwagenScale, 
        final PanzerwagenTargetCursorModel targetCursorModel, 
        final PanzerwagenSniperCursorModel sniperCursorModel) {
        super(assetManager, physicsSpace, rootNode, stateManager, renderManager, panzerwagenScale);
        appSettings_ = appSettings;
        guiNode_ = guiNode;
        targetCursorModel_ = targetCursorModel;
        sniperCursorModel_ = sniperCursorModel;
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the game application's settings.
     * 
     * @return AppSettings The game application's settings.
     */
    @Override
    public AppSettings getAppSettings() {
        return appSettings_;
    }
    
    /**
     * The method returns the game application's GUI node.
     * 
     * @return Node The game application's GUI node.
     */
    @Override
    public Node getGuiNode() {
        return guiNode_;
    }
    
    /**
     * The method returns the armoured fighting carriage's target cursor model.
     * 
     * @return PanzerwagenTargetModel The armoured fighting carriage's target 
     * cursor model.
     */
    @Override
    public PanzerwagenTargetCursorModel getTargetCursorModel() {
        return targetCursorModel_;
    }
    
    /**
     * The method returns the armoured fighting carriage's sniper cursor model.
     * 
     * @return PanzerwagenSniperCursorModel The armoured fighting carriage's 
     * sniper cursor model.
     */
    @Override
    public PanzerwagenSniperCursorModel getSniperCursorModel() {
        return sniperCursorModel_;
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}