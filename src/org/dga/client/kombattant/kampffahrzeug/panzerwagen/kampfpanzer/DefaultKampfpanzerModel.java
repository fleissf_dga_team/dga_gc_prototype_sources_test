/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kombattant.kampffahrzeug.panzerwagen.kampfpanzer;

import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Node;
import com.jme3.system.AppSettings;
import org.dga.client.kombattant.kampffahrzeug.panzerwagen.AbstractPanzerwagenModel;
import org.dga.client.kombattant.kampffahrzeug.panzerwagen.PanzerwagenSniperCursorModel;
import org.dga.client.kombattant.kampffahrzeug.panzerwagen.PanzerwagenTargetCursorModel;
import org.dga.client.DgaJmeScale;

/**
 * The default implementation of a model for a battle tank.
 * 
 * @extends AbstractPanzerwagenModel
 * @implements KampfpanzerModel
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 25.07.2018
 */
public final class DefaultKampfpanzerModel extends AbstractPanzerwagenModel 
    implements KampfpanzerModel {
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new model with the supplied properties.
     * 
     * @param assetManager The game application's asset manager.
     * @param physicsSpace The game application's physics space.
     * @param rootNode The game application's scene root node.
     * @param stateManager The game application's state manager.
     * @param renderManager The game application's render manager.
     * @param appSettings The game application's settings.
     * @param guiNode The game application's GUI node.
     * @param kampfpanzerScale The battle tank's scale.
     */
    public DefaultKampfpanzerModel(final AssetManager assetManager, 
        final PhysicsSpace physicsSpace, final Node rootNode, 
        final AppStateManager stateManager, final RenderManager renderManager, 
        final AppSettings appSettings, final Node guiNode, 
        final DgaJmeScale kampfpanzerScale) {
        super(assetManager, physicsSpace, rootNode, stateManager, renderManager, 
            appSettings, guiNode, kampfpanzerScale);
    }
    
    /**
     * The constructor creates a new model with the supplied properties.
     * 
     * @param assetManager The game application's asset manager.
     * @param physicsSpace The game application's physics space.
     * @param rootNode The game application's scene root node.
     * @param stateManager The game application's state manager.
     * @param renderManager The game application's render manager.
     * @param appSettings The game application's settings.
     * @param guiNode The game application's GUI node.
     * @param kampfpanzerScale The battle tank's scale.
     * @param targetCursorModel The battle tank's target cursor model.
     */
    public DefaultKampfpanzerModel(final AssetManager assetManager, 
        final PhysicsSpace physicsSpace, final Node rootNode, 
        final AppStateManager stateManager, final RenderManager renderManager, 
        final AppSettings appSettings, final Node guiNode, 
        final DgaJmeScale kampfpanzerScale, 
        final PanzerwagenTargetCursorModel targetCursorModel) {
        super(assetManager, physicsSpace, rootNode, stateManager, renderManager, 
            appSettings, guiNode, kampfpanzerScale, targetCursorModel);
    }
    
    /**
     * The constructor creates a new model with the supplied properties.
     * 
     * @param assetManager The game application's asset manager.
     * @param physicsSpace The game application's physics space.
     * @param rootNode The game application's scene root node.
     * @param stateManager The game application's state manager.
     * @param renderManager The game application's render manager.
     * @param appSettings The game application's settings.
     * @param guiNode The game application's GUI node.
     * @param kampfpanzerScale The battle tank's scale.
     * @param targetCursorModel The battle tank's target cursor model.
     * @param sniperCursorModel The battle tank's sniper cursor model.
     */
    public DefaultKampfpanzerModel(final AssetManager assetManager, 
        final PhysicsSpace physicsSpace, final Node rootNode, 
        final AppStateManager stateManager, final RenderManager renderManager, 
        final AppSettings appSettings, final Node guiNode, 
        final DgaJmeScale kampfpanzerScale, 
        final PanzerwagenTargetCursorModel targetCursorModel, 
        final PanzerwagenSniperCursorModel sniperCursorModel) {
        super(assetManager, physicsSpace, rootNode, stateManager, renderManager, 
            appSettings, guiNode, kampfpanzerScale, targetCursorModel, 
            sniperCursorModel);
    }
    //
    // *************************** Public Methods ******************************
    //
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}