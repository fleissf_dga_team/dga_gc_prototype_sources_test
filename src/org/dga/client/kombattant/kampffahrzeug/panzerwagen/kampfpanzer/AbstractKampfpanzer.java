/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kombattant.kampffahrzeug.panzerwagen.kampfpanzer;

import org.dga.client.kombattant.kampffahrzeug.panzerwagen.AbstractKettenfahrzeug;
import org.dga.client.DgaJmeScale;

/**
 * The abstract implementation of a battle tank.
 * 
 * @extends AbstractKettenfahrzeug
 * @implements Kampfpanzer
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 4.5.2018
 */
public abstract class AbstractKampfpanzer extends AbstractKettenfahrzeug implements Kampfpanzer {
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates an instance with the specified scale.
     * 
     * @param kampfpanzerScale The battle tank's scale.
     */
    public AbstractKampfpanzer(final DgaJmeScale kampfpanzerScale) {
        super(kampfpanzerScale);
    }
    //
    // *************************** Public Methods ******************************
    //
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
