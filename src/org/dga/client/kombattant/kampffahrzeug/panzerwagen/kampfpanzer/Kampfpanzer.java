/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kombattant.kampffahrzeug.panzerwagen.kampfpanzer;

import org.dga.client.kombattant.infanterie.handwaffe.Schiesspatrone;
import org.dga.client.kombattant.kampffahrzeug.artillerie.Kanonenpatrone;
import org.dga.client.kombattant.kampffahrzeug.panzerwagen.Kettenfahrzeug;
import java.util.List;

/**
 * The interface of a battle tank.
 * 
 * @extends Kettenfahrzeug
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 27.4.2018
 */
public interface Kampfpanzer extends Kettenfahrzeug {
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the primary cannon's ammunition list. Normally the 
     * primary cannon is the turret cannon. The method can return an empty list 
     * if the battle tank has no cannons.
     * 
     * @return List<Kanonenpatrone> The primary cannon's ammunition list.
     */
    public List<Kanonenpatrone> getPrimaryKanoneMunitionList();
    
    /**
     * The method loads the primary cannon with the specified type of rounds 
     * (cartridges). Normally the primary cannon is the turret cannon. The method 
     * can do nothing if the battle tank has no cannons.
     * 
     * @param patroneType The type of the primary cannon's rounds (cartridges).
     */
    public void loadPrimaryKanone(final Kanonenpatrone patroneType);
    
    /**
     * The method returns the secondary cannon's ammunition list. Normally the 
     * secondary cannon is the hull cannon. The method can return an empty list 
     * if the battle tank has only one primary cannon.
     * 
     * @return List<Kanonenpatrone> The secondary cannon's ammunition list.
     */
    public List<Kanonenpatrone> getSecondaryKanoneMunitionList();
    
    /**
     * The method loads the secondary cannon with the specified type of rounds 
     * (cartridges). Normally the secondary cannon is the hull cannon. The method 
     * can do nothing if the battle tank has only one primary cannon.
     * 
     * @param patroneType The type of the seconary cannon's rounds (cartridges).
     */
    public void loadSecondaryKanone(final Kanonenpatrone patroneType);
    
    /**
     * The method returns the coaxial machine gun's ammunition list. Normally 
     * this machine gun is coaxial with the primary cannon located in the turret. 
     * The method can return an empty list if the battle tank has no coaxial 
     * machine gun.
     * 
     * @return List<Schiesspatrone> The coaxial machine gun's ammunition list.
     */
    public List<Schiesspatrone> getCoaxialMaschinengewehrMunitionList();
    
    /**
     * The method loads the coaxial machine gun with the specified type of rounds 
     * (cartridges). Normally this machine gun is coaxial with the primary cannon 
     * located in the turret. The method can do nothing if the battle tank has 
     * no coaxial machine gun.
     * 
     * @param patroneType The type of the coaxial machine gun's rounds (cartridges).
     */
    public void loadCoaxialMaschinengewehr(final Schiesspatrone patroneType);
    
    /**
     * The method returns the course machine gun's ammunition list. Normally 
     * this machine gun is located in the front side of the hull. The method can 
     * return an empty list if the battle tank has no course machine gun.
     * 
     * @return List<Schiesspatrone> The coaxial machine gun's ammunition list.
     */
    public List<Schiesspatrone> getCourseMaschinengewehrMunitionList();
    
    /**
     * The method loads the course machine gun with the specified type of rounds 
     * (cartridges). Normally this machine gun is located in the front side of 
     * the hull. The method can do nothing if the battle tank has no course 
     * machine gun.
     * 
     * @param patroneType The type of the coaxial machine gun's rounds (cartridges).
     */
    public void loadCourseMaschinengewehr(final Schiesspatrone patroneType);
    
    /**
     * The method returns the cupola machine gun's ammunition list. Normally 
     * this machine gun is located beside the commander's cupola. The method can 
     * return an empty list if the battle tank has no cupola machine gun.
     * 
     * @return List<Schiesspatrone> The cupola machine gun's ammunition list.
     */
    public List<Schiesspatrone> getCupolaMaschinengewehrMunitionList();
    
    /**
     * The method loads the cupola machine gun with the specified type of rounds 
     * (cartridges). Normally this machine gun is located beside the commander's 
     * cupola. The method can do nothing if the battle tank has no cupola machine 
     * gun.
     * 
     * @param patroneType The type of the cupola machine gun's rounds (cartridges).
     */
    public void loadCupolaMaschinengewehr(final Schiesspatrone patroneType);
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
