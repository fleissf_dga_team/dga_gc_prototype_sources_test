/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kombattant.kampffahrzeug.panzerwagen;

import org.dga.client.AbstractDgaJmeTargetCursor;
import org.dga.client.DgaJmeAxis;
import org.dga.client.DgaJmeUtils;
import org.dga.client.DefaultDgaJmeScale;
import org.dga.client.kriegsmittel.KriegsmittelCommonConstant;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.dga.client.DgaJmeScale;
import org.dga.client.DgaJmeModel;
import org.dga.client.DgaJmeCollisionableModel;

/**
 * The abstract implementation of a target cursor for armoured fighting carriages.
 *
 * @extends AbstractDgaJmeTarget
 * @implements PanzerwagenTarget
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 17.7.2018
 */
public abstract class AbstractPanzerwagenTargetCursor extends AbstractDgaJmeTargetCursor 
    implements PanzerwagenTargetCursor {
    private static final float DEFAULT_TARGET_VERTICAL_ANGLE_FLOAT_ = 
        KriegsmittelCommonConstant.DEFAULT_FIRE_VERTICAL_ANGLE.getFloatValue();
    private static final int DEFAULT_TARGET_VERTICAL_ANGLE_INT_ = 
        KriegsmittelCommonConstant.DEFAULT_FIRE_VERTICAL_ANGLE.getIntValue();
    private PanzerwagenTargetCursorModel modelTarget_ = null;
    private Spatial spatialTarget_ = null;
    private Node nodePivotPoint_ = null;
    private boolean isEnabled_ = true;
    private boolean isAdded_ = false;
    private boolean isApplyPhysicsLocal_ = false;
    private DgaJmeScale targetScale_ = DefaultDgaJmeScale.ORIGINAL_MODEL_SCALE;
    private Quaternion quatVerticalRotation_ = null;
    private float verticalAngle_ = 0f * FastMath.DEG_TO_RAD;
    private float maxVerticalAngle_ = 
        PanzerwagenCommonConstant.PANZERKANONE_MAX_ELEVATION_ANGLE.getRadiansValue();
    private float minVerticalAngle_ = 
        PanzerwagenCommonConstant.PANZERKANONE_MAX_DEPRESSION_ANGLE.getRadiansValue();
    private float targetOffset_ = 
        PanzerwagenCommonConstant.PANZER_TARGET_DEFAULT_OFFSET.getFloatValue();
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new target cursor with the supplied properties.
     * 
     * @param targetModel The target cursor's model.
     */
    public AbstractPanzerwagenTargetCursor(final PanzerwagenTargetCursorModel targetModel) {
        super(targetModel);
        modelTarget_ = targetModel;
        spatialTarget_ = modelTarget_.getSpatial();
        nodePivotPoint_ = modelTarget_.getPivotPoint();
        targetOffset_ = targetModel.getOffset();
        targetScale_ = modelTarget_.getScale();
        this.applyScale();
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the pivot point about which the game object can be rotated.
     * 
     * @return Node The pivot point about which the game object can be rotated.
     */
    @Override
    public Node getPivotPoint() {
        return nodePivotPoint_;
    }
    
    /**
     * The method sets the maximum elevation angle for the target cursor in degrees.
     *
     * @param angle The maximum elevation angle for the target cursor in degrees.
     */
    @Override
    public void setMaxVerticalAngle(final int angle) {
        int angle_ = Math.abs(angle);
        if (angle_ <= DEFAULT_TARGET_VERTICAL_ANGLE_INT_) {
            maxVerticalAngle_ = angle_ * FastMath.DEG_TO_RAD;
        }
    }

    /**
     * The method sets the maximum elevation angle for the target cursor in radians.
     *
     * @param angle The maximum elevation angle for the target cursor in radians.
     */
    @Override
    public void setMaxVerticalAngle(final float angle) {
        float angle_ = Math.abs(angle);
        if (angle_ <= DEFAULT_TARGET_VERTICAL_ANGLE_FLOAT_) {
            maxVerticalAngle_ = angle_;
        }
    }

    /**
     * The method returns the maximum elevation angle for the target cursor in radians.
     *
     * @return float The maximum elevation angle for the target cursor in radians.
     */
    @Override
    public float getMaxVerticalAngle() {
        return maxVerticalAngle_;
    }

    /**
     * The method sets the minimum depression angle for the target cursor in degrees.
     *
     * @param angle The minimum depression angle for the target cursor in degrees.
     */
    @Override
    public void setMinVerticalAngle(final int angle) {
        int angle_ = Math.abs(angle);
        if (angle_ <= DEFAULT_TARGET_VERTICAL_ANGLE_INT_) {
            minVerticalAngle_ = -angle_ * FastMath.DEG_TO_RAD;
        }
    }

    /**
     * The method sets the minimum depression angle for the target cursor in radians.
     *
     * @param angle The minimum depression angle for the target cursor in radians.
     */
    @Override
    public void setMinVerticalAngle(final float angle) {
        float angle_ = Math.abs(angle);
        if (angle_ <= DEFAULT_TARGET_VERTICAL_ANGLE_FLOAT_) {
            minVerticalAngle_ = -angle_;
        }
    }

    /**
     * The method returns the minimum depression angle for the target cursor in radians.
     *
     * @return float The minimum depression angle for the target cursor in radians.
     */
    @Override
    public float getMinVerticalAngle() {
        return minVerticalAngle_;
    }
    
    /**
     * The method sets a vertical angle for the target cursor in radians.
     *
     * @param angle A vertical angle for the target cursor in radians.
     */
    @Override
    public void setVerticalRotation(final float angle) {
        verticalAngle_ += angle;
        if (verticalAngle_ > maxVerticalAngle_) {
            verticalAngle_ = maxVerticalAngle_;
        } else if (verticalAngle_ < minVerticalAngle_) {
            verticalAngle_ = minVerticalAngle_;
        }
        float diff_ = targetOffset_ * (verticalAngle_ * (0.04f / FastMath.HALF_PI)); // Manual factor
        float distance_ = 0f;
        if (verticalAngle_ <= 0) {
            distance_ = targetOffset_ + diff_;
        } else {
            distance_ = targetOffset_ - diff_;
        }
        DgaJmeUtils.rotateAround(spatialTarget_, nodePivotPoint_, true, distance_, 
            verticalAngle_, DgaJmeAxis.AXIS_X);
        Quaternion quatVertRot_ = spatialTarget_.getLocalRotation();
        spatialTarget_.getLocalRotation().set(
            quatVertRot_.getX(), quatVertRot_.getY(), -verticalAngle_, quatVertRot_.getW());
        if (this.getAttachedEquipmentList().size() > 0) {
            for (DgaJmeModel m_ : this.getAttachedEquipmentList()) {
                m_.getSpatial().setLocalRotation(quatVertRot_);
            }
        }
        //
        // Variant 3. The target moves are calculated with circle chord (way too approximate).
        //
        //float targetY_ = DgaJmeMathUtils.calculateCircleChord(targetOffset_, verticalAngle_);
        //float targetZ_ = targetOffset_ * FastMath.cos(verticalAngle_);
        //spatialTarget_.getLocalTranslation().setY(targetY_);
        //spatialTarget_.getLocalTranslation().setZ(targetZ_);
        //
        // Variant 2. The target moves are calculated with trigonometric functions (way too approximate).
        //
        //targetZ_ = targetOffset_ * FastMath.cos(verticalAngle_);
        //targetY_ = targetOffset_ * FastMath.sin(verticalAngle_);
        //spatialTarget_.getLocalTranslation().setZ(targetZ_);
        //spatialTarget_.getLocalTranslation().setY(targetY_);
        //quatTarget_ = spatialTarget_.getLocalRotation();
        //spatialTarget_.getLocalRotation().set(
        //    quatTarget_.getX(), quatTarget_.getY(), -verticalAngle_, quatTarget_.getW());
        //
        // Variant 1. The target moves vertically without rotation.
        //
        //verticalAngle_ += angle;
        //if (verticalAngle_ > maxVerticalAngle_) {
        //    verticalAngle_ = maxVerticalAngle_;
        //} else if (verticalAngle_ < minVerticalAngle_) {
        //    verticalAngle_ = minVerticalAngle_;
        //}
        //if (verticalAngle_ > minVerticalAngle_ && verticalAngle_ < maxVerticalAngle_) {
        //    targetZ_ = targetSpatial_.getLocalTranslation().getZ();
        //    targetY_ = targetSpatial_.getLocalTranslation().getY();
        //    offsetY_ = targetZ_ * FastMath.tan(FastMath.abs(angle));
        //    if (angle < 0) {
        //        targetY_ -= offsetY_;
        //    } else {
        //        targetY_ += offsetY_;
        //    }
        //    targetSpatial_.getLocalTranslation().setY(targetY_);
        //}
    }
    //
    // *************************************************************************
    //
    /**
     * The method returns the spatial's translation.
     * 
     * @return Vector3f The spatial's translation.
     */
    @Override
    public Vector3f getSpatialTranslation() {
        if (isApplyPhysicsLocal_ == true) {
            return spatialTarget_.getLocalTranslation();
        }
        return spatialTarget_.getWorldTranslation();
    }

    /**
     * The method returns the spatial's rotation.
     * 
     * @return Quaternion The spatial's rotation.
     */
    @Override
    public Quaternion getSpatialRotation() {
        if (isApplyPhysicsLocal_ == true) {
            return spatialTarget_.getLocalRotation();
        }
        return spatialTarget_.getWorldRotation();
    }
    //
    // *************************************************************************
    //
    /**
     * The method returns whether the physics of the control is applied locally.
     * 
     * @return boolean The apply of apply physics locally.
     */
    @Override
    public boolean isApplyPhysicsLocal() {
        return isApplyPhysicsLocal_;
    }

    /**
     * The method when set to true defines that the physics coordinates will be
     * applied to the local translation of the spatial instead of the world
     * translation.
     *
     * @param applyPhysicsLocal The flag of apply physics local translation.
     */
    @Override
    public void setApplyPhysicsLocal(final boolean applyPhysicsLocal) {
        isApplyPhysicsLocal_ = applyPhysicsLocal;
    }
//    
//    /**
//     * The method returns the physics space of this control.
//     *
//     * @return PhysicsSpace The physics space of this control.
//     */
//    @Override
//    public PhysicsSpace getPhysicsSpace() {
//        return space_;
//    }
//
//    /**
//     * The method sets the physics space for this control. The method is only
//     * used internally, should not be called from user code.
//     *
//     * @param space The physics space for this control.
//     */
//    @Override
//    public void setPhysicsSpace(final PhysicsSpace space) {
//        if (space == null) {
//            if (this.space_ != null) {
//                this.space_.removeCollisionObject(this);
//                added_ = false;
//            }
//        } else {
//            if (this.space_ == space) {
//                return;
//            }
//            if (isEnabled() == true) {
//                space.addCollisionObject(this);
//                added_ = true;
//            }
//        }
//        space_ = space;
//    }
    
    /**
     * The method sets the spatial for this control. This should not be called
     * from user code.
     * 
     * @param spatial The spatial to be controlled.
     */
    @Override
    public void setSpatial(final Spatial spatial) {
        spatialTarget_ = spatial;
    }
    
//    /**
//     * The method sets whether the physics object is enabled or not. The physics
//     * object is removed from the physics space when the control is disabled.
//     * When the control is enabled again the physics object is moved to the
//     * current location of the spatial and then added to the physics space. This
//     * allows disabling/enabling physics to move the spatial freely.
//     *
//     * @param enabled The flag whether the physics object is enabled or not.
//     */
//    @Override
//    public void setEnabled(final boolean enabled) {
//        enabled_ = enabled;
//        if (enabled == true && added_ == false) {
//            added_ = true;
//        } else if (enabled == false && added_ == true) {
//            added_ = false;
//        }
//    }
//
//    /**
//     * The method returns the current enabled state of the physics control.
//     *
//     * @return boolean The current enabled state.
//     */
//    @Override
//    public boolean isEnabled() {
//        return enabled_;
//    }
    
    /**
     * The method updates the control. This should not be called from the user code.
     *
     * @param tpf Time per frame.
     */
    @Override
    public void update(final float tpf) {
        // Nothing to do.
    }

    /**
     * The method is called by the RenderManager prior to queuing the spatial. 
     * This should not be called from user code.
     *
     * @param rm The render manager.
     * @param vp The view port.
     */
    @Override
    public void render(final RenderManager rm, final ViewPort vp) {
        // Nothing to render.
    }
    
    /**
     * The method reads the object from the importer.
     * 
     * @param im The importer.
     * 
     * @throws IOException If an input/output exceptions has occurred.
     */
    @Override
    public void read(final JmeImporter im) throws IOException {
        // TODO
//        InputCapsule ic_ = im.getCapsule(this);
//        spatialTarget_ = (Spatial) ic_.readSavable("spatialTarget", this);
//        nodePivotPoint_ = (Node) ic_.readSavable("nodePivotPoint", null);
//        targetOffset_ = ic_.readFloat(
//            "targetOffset", KriegsmittelScale.DEFAULT_KRIEGSMITTEL_SCALE.getFloatValue());
//        targetScale_ = (DgaJmeScale) ic_.readSavable("targetScale", null);
    }

    /**
     * The method writes the object to the exporter.
     * 
     * @param ex The exporter.
     * 
     * @throws IOException If an input/output exceptions has occurred.
     */
    @Override
    public void write(final JmeExporter ex) throws IOException {
        // TODO
//        OutputCapsule oc_ = ex.getCapsule(this);
//        oc_.write(enabled_, "enabled", true);
//        oc_.write(applyLocal_, "applyLocalPhysics", false);
//        oc_.write(targetSpatial_, "spatial", null);
    }
    
//    /**
//     * The method is implemented to perform deep cloning for this object,
//     * resolving local cloned references using the specified cloner. The object
//     * can call cloner.clone(fieldValue) to deep clone any of its fields.
//     * <p>
//     * <p>
//     * Note: during normal clone operations the original object will not be
//     * needed as the clone has already had all of the fields shallow copied.</p>
//     *
//     * @param cloner The cloner that is performing the cloning operation. The
//     * cloneFields method can call back into the cloner to make clones if its
//     * subordinate fields.
//     * @param original The original object from which this object was cloned.
//     * This is provided for the very rare case that this object needs to refer
//     * to its original for some reason. In general, all of the relevant values
//     * should have been transferred during the shallow clone and this object
//     * need merely clone what it wants.
//     */
//    @Override
//    public void cloneFields(final Cloner cloner, final Object original) {
//        spatialTarget_ = cloner.clone(spatialTarget_);
//    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    /**
     * The method applies the saved scale to the models.
     */
    private void applyScale() {
        float scale_ = targetScale_.getFloatValue();
        spatialTarget_.scale(scale_);
        for (DgaJmeModel m_ : this.getAttachedEquipmentList()) {
            m_.getSpatial().scale(scale_);
        }
        for (DgaJmeModel m_ : this.getAttachedEquipmentList()) {
            m_.getSpatial().scale(scale_);
        }
    }
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
