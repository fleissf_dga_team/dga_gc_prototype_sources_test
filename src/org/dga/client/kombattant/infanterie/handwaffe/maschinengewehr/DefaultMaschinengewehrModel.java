/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kombattant.infanterie.handwaffe.maschinengewehr;

import org.dga.client.kombattant.Kombattant;
import org.dga.client.kombattant.infanterie.handwaffe.AbstractHandwaffeModel;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import org.dga.client.DgaJmeScale;

/**
 * The default model for a machine gun.
 * 
 * @extends AbstractHandwaffeModel
 * @implements MaschinengewehrModel
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 25.07.2018
 */
public final class DefaultMaschinengewehrModel extends AbstractHandwaffeModel 
    implements MaschinengewehrModel {
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new model with the supplied properties.
     *
     * @param assetManager The game application's asset manager.
     * @param physicsSpace The game application's physics space.
     * @param rootNode The game application's scene root node.
     * @param stateManager The game application's state manager.
     * @param renderManager The game application's render manager.
     * @param kombattant The combat participator the machine gun is created for.
     * @param maschinengewehrSpatial The machine gun's spatial.
     * @param maschinengewehrScale The machine gun's scale.
     * @param maschinengewehrShape The machine gun's collision shape.
     * @param maschinengewehrPivotPoint The machine gun's pivot point.
     * @param maschinengewehrShootingPoint The machine gun's shooting point.
     */
    public DefaultMaschinengewehrModel(final AssetManager assetManager, 
        final PhysicsSpace physicsSpace, final Node rootNode, 
        final AppStateManager stateManager, final RenderManager renderManager, 
        final Kombattant kombattant, final Spatial maschinengewehrSpatial, 
        final DgaJmeScale maschinengewehrScale, 
        final CollisionShape maschinengewehrShape, final Node maschinengewehrPivotPoint, 
        final Node maschinengewehrShootingPoint) {
        super(assetManager, physicsSpace, rootNode, stateManager, renderManager, 
            kombattant, maschinengewehrSpatial, maschinengewehrScale, 
            maschinengewehrShape, maschinengewehrPivotPoint, 
            maschinengewehrShootingPoint);
    }
    //
    // *************************** Public Methods ******************************
    //
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}