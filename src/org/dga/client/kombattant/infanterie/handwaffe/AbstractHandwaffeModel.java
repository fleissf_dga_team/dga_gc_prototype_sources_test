/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kombattant.infanterie.handwaffe;

import org.dga.client.kombattant.Kombattant;
import org.dga.client.kriegsmittel.AbstractGeschuetzModel;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import org.dga.client.DgaJmeScale;

/**
 * The abstract implementation of a model for a personal weapon.
 * 
 * @extends AbstractGeschuetzModel
 * @implements HandwaffeModel
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 26.07.2018
 */
public abstract class AbstractHandwaffeModel extends AbstractGeschuetzModel 
    implements HandwaffeModel {
    private AssetManager assetManager_ = null;
    private PhysicsSpace physicsSpace_ = null;
    private Node rootNode_ = null;
    private AppStateManager stateManager_ = null;
    private RenderManager renderManager_ = null;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new model with the supplied properties.
     *
     * @param assetManager The game application's asset manager.
     * @param physicsSpace The game application's physics space.
     * @param rootNode The game application's scene root node.
     * @param stateManager The game application's state manager.
     * @param renderManager The game application's render manager.
     * @param kombattant The combat participator the personal weapon is created for.
     * @param handwaffeSpatial The personal weapon's spatial.
     * @param handwaffeScale The personal weapon's scale.
     * @param handwaffeShape The personal weapon's collision shape.
     * @param handwaffePivotPoint The personal weapon's pivot point.
     * @param handwaffeShootingPoint The personal weapon's shooting point.
     */
    public AbstractHandwaffeModel(final AssetManager assetManager, 
        final PhysicsSpace physicsSpace, final Node rootNode, 
        final AppStateManager stateManager, final RenderManager renderManager, 
        final Kombattant kombattant, final Spatial handwaffeSpatial, 
        final DgaJmeScale handwaffeScale, final CollisionShape handwaffeShape, 
        final Node handwaffePivotPoint, final Node handwaffeShootingPoint) {
        super(kombattant, handwaffeSpatial, handwaffeScale, handwaffeShape, 
            handwaffePivotPoint, handwaffeShootingPoint);
        assetManager_ = assetManager;
        physicsSpace_ = physicsSpace;
        rootNode_ = rootNode;
        stateManager_ = stateManager;
        renderManager_ = renderManager;
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the game application's asset manager.
     * 
     * @return AssetManager The game application's asset manager.
     */
    @Override
    public AssetManager getAssetManager() {
        return assetManager_;
    }
        
    /**
     * The method returns the game application's physics space.
     * 
     * @return PhysicsSpace The game application's physics space.
     */
    @Override
    public PhysicsSpace getPhysicsSpace() {
        return physicsSpace_;
    }
    
    /**
     * The method returns the game application's scene root node.
     * 
     * @return Node The game application's scene root node.
     */
    @Override
    public Node getRootNode() {
        return rootNode_;
    }
    
    /**
     * The method returns the game application's state manager.
     * 
     * @return AppStateManager The game application's state manager.
     */
    @Override
    public AppStateManager getStateManager() {
        return stateManager_;
    }
    
    /**
     * The method returns the game application's render manager.
     * 
     * @return RenderManager The game application's render manager.
     */
    @Override
    public RenderManager getRenderManager() {
        return renderManager_;
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}