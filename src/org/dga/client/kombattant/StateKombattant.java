/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kombattant;

import org.dga.client.DgaJmeState;
import org.dga.client.DgaJmePhysicsGhostObjectable;

/**
 * The interface of a combat participator based on the application state.
 *
 * @extends Kombattant
 * @extends DgaJmeState
 * @extends DgaJmePhysicsGhostObjectable
 * @pattern Adapter
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 18.08.2018
 */
public interface StateKombattant extends Kombattant, DgaJmeState, DgaJmePhysicsGhostObjectable {
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method stops the forward movement.
     */
    public void stopForward();
    
    /**
     * The method stops the backward movement.
     */
    public void stopBackward();
    
    /**
     * The method stops the leftward movement.
     */
    public void stopLeftward();
    
    /**
     * The method stops the rightward movement.
     */
    public void stopRightward();
    
    /**
     * The method stops the left forward movement.
     */
    public void stopLeftForward();
    
    /**
     * The method stops the right forward movement.
     */
    public void stopRightForward();
    
    /**
     * The method stops the left backward movement.
     */
    public void stopLeftBackward();
    
    /**
     * The method stops the right backward movement.
     */
    public void stopRightBackward();
    
    /**
     * The method stops the upward movement.
     */
    public void stopUpward();
    
    /**
     * The method stops the downward movement.
     */
    public void stopDownward();
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************************************************************
}