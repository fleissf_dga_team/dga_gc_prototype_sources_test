/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kombattant;

import org.dga.client.DgaJmeSpatialable;
import org.dga.client.DgaJmeNameable;
import org.dga.client.DgaJmeDestructor;
import org.dga.client.DgaJmeDestructible;
import org.dga.client.DgaJmeCombatable;
import org.dga.client.DgaJmeCollisionable;

/**
 * The common interface of a game character participating in combat.
 *
 * @extends Combatable
 * @extends DgaJmeSpatialable
 * @extends DgaJmeCollisionable
 * @extends DgaJmeNameable
 * @extends DgaJmeDestructible
 * @extends DgaJmeDestructor
 * @pattern Adapter
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 19.07.2018
 */
public interface Kombattant extends DgaJmeCombatable, DgaJmeSpatialable, DgaJmeCollisionable, 
    DgaJmeNameable, DgaJmeDestructible, DgaJmeDestructor {
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method initializes the combat participator's control triggers.
     */
    public void initTriggers();
    
    /**
     * The method initializes the combat participator's spatials and controls.
     */
    public void initModel();
    
    /**
     * The method initializes the custom camera.
     */
    public void initCamera();
    
    /**
     * The method adds nodes, controls, and other required parts to the scene.
     */
    public void addToScene();
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************************************************************
}