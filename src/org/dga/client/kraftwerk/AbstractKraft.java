/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kraftwerk;

/**
 * The abstract implementation of game characters' power (combat readiness, 
 * fighting capacity, health power, living power, etc).
 *
 * @extends java.lang.Object
 * @implements Kraft
 * @pattern Template Method
 * @pattern Builder
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 7.7.2018
 */
public abstract class AbstractKraft extends Object implements Kraft {
    private float initialValue_ = 0f;
    private float currentValue_ = 0f;
    private boolean isGone_ = false;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new instance with the supplied properties.
     * 
     * @param value The float value of power.
     */
    public AbstractKraft(final float value) {
        super();
        initialValue_ = value;
        currentValue_ = value;
        this.checkIsGone();
    }
    
    /**
     * The constructor creates a new instance from the other instance.
     * 
     * @param other The other instance.
     */
    public AbstractKraft(final AbstractKraft other) {
        this(other.initialValue_);
        this.currentValue_ = other.currentValue_;
        this.isGone_ = other.isGone_;
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the initial value of the game character's power.
     * 
     * @return float The initial value of the game character's power.
     */
    @Override
    public float getInitialFloatValue() {
        return initialValue_;
    }
    
    /**
     * The method returns the current value of the game character's power.
     * 
     * @return float The current value of the game character's power.
     */
    @Override
    public float getCurrentFloatValue() {
        return currentValue_;
    }
    
    /**
     * The method diminishes the game character's power.
     * 
     * @param value The value the game character's power is going to be diminished.
     */
    @Override
    public void diminish(final float value) {
        currentValue_ -= value;
        this.checkIsGone();
    }
    
    /**
     * The method restores the game character's power.
     * 
     * @param value The value the game character's power is going to be restored.
     */
    @Override
    public void restore(final float value) {
        currentValue_ += value;
        this.checkIsGone();
    }
    
    /**
     * The method returns the flag whether the power is completely gone or not.
     * 
     * @return boolean The flag whether the power is completely gone or not.
     */
    @Override
    public boolean isGone() {
        return isGone_;
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    /**
     * The method checks the power is gone.
     */
    private void checkIsGone() {
        isGone_ = currentValue_ <= 0;
    }
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
