/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kamera;

import com.jme3.export.InputCapsule;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.input.CameraInput;
import com.jme3.input.FlyByCamera;
import com.jme3.input.InputManager;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.MouseAxisTrigger;
import com.jme3.input.controls.Trigger;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;
import com.jme3.util.clone.Cloner;
import com.jme3.util.clone.JmeCloneable;
import java.io.IOException;

/**
 * The general implementation of a chase camera that follows the game character 
 * and can turn around it by dragging the mouse.
 *
 * @extends java.lang.Object
 * @implements PanzerwagenChaseCamera
 * @implements com.jme3.scene.control.Control
 * @implements com.jme3.util.clone.JmeCloneable
 * @implements com.jme3.input.controls.ActionListener
 * @implements com.jme3.input.controls.AnalogListener
 * @pattern Adapter
 * @author nehon
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 30.4.2018
 */
public final class GeneralChaseCamera extends Object implements DgaChaseCamera,
    Control, JmeCloneable, ActionListener, AnalogListener {
    private final float DEFAULT_OFFSET_DISTANCE_ = 0.002f;
    private final Vector3f TARGET_DIRECTION_ = new Vector3f();
    private final Vector3f TARGET_LOCATION_ = new Vector3f(0, 0, 0);
    private Spatial target_ = null;
    private float minVerticalRotation_ = 0.00f;
    private float maxVerticalRotation_ = FastMath.PI / 2;
    private float minDistance_ = 1.0f;
    private float maxDistance_ = 40.0f;
    private float distance_ = 20;
    private float rotationSpeed_ = 1.0f;
    private float rotation_ = 0;
    private float trailingRotationInertia_ = 0.05f;
    private float zoomSensitivity_ = 2f;
    private float rotationSensitivity_ = 5f;
    private float chasingSensitivity_ = 5f;
    private float trailingSensitivity_ = 0.5f;
    private float vRotation_ = FastMath.PI / 6;
    private boolean smoothMotion_ = false;
    private boolean trailingEnabled_ = true;
    private float rotationLerpFactor_ = 0;
    private float trailingLerpFactor_ = 0;
    private boolean rotating_ = false;
    private boolean vRotating_ = false;
    private float targetRotation_ = rotation_;
    private InputManager inputManager_ = null;
    private Vector3f initialUpVec_ = null;
    private float targetVRotation_ = vRotation_;
    private float vRotationLerpFactor_ = 0;
    private float targetDistance_ = distance_;
    private float distanceLerpFactor_ = 0;
    private boolean zooming_ = false;
    private boolean trailing_ = false;
    private boolean chasing_ = false;
    private boolean veryCloseRotation_ = true;
    private boolean canRotate_ = false;
    private Vector3f prevPos_ = null;
    private boolean targetMoves_ = false;
    private boolean enabled_ = true;
    private Camera cam_ = null;
    private float previousTargetRotation_;
    private boolean dragToRotate_ = true;
    private Vector3f lookAtOffset_ = new Vector3f(0, 0, 0);
    private boolean invertYaxis_ = false;
    private boolean invertXaxis_ = false;
    private boolean zoomIn_;
    private boolean hideCursorOnRotate_ = true;
    private Vector3f pos_ = new Vector3f();
    private boolean leftClickRotate_ = true;
    private boolean rightClickRotate_ = true;
    private Vector3f temp_ = new Vector3f(0, 0, 0);
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a chase camera. If you use this constructor you
     * have to attach the cam later to a spatial doing spatial.addControl(chaseCamera);
     *
     * @param cam The application camera.
     */
    public GeneralChaseCamera(final Camera cam) {
        cam_ = cam;
        initialUpVec_ = cam.getUp().clone();
    }
    
    /**
     * The constructor creates a chase camera.
     *
     * @param cam The application camera.
     * @param target The spatial to follow.
     */
    public GeneralChaseCamera(final Camera cam, final Spatial target) {
        this(cam);
        target.addControl(this);
    }

    /**
     * The constructor creates a chase camera and registers inputs. If you use
     * this constructor you have to attach the cam later to a spatial doing
     * spatial.addControl(chaseCamera).
     *
     * @param cam The application camera.
     * @param inputManager The inputManager of the application to register inputs.
     */
    public GeneralChaseCamera(final Camera cam, final InputManager inputManager) {
        this(cam);
        this.registerWithInput(inputManager);
    }

    /**
     * The constructor creates a chase camera and registers inputs.
     *
     * @param cam The application camera.
     * @param target The spatial to follow.
     * @param inputManager The inputManager of the application to register inputs.
     */
    public GeneralChaseCamera(final Camera cam, final Spatial target, final InputManager inputManager) {
        this(cam, target);
        this.registerWithInput(inputManager);
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method is called when an input to which this listener is registered to
     * is invoked.
     * 
     * @param name The name of the mapping that was invoked.
     * @param isPressed True if the action is "pressed", false otherwise.
     * @param tpf The time per frame value.
     */
    @Override
    public void onAction(final String name, final boolean isPressed, final float tpf) {
        if (dragToRotate_ == true) {
            if (name.equals(CameraInput.CHASECAM_TOGGLEROTATE) && enabled_) {
                if (isPressed == true) {
                    canRotate_ = true;
                    if (hideCursorOnRotate_ == true) {
                        inputManager_.setCursorVisible(false);
                    }
                } else {
                    canRotate_ = false;
                    if (hideCursorOnRotate_ == true) {
                        inputManager_.setCursorVisible(true);
                    }
                }
            }
        }
    }

    /**
     * The method is called to notify the implementation that an analog event
     * has occurred. The results of KeyTrigger and MouseButtonTrigger events
     * will have tpf == value.
     *
     * @param name The name of the mapping that was invoked.
     * @param value Value of the axis, from 0 to 1.
     * @param tpf The time per frame value.
     */
    @Override
    public void onAnalog(final String name, final float value, final float tpf) {
        switch (name) {
            case CameraInput.CHASECAM_MOVELEFT:
                rotateCamera(-value);
                break;
            case CameraInput.CHASECAM_MOVERIGHT:
                rotateCamera(value);
                break;
            case CameraInput.CHASECAM_UP:
                vRotateCamera(value);
                break;
            case CameraInput.CHASECAM_DOWN:
                vRotateCamera(-value);
                break;
            case CameraInput.CHASECAM_ZOOMIN:
                zoomCamera(-value);
                if (zoomIn_ == false) {
                    distanceLerpFactor_ = 0;
                }
                zoomIn_ = true;
                break;
            case CameraInput.CHASECAM_ZOOMOUT:
                zoomCamera(+value);
                if (zoomIn_ == true) {
                    distanceLerpFactor_ = 0;
                }
                zoomIn_ = false;
                break;
            default:
                break;
        }
    }

    /**
     * The method registers inputs with the input manager.
     *
     * @param inputManager The input manager.
     */
    @Override
    public final void registerWithInput(final InputManager inputManager) {
        String[] inputs_ = {
            //CameraInput.CHASECAM_TOGGLEROTATE,
            CameraInput.CHASECAM_DOWN,
            CameraInput.CHASECAM_UP,
            CameraInput.CHASECAM_MOVELEFT,
            CameraInput.CHASECAM_MOVERIGHT,
            CameraInput.CHASECAM_ZOOMIN,
            CameraInput.CHASECAM_ZOOMOUT
        };
        inputManager_ = inputManager;
        if (invertYaxis_ == false) {
            inputManager_.addMapping(CameraInput.CHASECAM_DOWN, new MouseAxisTrigger(MouseInput.AXIS_Y, true));
            inputManager_.addMapping(CameraInput.CHASECAM_UP, new MouseAxisTrigger(MouseInput.AXIS_Y, false));
        } else {
            inputManager_.addMapping(CameraInput.CHASECAM_DOWN, new MouseAxisTrigger(MouseInput.AXIS_Y, false));
            inputManager_.addMapping(CameraInput.CHASECAM_UP, new MouseAxisTrigger(MouseInput.AXIS_Y, true));
        }
        inputManager_.addMapping(CameraInput.CHASECAM_ZOOMIN, new MouseAxisTrigger(MouseInput.AXIS_WHEEL, false));
        inputManager_.addMapping(CameraInput.CHASECAM_ZOOMOUT, new MouseAxisTrigger(MouseInput.AXIS_WHEEL, true));
        if (invertXaxis_ == false) {
            inputManager_.addMapping(CameraInput.CHASECAM_MOVELEFT, new MouseAxisTrigger(MouseInput.AXIS_X, true));
            inputManager_.addMapping(CameraInput.CHASECAM_MOVERIGHT, new MouseAxisTrigger(MouseInput.AXIS_X, false));
        } else {
            inputManager_.addMapping(CameraInput.CHASECAM_MOVELEFT, new MouseAxisTrigger(MouseInput.AXIS_X, false));
            inputManager_.addMapping(CameraInput.CHASECAM_MOVERIGHT, new MouseAxisTrigger(MouseInput.AXIS_X, true));
        }
        //inputManager_.addMapping(CameraInput.CHASECAM_TOGGLEROTATE, new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
        //inputManager_.addMapping(CameraInput.CHASECAM_TOGGLEROTATE, new MouseButtonTrigger(MouseInput.BUTTON_RIGHT));
        inputManager_.addListener(this, inputs_);
    }

    /**
     * The method cleans up the input mappings from the input manager. This method
     * undoes the work of the method registerWithInput.
     *
     * @param inputManager The input manager from which to cleanup mappings.
     */
    @Override
    public void cleanUpWithInput(final InputManager inputManager) {
        //mgr.deleteMapping(CameraInput.CHASECAM_TOGGLEROTATE);
        inputManager.deleteMapping(CameraInput.CHASECAM_DOWN);
        inputManager.deleteMapping(CameraInput.CHASECAM_UP);
        inputManager.deleteMapping(CameraInput.CHASECAM_MOVELEFT);
        inputManager.deleteMapping(CameraInput.CHASECAM_MOVERIGHT);
        inputManager.deleteMapping(CameraInput.CHASECAM_ZOOMIN);
        inputManager.deleteMapping(CameraInput.CHASECAM_ZOOMOUT);
        inputManager.removeListener(this);
    }

    /**
     * The method sets custom triggers for toggling the rotation of the camera.
     * Defaults are left mouse button MouseButtonTrigger(MouseInput.BUTTON_LEFT)
     * and right mouse button MouseButtonTrigger(MouseInput.BUTTON_RIGHT).
     *
     * @param triggers Custom triggers for toggling the rotation of the camera.
     */
    @Override
    public void setToggleRotationTrigger(final Trigger... triggers) {
        //inputManager_.deleteMapping(CameraInput.CHASECAM_TOGGLEROTATE);
        //inputManager_.addMapping(CameraInput.CHASECAM_TOGGLEROTATE, triggers);
        //inputManager_.addListener(this, CameraInput.CHASECAM_TOGGLEROTATE);
    }

    /**
     * The method sets custom triggers for zooming in the camera. Default is mouse
     * wheel up MouseAxisTrigger(MouseInput.AXIS_WHEEL, true).
     *
     * @param triggers  Custom triggers for zooming in the camera.
     */
    @Override
    public void setZoomInTrigger(final Trigger... triggers) {
        inputManager_.deleteMapping(CameraInput.CHASECAM_ZOOMIN);
        inputManager_.addMapping(CameraInput.CHASECAM_ZOOMIN, triggers);
        inputManager_.addListener(this, CameraInput.CHASECAM_ZOOMIN);
    }

    /**
     * The method sets custom triggers for zooming out the camera. Default is 
     * mouse wheel down MouseAxisTrigger(MouseInput.AXIS_WHEEL, false).
     *
     * @param triggers Custom triggers for zooming out the camera.
     */
    @Override
    public void setZoomOutTrigger(final Trigger... triggers) {
        inputManager_.deleteMapping(CameraInput.CHASECAM_ZOOMOUT);
        inputManager_.addMapping(CameraInput.CHASECAM_ZOOMOUT, triggers);
        inputManager_.addListener(this, CameraInput.CHASECAM_ZOOMOUT);
    }

    /**
     * The method returns whether the camera is enabled or not.
     *
     * @return boolean Whether the camera is enabled or not.
     */
    @Override
    public boolean isEnabled() {
        return enabled_;
    }

    /**
     * The method enables or disables the camera.
     *
     * @param enabled True to enable, false otherwise.
     */
    @Override
    public void setEnabled(final boolean enabled) {
        enabled_ = enabled;
        if (enabled == false) {
            canRotate_ = false; // reset this flag in-case it was on before
        }
    }

    /**
     * The method returns the maximum zoom distance of the camera (default is 40).
     *
     * @return float The maximum zoom distance of the camera.
     */
    @Override
    public float getMaxDistance() {
        return maxDistance_;
    }

    /**
     * The method sets the maximum zoom distance of the camera (default is 40).
     *
     * @param maxDistance The maximum zoom distance of the camera.
     */
    @Override
    public void setMaxDistance(final float maxDistance) {
        maxDistance_ = maxDistance;
        if (maxDistance < distance_) {
            zoomCamera(maxDistance - distance_);
        }
    }

    /**
     * The method returns the minimal zoom distance of the camera (default is 1).
     *
     * @return minDistance The minimal zoom distance of the camera.
     */
    @Override
    public float getMinDistance() {
        return minDistance_;
    }

    /**
     * The method sets the minimal zoom distance of the camera (default is 1).
     *
     * @param minDistance The minimal zoom distance of the camera.
     */
    @Override
    public void setMinDistance(final float minDistance) {
        minDistance_ = minDistance;
        if (minDistance > distance_) {
            zoomCamera(distance_ - minDistance);
        }
    }

    /**
     * The method clones this camera for a spatial.
     *
     * @param spatial The spatial.
     * @return Control The camera for a spatial.
     */
    @Override
    public Control cloneForSpatial(final Spatial spatial) {
        GeneralChaseCamera cc_ = new GeneralChaseCamera(cam_, spatial, inputManager_);
        cc_.setMaxDistance(getMaxDistance());
        cc_.setMinDistance(getMinDistance());
        return cc_;
    }

    /**
     * The method performs a regular shallow clone of the object. Some fields may
     * also be cloned but generally only if they will never be shared with other
     * objects. (For example, local Vector3fs and so on.)
     *
     * <p>This method is separate from the regular clone() method so that objects 
     * might still maintain their own regular java clone() semantics (perhaps even
     * using Cloner for those methods). However, because Java's clone() has
     * specific features in the sense of Object's clone() implementation, it's
     * usually best to have some path for subclasses to bypass the public clone()
     * method that might be cloning fields and instead get at the superclass
     * protected clone() methods. For example, through super.jmeClone() or another
     * protected clone method that some base class eventually calls super.clone()
     * in.</p>
     * @return 
     */
    @Override
    public Object jmeClone() {
        GeneralChaseCamera cc_ = new GeneralChaseCamera(cam_, inputManager_);
        cc_.target_ = target_;
        cc_.setMaxDistance(getMaxDistance());
        cc_.setMinDistance(getMinDistance());
        return cc_;
    }

    /**
     * The method implements deep cloning for this object, resolving local cloned
     * references using the specified cloner. The object can call
     * cloner.clone(fieldValue) to deep clone any of its fields.
     * 
     * <p>Note: during normal clone operations the original object
     * will not be needed as the clone has already had all of the fields
     * shallow copied.</p>
     *
     * @param cloner The cloner that is performing the cloning operation. The 
     * cloneFields method can call back into the cloner to make clones if its
     * subordinate fields.     
     * @param original The original object from which this object was cloned.
     * This is provided for the very rare case that this object needs to refer
     * to its original for some reason. In general, all of the relevant values
     * should have been transferred during the shallow clone and this object
     * need merely clone what it wants.
     */
    @Override
    public void cloneFields(final Cloner cloner, final Object original) {
        target_ = cloner.clone(target_);
        computePosition();
        prevPos_ = new Vector3f(target_.getWorldTranslation());
        cam_.setLocation(pos_);
    }

    /**
     * The method sets the spatial for the camera control, should only be used internally.
     *
     * @param spatial The spatial for the camera control.
     */
    @Override
    public void setSpatial(final Spatial spatial) {
        target_ = spatial;
        if (spatial == null) {
            return;
        }
        computePosition();
        prevPos_ = new Vector3f(target_.getWorldTranslation());
        cam_.setLocation(pos_);
    }

    /**
     * The method updates the camera control, should only be used internally.
     *
     * @param tpf The time per frame (tpf).
     */
    @Override
    public void update(final float tpf) {
        updateCamera(tpf);
    }

    /**
     * The method renders the camera control, should only be used internally.
     *
     * @param rm The render manager.
     * @param vp The view port.
     */
    @Override
    public void render(final RenderManager rm, final ViewPort vp) {
        // Nothing to render
    }

    /**
     * The method writes the camera.
     *
     * @param ex the exporter
     * @throws IOException If an input/output exception occurs.
     */
    @Override
    public void write(final JmeExporter ex) throws IOException {
        // TEMP CODE
        throw new UnsupportedOperationException("remove ChaseCamera before saving");
        // TEMP CODE
    }

    /**
     * The method reads the camera.
     *
     * @param im The importer.
     * @throws IOException If an input/output exception occurs.
     */
    @Override
    public void read(final JmeImporter im) throws IOException {
        InputCapsule ic_ = im.getCapsule(this);
        maxDistance_ = ic_.readFloat("maxDistance", 40);
        minDistance_ = ic_.readFloat("minDistance", 1);
    }

    /**
     * The method returns the maximum vertical rotation of the camera.
     * 
     * @return float The maximal vertical rotation angle in radian of the camera
     * around the target.
     */
    @Override
    public float getMaxVerticalRotation() {
        return maxVerticalRotation_;
    }

    /**
     * The method sets the maximum vertical rotation angle in radians of the
     * camera around the target. Default is Pi/2.
     *
     * @param maxVerticalRotation The maximum vertical rotation angle in radians.
     */
    @Override
    public void setMaxVerticalRotation(final float maxVerticalRotation) {
        maxVerticalRotation_ = maxVerticalRotation;
    }

    /**
     * The method returns the minimal vertical rotation of the camera.
     * 
     * @return float The minimal vertical rotation angle in radians of the camera
     * around the target.
     */
    @Override
    public float getMinVerticalRotation() {
        return minVerticalRotation_;
    }

    /**
     * The method sets the minimal vertical rotation angle in radians of the
     * camera around the target. Default is 0.
     *
     * @param minHeight The minimal vertical rotation angle in radians.
     */
    @Override
    public void setMinVerticalRotation(final float minHeight) {
        minVerticalRotation_ = minHeight;
    }

    /**
     * The method returns whether the camera's smooth motion is enabled or not.
     * 
     * @return boolean True is smooth motion is enabled for this camera or false otherwise.
     */
    @Override
    public boolean isSmoothMotion() {
        return smoothMotion_;
    }

    /**
     * The method enables smooth motion for this camera.
     *
     * @param smoothMotion The flag of smooth motion.
     */
    @Override
    public void setSmoothMotion(final boolean smoothMotion) {
        smoothMotion_ = smoothMotion;
    }

    /**
     * The method returns the chasing sensitivity of the camera.
     *
     * @return float The chasing sensitivity of the camera.
     */
    @Override
    public float getChasingSensitivity() {
        return chasingSensitivity_;
    }

    /**
     * The method sets the chasing sensitivity. The lower the value is the slower
     * the camera will follow the target when it moves. Default is 5. Only has
     * an effect if smoothMotion is set to true and trailing is enabled.
     *
     * @param chasingSensitivity The chasing sensitivity.
     */
    @Override
    public void setChasingSensitivity(final float chasingSensitivity) {
        chasingSensitivity_ = chasingSensitivity;
    }

    /**
     * The method returns the rotation sensitivity of the camera.
     *
     * @return float The rotation sensitivity of the camera.
     */
    @Override
    public float getRotationSensitivity() {
        return rotationSensitivity_;
    }

    /**
     * The method sets the rotation sensitivity. The lower the value is the slower
     * the camera will rotates around the target when dragging with the mouse.
     * Default is 5, values over 5 should have no effect. If you want a significant
     * slow down then try values below 1. Only has an effect if smoothMotion is set
     * to true.
     *
     * @param rotationSensitivity The rotation sensitivity.
     */
    @Override
    public void setRotationSensitivity(final float rotationSensitivity) {
        rotationSensitivity_ = rotationSensitivity;
    }

    /**
     * The method returns true if the trailing is enabled.
     *
     * @return boolean The flag of trailing for the camera.
     */
    @Override
    public boolean isTrailingEnabled() {
        return trailingEnabled_;
    }

    /**
     * The method enables the camera trailing. The camera can smoothly go in the
     * targets trail when it moves. The method only has an effect if smoothMotion
     * is set to true.
     *
     * @param trailingEnabled The flag of camera trailing.
     */
    @Override
    public void setTrailingEnabled(final boolean trailingEnabled) {
        trailingEnabled_ = trailingEnabled;
    }

    /**
     * The method returns the trailing rotation inertia of the camera.
     *
     * @return float The value of trailing rotation inertia.
     */
    @Override
    public float getTrailingRotationInertia() {
        return trailingRotationInertia_;
    }

    /**
     * The method sets the trailing rotation inertia of the camera. Default is 0.1.
     * This prevents the camera to roughly stop when the target stops or moving
     * before the camera reached the trail position. Only has an effect if
     * smoothMotion is set to true and trailing is enabled
     *
     * @param trailingRotationInertia The trailing rotation inertia of the camera.
     */
    @Override
    public void setTrailingRotationInertia(final float trailingRotationInertia) {
        trailingRotationInertia_ = trailingRotationInertia;
    }

    /**
     * The method returns the trailing sensitivity of the camera.
     *
     * @return float The trailing sensitivity of the camera.
     */
    @Override
    public float getTrailingSensitivity() {
        return trailingSensitivity_;
    }

    /**
     * The method sets the trailing sensitivity for this camera. The lower the
     * value is the slower the camera will go in the target trail when it moves.
     * Default is 0.5. Only has an effect if smoothMotion is set to true and
     * trailing is enabled.
     *
     * @param trailingSensitivity The trailing sensitivity for this camera.
     */
    @Override
    public void setTrailingSensitivity(final float trailingSensitivity) {
        trailingSensitivity_ = trailingSensitivity;
    }

    /**
     * The method returns the zoom sensitivity.
     *
     * @return float The value of zoom sensitivity.
     */
    @Override
    public float getZoomSensitivity() {
        return zoomSensitivity_;
    }

    /**
     * The method sets the zoom sensitivity for this camera. The lower the value
     * is the slower the camera will zoom in and out. Default is 2.
     *
     * @param zoomSensitivity The zoom sensitivity for this camera.
     */
    @Override
    public void setZoomSensitivity(final float zoomSensitivity) {
        zoomSensitivity_ = zoomSensitivity;
    }

    /**
     * The method returns the rotation speed when the mouse is moved.
     *
     * @return float The rotation speed when the mouse is moved.
     */
    @Override
    public float getRotationSpeed() {
        return rotationSpeed_;
    }

    /**
     * The method sets the rotation amount when user moves the mouse. The lower
     * the value is the slower the camera will rotate. Default is 1.
     *
     * @param rotationSpeed The rotation speed on mouse movement.
     */
    @Override
    public void setRotationSpeed(final float rotationSpeed) {
        rotationSpeed_ = rotationSpeed;
    }

    /**
     * The method sets the default distance at the application's start.
     *
     * @param defaultDistance The default distance at the application's start.
     */
    @Override
    public void setDefaultDistance(final float defaultDistance) {
        distance_ = defaultDistance;
        targetDistance_ = distance_;
    }

    /**
     * The method sets the default horizontal rotation in radians of the camera
     * at the application's start.
     *
     * @param angleInRadians The default horizontal rotation in radians.
     */
    @Override
    public void setDefaultHorizontalRotation(final float angleInRadians) {
        rotation_ = angleInRadians;
        targetRotation_ = angleInRadians;
    }

    /**
     * The method sets the default vertical rotation in radians of the camera at
     * the application's start.
     *
     * @param angleInRadians The default vertical rotation in radians.
     */
    @Override
    public void setDefaultVerticalRotation(final float angleInRadians) {
        vRotation_ = angleInRadians;
        targetVRotation_ = angleInRadians;
    }

    /**
     * The method returns whether drag capability to rotate is set or not.
     * 
     * @return boolean True if drag to rotate feature is enabled.
     * 
     * @see FlyByCamera#setDragToRotate(boolean)
     */
    @Override
    public boolean isDragToRotate() {
        return dragToRotate_;
    }

    /**
     * The method sets drag capability to rotate. When it sets to true, the user
     * must hold the mouse button and drag over the screen to rotate the camera,
     * and the cursor is visible until dragged. Otherwise, the cursor is invisible
     * at all times and holding the mouse button is not needed to rotate the camera.
     * This feature is disabled by default.
     * 
     * @param dragToRotate Drag capability to rotate.
     */
    @Override
    public void setDragToRotate(final boolean dragToRotate) {
        dragToRotate_ = dragToRotate;
        canRotate_ = !dragToRotate;
        inputManager_.setCursorVisible(dragToRotate);
    }

    /**
     * The method sets down rotate capability on close view only. When this flag
     * is set to false the camera will always rotate around its spatial
     * independently of their distance to one another. If set to true, the chase
     * camera will only be allowed to rotated below the "horizon" when the distance
     * is smaller than minDistance + 1.0f (when fully zoomed-in).
     * 
     * @param rotateOnlyWhenClose  Down rotate capability on close view only.
     */
    @Override
    public void setDownRotateOnCloseViewOnly(final boolean rotateOnlyWhenClose) {
        veryCloseRotation_ = rotateOnlyWhenClose;
    }

    /**
     * The method returns down rotate capability on close view only. The method
     * returns true if rotation below the vertical plane of the spatial tied to
     * the camera is allowed only when zoomed in at minDistance + 1.0f. The 
     * method returns false if vertical rotation is always allowed.
     * 
     * @return boolean True if down rotate capability on close view only is enabled.
     */
    @Override
    public boolean getDownRotateOnCloseViewOnly() {
        return veryCloseRotation_;
    }

    /**
     * The method returns the current distance from the camera to the target.
     *
     * @return float The current distance from the camera to the target.
     */
    @Override
    public float getDistanceToTarget() {
        return distance_;
    }

    /**
     * The method returns the current horizontal rotation around the target in radians.
     *
     * @return float The current horizontal rotation around the target in radians.
     */
    @Override
    public float getHorizontalRotation() {
        return rotation_;
    }

    /**
     * The method returns the current vertical rotation around the target in radians.
     *
     * @return float The current vertical rotation around the target in radians.
     */
    @Override
    public float getVerticalRotation() {
        return vRotation_;
    }

    /**
     * The method returns the offset from the target's position where the camera looks at.
     *
     * @return Vector3f The offset from the target's position where the camera looks at.
     */
    @Override
    public Vector3f getLookAtOffset() {
        return lookAtOffset_;
    }

    /**
     * The method sets the offset from the target's position where the camera looks at.
     *
     * @param lookAtOffset The offset from the target's position where the camera looks at.
     */
    @Override
    public void setLookAtOffset(final Vector3f lookAtOffset) {
        lookAtOffset_ = lookAtOffset;
    }

    /**
     * The method sets the up vector of the camera used for the lookAt on the target.
     *
     * @param up The up vector of the camera used for the lookAt on the target.
     */
    @Override
    public void setUpVector(final Vector3f up) {
        initialUpVec_ = up;
    }

    /**
     * The method returns the up vector of the camera used for the lookAt on the target.
     *
     * @return Vector3f The up vector of the camera used for the lookAt on the target.
     */
    @Override
    public Vector3f getUpVector() {
        return initialUpVec_;
    }

    /**
     * The method returns the hide cursor property on rotate.
     * 
     * @return boolean The flag of the hide cursor property on rotate.
     */
    @Override
    public boolean isHideCursorOnRotate() {
        return hideCursorOnRotate_;
    }

    /**
     * The method sets the hide cursor property on rotate.
     * 
     * @param hideCursorOnRotate The hide cursor property on rotate.
     */
    @Override
    public void setHideCursorOnRotate(final boolean hideCursorOnRotate) {
        hideCursorOnRotate_ = hideCursorOnRotate;
    }

    /**
     * The method inverts the vertical axis movement of the mouse.
     *
     * @param invertYaxis The flag to invert the vertical axis movement of the mouse.
     */
    @Override
    public void setInvertVerticalAxis(final boolean invertYaxis) {
        invertYaxis_ = invertYaxis;
        inputManager_.deleteMapping(CameraInput.CHASECAM_DOWN);
        inputManager_.deleteMapping(CameraInput.CHASECAM_UP);
        if (invertYaxis == false) {
            inputManager_.addMapping(CameraInput.CHASECAM_DOWN, new MouseAxisTrigger(MouseInput.AXIS_Y, true));
            inputManager_.addMapping(CameraInput.CHASECAM_UP, new MouseAxisTrigger(MouseInput.AXIS_Y, false));
        } else {
            inputManager_.addMapping(CameraInput.CHASECAM_DOWN, new MouseAxisTrigger(MouseInput.AXIS_Y, false));
            inputManager_.addMapping(CameraInput.CHASECAM_UP, new MouseAxisTrigger(MouseInput.AXIS_Y, true));
        }
        inputManager_.addListener(this, CameraInput.CHASECAM_DOWN, CameraInput.CHASECAM_UP);
    }

    /**
     * The method inverts the horizontal axis movement of the mouse.
     *
     * @param invertXaxis The flag to invert the horizontal axis movement of the mouse.
     */
    @Override
    public void setInvertHorizontalAxis(final boolean invertXaxis) {
        invertXaxis_ = invertXaxis;
        inputManager_.deleteMapping(CameraInput.CHASECAM_MOVELEFT);
        inputManager_.deleteMapping(CameraInput.CHASECAM_MOVERIGHT);
        if (!invertXaxis) {
            inputManager_.addMapping(CameraInput.CHASECAM_MOVELEFT, new MouseAxisTrigger(MouseInput.AXIS_X, true));
            inputManager_.addMapping(CameraInput.CHASECAM_MOVERIGHT, new MouseAxisTrigger(MouseInput.AXIS_X, false));
        } else {
            inputManager_.addMapping(CameraInput.CHASECAM_MOVELEFT, new MouseAxisTrigger(MouseInput.AXIS_X, false));
            inputManager_.addMapping(CameraInput.CHASECAM_MOVERIGHT, new MouseAxisTrigger(MouseInput.AXIS_X, true));
        }
        inputManager_.addListener(this, CameraInput.CHASECAM_MOVELEFT, CameraInput.CHASECAM_MOVERIGHT);
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    /**
     * The method computes position of the camera.
     */
    private void computePosition() {
        float hDistance_ = (distance_) * FastMath.sin((FastMath.PI / 2.0f) - vRotation_);
        pos_.set(
            hDistance_ * FastMath.cos(rotation_),
            distance_ * FastMath.sin(vRotation_),
            hDistance_ * FastMath.sin(rotation_));
        pos_.addLocal(target_.getWorldTranslation());
    }

    /**
     * The method rotates the camera around the target on the horizontal plane.
     * 
     * @param value The angle to rotate.
     */
    private void rotateCamera(final float value) {
        if (canRotate_ == false || enabled_ == false) {
            return;
        }
        rotating_ = true;
        targetRotation_ += value * rotationSpeed_;
    }

    /**
     * The method moves the camera toward or away the target.
     * 
     * @param value The value to move.
     */
    private void zoomCamera(final float value) {
        if (enabled_ == false) {
            return;
        }
        zooming_ = true;
        targetDistance_ += value * zoomSensitivity_;
        if (targetDistance_ > maxDistance_) {
            targetDistance_ = maxDistance_;
        }
        if (targetDistance_ < minDistance_) {
            targetDistance_ = minDistance_;
        }
        if (veryCloseRotation_ == true) {
            if ((targetVRotation_ < minVerticalRotation_) && (targetDistance_ > (minDistance_ + 1.0f))) {
                targetVRotation_ = minVerticalRotation_;
            }
        }
    }

    /**
     * The method rotates the camera around the target on the vertical plane.
     * 
     * @param value The angle to rotate.
     */
    private void vRotateCamera(final float value) {
        if (canRotate_ == false || enabled_ == false) {
            return;
        }
        vRotating_ = true;
        float lastGoodRot = targetVRotation_;
        targetVRotation_ += value * rotationSpeed_;
        if (targetVRotation_ > maxVerticalRotation_) {
            targetVRotation_ = lastGoodRot;
        }
        if (veryCloseRotation_ == true) {
            if ((targetVRotation_ < minVerticalRotation_) && (targetDistance_ > (minDistance_ + 1.0f))) {
                targetVRotation_ = minVerticalRotation_;
            } else if (targetVRotation_ < -FastMath.DEG_TO_RAD * 90) {
                targetVRotation_ = lastGoodRot;
            }
        } else {
            if ((targetVRotation_ < minVerticalRotation_)) {
                targetVRotation_ = lastGoodRot;
            }
        }
    }

    /**
     * The method updates the camera and should only be called internally.
     *
     * @param tpf Time per frame.
     */
    private void updateCamera(final float tpf) {
        if (enabled_ == true) {
            TARGET_LOCATION_.set(target_.getWorldTranslation()).addLocal(lookAtOffset_);
            if (smoothMotion_ == true) {
                // Computation of target direction
                TARGET_DIRECTION_.set(TARGET_LOCATION_).subtractLocal(prevPos_);
                float dist_ = TARGET_DIRECTION_.length();
                // Low pass filtering on the target postition to avoid shaking when physics are enabled.
                if (DEFAULT_OFFSET_DISTANCE_ < dist_) {
                    // Target moves, start chasing.
                    chasing_ = true;
                    // Target moves, start trailing if it has to.
                    if (trailingEnabled_) {
                        trailing_ = true;
                    }
                    // Target moves...
                    targetMoves_ = true;
                } else {
                    // If target was moving, we compute a slight offset in rotation to avoid a rought stop of the cam
                    // We do not if the player is rotationg the cam
                    if (targetMoves_ && !canRotate_) {
                        if (targetRotation_ - rotation_ > trailingRotationInertia_) {
                            targetRotation_ = rotation_ + trailingRotationInertia_;
                        } else if (targetRotation_ - rotation_ < -trailingRotationInertia_) {
                            targetRotation_ = rotation_ - trailingRotationInertia_;
                        }
                    }
                    // Target stops
                    targetMoves_ = false;
                }
                // The user is rotating the cam by dragging the mouse
                if (canRotate_) {
                    // Reseting the trailing lerp factor
                    trailingLerpFactor_ = 0;
                    // Stop trailing user has the control
                    trailing_ = false;
                }
                if (trailingEnabled_ && trailing_) {
                    if (targetMoves_) {
                        // Computation if the inverted direction of the target
                        Vector3f a = TARGET_DIRECTION_.negate().normalizeLocal();
                        // The x unit vector
                        Vector3f b = Vector3f.UNIT_X;
                        // 2d is good enough
                        a.y = 0;
                        // Computation of the rotation angle between the x axis and the trail
                        if (TARGET_DIRECTION_.z > 0) {
                            targetRotation_ = FastMath.TWO_PI - FastMath.acos(a.dot(b));
                        } else {
                            targetRotation_ = FastMath.acos(a.dot(b));
                        }
                        if (targetRotation_ - rotation_ > FastMath.PI || targetRotation_ - rotation_ < -FastMath.PI) {
                            targetRotation_ -= FastMath.TWO_PI;
                        }
                        // If there is an important change in the direction while
                        // trailing reset of the lerp factor to avoid jumpy movements
                        if (targetRotation_ != previousTargetRotation_ && 
                            FastMath.abs(targetRotation_ - previousTargetRotation_) > FastMath.PI / 8) {
                            trailingLerpFactor_ = 0;
                        }
                        previousTargetRotation_ = targetRotation_;
                    }
                    // Computing lerp factor
                    trailingLerpFactor_ = Math.min(trailingLerpFactor_ + tpf * tpf * trailingSensitivity_, 1);
                    // Computing rotation by linear interpolation
                    rotation_ = FastMath.interpolateLinear(trailingLerpFactor_, rotation_, targetRotation_);
                    // If the rotation is near the target rotation we're good, that's over
                    if (targetRotation_ + 0.01f >= rotation_ && targetRotation_ - 0.01f <= rotation_) {
                        trailing_ = false;
                        trailingLerpFactor_ = 0;
                    }
                }
                // Linear interpolation of the distance while chasing
                if (chasing_) {
                    distance_ = temp_.set(TARGET_LOCATION_).subtractLocal(cam_.getLocation()).length();
                    distanceLerpFactor_ = Math.min(distanceLerpFactor_ + (tpf * tpf * chasingSensitivity_ * 0.05f), 1);
                    distance_ = FastMath.interpolateLinear(distanceLerpFactor_, distance_, targetDistance_);
                    if (targetDistance_ + 0.01f >= distance_ && targetDistance_ - 0.01f <= distance_) {
                        distanceLerpFactor_ = 0;
                        chasing_ = false;
                    }
                }
                // Linear interpolation of the distance while zooming
                if (zooming_) {
                    distanceLerpFactor_ = Math.min(distanceLerpFactor_ + (tpf * tpf * zoomSensitivity_), 1);
                    distance_ = FastMath.interpolateLinear(distanceLerpFactor_, distance_, targetDistance_);
                    if (targetDistance_ + 0.1f >= distance_ && targetDistance_ - 0.1f <= distance_) {
                        zooming_ = false;
                        distanceLerpFactor_ = 0;
                    }
                }
                // Linear interpolation of the rotation while rotating horizontally
                if (rotating_) {
                    rotationLerpFactor_ = Math.min(rotationLerpFactor_ + tpf * tpf * rotationSensitivity_, 1);
                    rotation_ = FastMath.interpolateLinear(rotationLerpFactor_, rotation_, targetRotation_);
                    if (targetRotation_ + 0.01f >= rotation_ && targetRotation_ - 0.01f <= rotation_) {
                        rotating_ = false;
                        rotationLerpFactor_ = 0;
                    }
                }
                // Linear interpolation of the rotation while rotating vertically
                if (vRotating_) {
                    vRotationLerpFactor_ = Math.min(vRotationLerpFactor_ + tpf * tpf * rotationSensitivity_, 1);
                    vRotation_ = FastMath.interpolateLinear(vRotationLerpFactor_, vRotation_, targetVRotation_);
                    if (targetVRotation_ + 0.01f >= vRotation_ && targetVRotation_ - 0.01f <= vRotation_) {
                        vRotating_ = false;
                        vRotationLerpFactor_ = 0;
                    }
                }
                // Computing the position
                computePosition();
                // Setting the position at last
                cam_.setLocation(pos_.addLocal(lookAtOffset_));
            } else {
                // Easy no smooth motion
                vRotation_ = targetVRotation_;
                rotation_ = targetRotation_;
                distance_ = targetDistance_;
                computePosition();
                cam_.setLocation(pos_.addLocal(lookAtOffset_));
            }
            // Keeping track on the previous position of the target
            prevPos_.set(TARGET_LOCATION_);
            // The cam looks at the target
            cam_.lookAt(TARGET_LOCATION_, initialUpVec_);
        }
    }
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************

}
