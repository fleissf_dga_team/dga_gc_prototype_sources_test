/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.effekt;

import com.jme3.audio.AudioNode;
import com.jme3.effect.ParticleEmitter;
import org.dga.client.DgaJmeEffect;

/**
 * The interface of fire effects provided by a shooting game object.
 *
 * @extends DgaJmeEffect
 * @pattern Adapter
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 24.08.2018
 */
public interface FireEffect extends DgaJmeEffect {
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the flame effect.
     * 
     * @return ParticleEmitter The flame effect.
     */
    public ParticleEmitter getFlame();
    
    /**
     * The method returns the flash effect.
     * 
     * @return ParticleEmitter The flash effect.
     */
    public ParticleEmitter getFlash();
    
    /**
     * The method returns the smoke effect.
     * 
     * @return ParticleEmitter The smoke effect.
     */
    public ParticleEmitter getSmoke();
    
    /**
     * The method returns the fire sound.
     * 
     * @return ParticleEmitter The fire sound.
     */
    public AudioNode getFireSound();
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************************************************************
}