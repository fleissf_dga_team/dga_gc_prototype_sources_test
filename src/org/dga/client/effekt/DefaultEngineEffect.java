/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.effekt;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioData;
import com.jme3.audio.AudioNode;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.shapes.EmitterSphereShape;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import org.dga.client.DgaJmeCommonConstant;
import org.dga.client.DgaJmeUtils;

/**
 * The default implementation of effects provided by an engine or motor that is 
 * a machine designed to convert one form of energy into mechanical energy.
 * 
 * @extends AbstractEngineEffect
 * @implements EngineEffect
 * @pattern Adapter
 * @pattern Template Method
 * @pattern Builder
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 26.08.2018
 */
public final class DefaultEngineEffect extends AbstractEngineEffect implements EngineEffect {
    private ParticleEmitter peExhaustOne_ = null;
    private ParticleEmitter peExhaustTwo_ = null;
    private ParticleEmitter peBlastOne_ = null;
    private ParticleEmitter peBlastTwo_ = null;
    private AudioNode anodeStanding_ = null;
    private AudioNode anodeRunning_ = null;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new instance.
     */
    public DefaultEngineEffect() {
        super();
    }
    
    /**
     * The constructor creates a new instance with the supplied properties.
     * 
     * @param engineEffectNode The engine effect's node.
     */
    public DefaultEngineEffect(final Node engineEffectNode) {
        super(engineEffectNode);
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns a new instance of the builder for an engine effect.
     * 
     * @param assetManager The game application's asset manager.
     * @return EngineBuilder A new instance of the builder for an engine effect.
     */
    public static final EngineBuilder newBuilder(final AssetManager assetManager) {
        return new EngineBuilder(assetManager);
    }
    
    /**
     * The method returns the first exhaust point.
     * 
     * @return ParticleEmitter The first exhaust point.
     */
    @Override
    public ParticleEmitter getExhaustOne() {
        return peExhaustOne_;
    }
    
    /**
     * The method returns the second exhaust point.
     * 
     * @return ParticleEmitter The second exhaust point.
     */
    @Override
    public ParticleEmitter getExhaustTwo() {
        return peExhaustTwo_;
    }
    
    /**
     * The method returns the first blast point.
     * 
     * @return ParticleEmitter The first blast point.
     */
    @Override
    public ParticleEmitter getBlastOne() {
        return peBlastOne_;
    }
    
    /**
     * The method returns the second blast point.
     * 
     * @return ParticleEmitter The second blast point.
     */
    @Override
    public ParticleEmitter getBlastTwo() {
        return peBlastTwo_;
    }
    
    /**
     * The method returns the standing sound.
     * 
     * @return AudioNode The standing sound.
     */
    @Override
    public AudioNode getStandingSound() {
        return anodeStanding_;
    }
    
    /**
     * The method returns the running sound.
     * 
     * @return AudioNode The running sound.
     */
    @Override
    public AudioNode getRunningSound() {
        return anodeRunning_;
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    /**
    * The builder for an engine effect provided by an engine or motor that is 
    * a machine designed to convert one form of energy into mechanical energy.
    * 
    * @extends java.lang.Object
    * @pattern Builder
    * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
    * @version 0.1
    * @date 26.08.2018
    */
    public static final class EngineBuilder extends Object {
        private static final int MAX_PARTICLES_NUMBER_ = 
            DgaJmeCommonConstant.OPTIMAL_MAXIMUM_PARTICLES_NUMBER.getIntValue();
        //
        private static final int EXHAUST_PARTICLES_NUMBER_ = 128;
        private static final ColorRGBA EXHAUST_START_COLOR_ = ColorRGBA.Black;
        private static final ColorRGBA EXHAUST_END_COLOR_ = ColorRGBA.BlackNoAlpha;
        private static final float EXHAUST_START_SIZE_ = 1.3f;
        private static final float EXHAUST_END_SIZE_ = 2.0f;
        private static final float EXHAUST_EMITTER_RADIUS_ = 2.0f;
        private static final Vector3f EXHAUST_GRAVITY_ = new Vector3f(0f, 0f, 0f);
        private static final float EXHAUST_LOW_LIFE_ = 0.4f;
        private static final float EXHAUST_HIGH_LIFE_ = 0.5f;
        private static final int EXHAUST_IMAGES_X_ = 2;
        private static final int EXHAUST_IMAGES_Y_ = 2;
        private static final Vector3f EXHAUST_LOCAL_TRANSLATION_ = new Vector3f(0f, 0f, 0f);
        //
        private static final int BLAST_PARTICLES_NUMBER_ = 128;
        private static final ColorRGBA BLAST_START_COLOR_ = ColorRGBA.Black;
        private static final ColorRGBA BLAST_END_COLOR_ = ColorRGBA.BlackNoAlpha;
        private static final float BLAST_START_SIZE_ = 1.3f;
        private static final float BLAST_END_SIZE_ = 2.0f;
        private static final float BLAST_EMITTER_RADIUS_ = 2.0f;
        private static final Vector3f BLAST_GRAVITY_ = new Vector3f(0f, 0f, 0f);
        private static final float BLAST_LOW_LIFE_ = 0.4f;
        private static final float BLAST_HIGH_LIFE_ = 0.5f;
        private static final int BLAST_IMAGES_X_ = 15;
        private static final int BLAST_IMAGES_Y_ = 1;
        private static final Vector3f BLAST_LOCAL_TRANSLATION_ = new Vector3f(0f, 0f, 0f);
        //
        private static final boolean STANDING_SOUND_IS_POSITIONAL_ = true;
        private static final float STANDING_SOUND_VOLUME_ = 
            DgaJmeCommonConstant.DEFAULT_VOLUME_LEVEL.getFloatValue();
        private static final boolean STANDING_SOUND_IS_LOOPING_ = false;
        //
        private static final boolean RUNNING_SOUND_IS_POSITIONAL_ = true;
        private static final float RUNNING_SOUND_VOLUME_ = 
            DgaJmeCommonConstant.DEFAULT_VOLUME_LEVEL.getFloatValue();
        private static final boolean RUNNING_SOUND_IS_LOOPING_ = true;
        //
        private AssetManager assetManager_ = null;
        private final Node NODE_ENGINE_EFFECT_ = new Node("EngineEffect");
        private final DefaultEngineEffect ENGINE_EFFECT_ = 
            new DefaultEngineEffect(NODE_ENGINE_EFFECT_);
        //
        // ************************* Constructors ******************************
        //
        /**
         * The constructor creates a new instance.
         * 
         * @param assetManager The game application's asset manager.
         */
        private EngineBuilder(final AssetManager assetManager) {
            super();
            assetManager_ = assetManager;
        }
        //
        // ************************* Public Methods ****************************
        //
        /**
         * The method adds the first exhaust point into the engine effect.
         * 
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder addExhaustOne() {
            return this.addExhaustOne(false, EXHAUST_PARTICLES_NUMBER_);
        }
        
        /**
         * The method adds the first exhaust point into the engine effect.
         * 
         * @param isPointSprite The flag whether the exhaust is made of points or of triangles.
         * @param particlesNumber The number of particles.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder addExhaustOne(final boolean isPointSprite, final int particlesNumber) {
            ENGINE_EFFECT_.peExhaustOne_ = new ParticleEmitter(
                "ExhaustOne",
                DgaJmeUtils.getParticleEmitterType(isPointSprite),
                (particlesNumber > 0 && particlesNumber <= MAX_PARTICLES_NUMBER_ ? 
                    particlesNumber : EXHAUST_PARTICLES_NUMBER_));
            ENGINE_EFFECT_.peExhaustOne_.setSelectRandomImage(true);
            ENGINE_EFFECT_.peExhaustOne_.setRandomAngle(true);
            ENGINE_EFFECT_.peExhaustOne_.getParticleInfluencer().setVelocityVariation(1.0f);
            //ENGINE_EFFECT_.peExhaustOne_.setStartColor(EXHAUST_START_COLOR_);
            //ENGINE_EFFECT_.peExhaustOne_.setEndColor(EXHAUST_END_COLOR_);
            //ENGINE_EFFECT_.peExhaustOne_.setStartSize(EXHAUST_START_SIZE_);
            //ENGINE_EFFECT_.peExhaustOne_.setEndSize(EXHAUST_END_SIZE_);
            //ENGINE_EFFECT_.peExhaustOne_.setShape(new EmitterSphereShape(Vector3f.ZERO, EXHAUST_EMITTER_RADIUS_));
            //ENGINE_EFFECT_.peExhaustOne_.setParticlesPerSec(0f);
            //ENGINE_EFFECT_.peExhaustOne_.setParticlesPerSec(0f);
            //ENGINE_EFFECT_.peExhaustOne_.setGravity(EXHAUST_GRAVITY_);
            //ENGINE_EFFECT_.peExhaustOne_.setLowLife(EXHAUST_LOW_LIFE_);
            //ENGINE_EFFECT_.peExhaustOne_.setHighLife(EXHAUST_HIGH_LIFE_);
            //ENGINE_EFFECT_.peExhaustOne_.getParticleInfluencer().setInitialVelocity(new Vector3f(0f, 5f, 0f));
            ENGINE_EFFECT_.peExhaustOne_.getParticleInfluencer().setVelocityVariation(1);
            ENGINE_EFFECT_.peExhaustOne_.setImagesX(EXHAUST_IMAGES_X_);
            ENGINE_EFFECT_.peExhaustOne_.setImagesY(EXHAUST_IMAGES_Y_);
            //ENGINE_EFFECT_.peExhaustOne_.setLocalTranslation(EXHAUST_LOCAL_TRANSLATION_);
            Material matExhaustOne_ = new Material(assetManager_, "Common/MatDefs/Misc/Particle.j3md");
            matExhaustOne_.setTexture("Texture", assetManager_.loadTexture("Effects/Smoke/Smoke3.png"));
            matExhaustOne_.setBoolean("PointSprite", isPointSprite);
            ENGINE_EFFECT_.peExhaustOne_.setMaterial(matExhaustOne_);
            NODE_ENGINE_EFFECT_.attachChild(ENGINE_EFFECT_.peExhaustOne_);
            ENGINE_EFFECT_.addVisualEffectEmit(ENGINE_EFFECT_.peExhaustOne_);
            ENGINE_EFFECT_.addVisualEffectKill(ENGINE_EFFECT_.peExhaustOne_);
            return this;
        }
        
        /**
         * The method sets the first exhaust point's start size.
         * 
         * @param exhaustStartSize The exhaust's start size.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setExhaustOneStartSize(final float exhaustStartSize) {
            if (ENGINE_EFFECT_.peExhaustOne_ == null) {
                this.addExhaustOne(false, EXHAUST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peExhaustOne_.setStartSize(exhaustStartSize);
            return this;
        }
        
        /**
         * The method sets the first exhaust point's end size.
         * 
         * @param exhaustEndSize The exhaust's end size.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setExhaustOneEndSize(final float exhaustEndSize) {
            if (ENGINE_EFFECT_.peExhaustOne_ == null) {
                this.addExhaustOne(false, EXHAUST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peExhaustOne_.setEndSize(exhaustEndSize);
            return this;
        }
        
        /**
         * The method sets the first exhaust point's emitter radius.
         * 
         * @param exhaustEmitterRadius The exhaust's emitter radius.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setExhaustOneEmitterRadius(final float exhaustEmitterRadius) {
            if (ENGINE_EFFECT_.peExhaustOne_ == null) {
                this.addExhaustOne(false, EXHAUST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peExhaustOne_.setShape(
                new EmitterSphereShape(Vector3f.ZERO, exhaustEmitterRadius));
            return this;
        }
        
        /**
         * The method sets the first exhaust point's gravity.
         * 
         * @param exhaustGravity The exhaust's gravity.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setExhaustOneGravity(final Vector3f exhaustGravity) {
            if (ENGINE_EFFECT_.peExhaustOne_ == null) {
                this.addExhaustOne(false, EXHAUST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peExhaustOne_.setGravity(exhaustGravity);
            return this;
        }
        
        /**
         * The method sets the first exhaust point's low life.
         * 
         * @param exhaustLowLife The exhaust's low life.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setExhaustOneLowLife(final float exhaustLowLife) {
            if (ENGINE_EFFECT_.peExhaustOne_ == null) {
                this.addExhaustOne(false, EXHAUST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peExhaustOne_.setLowLife(exhaustLowLife);
            return this;
        }
        
        /**
         * The method set the first exhaust point's high life.
         * 
         * @param exhaustHighLife The exhaust's high life.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setExhaustOneHighLife(final float exhaustHighLife) {
            if (ENGINE_EFFECT_.peExhaustOne_ == null) {
                this.addExhaustOne(false, EXHAUST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peExhaustOne_.setHighLife(exhaustHighLife);
            return this;
        }
        
        /**
         * The method sets the first exhaust point's quantity of images along the X axis.
         * 
         * @param exhaustImagesX The exhaust's quantity of images along the X axis.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setExhaustOneImagesX(final int exhaustImagesX) {
            if (ENGINE_EFFECT_.peExhaustOne_ == null) {
                this.addExhaustOne(false, EXHAUST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peExhaustOne_.setImagesX(exhaustImagesX);
            return this;
        }
        
        /**
         * The method sets the first exhaust point's quantity of images along the Y axis.
         * 
         * @param exhaustImagesY The exhaust's quantity of images along the Y axis.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setExhaustOneImagesY(final int exhaustImagesY) {
            if (ENGINE_EFFECT_.peExhaustOne_ == null) {
                this.addExhaustOne(false, EXHAUST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peExhaustOne_.setImagesY(exhaustImagesY);
            return this;
        }
        
        /**
         * The method sets the first exhaust point's local translation.
         * 
         * @param exhaustLocalTranslation The exhaust's local translation.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setExhaustOneLocalTranslation(final Vector3f exhaustLocalTranslation) {
            if (ENGINE_EFFECT_.peExhaustOne_ == null) {
                this.addExhaustOne(false, EXHAUST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peExhaustOne_.setLocalTranslation(exhaustLocalTranslation);
            return this;
        }
        //
        // *********************************************************************
        //
        /**
         * The method adds the second exhaust point into the engine effect.
         * 
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder addExhaustTwo() {
            return this.addExhaustTwo(false, EXHAUST_PARTICLES_NUMBER_);
        }
        
        /**
         * The method adds the second exhaust point into the engine effect.
         * 
         * @param isPointSprite The flag whether the exhaust is made of points or of triangles.
         * @param particlesNumber The number of particles.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder addExhaustTwo(final boolean isPointSprite, final int particlesNumber) {
            ENGINE_EFFECT_.peExhaustTwo_ = new ParticleEmitter(
                "ExhaustTwo",
                DgaJmeUtils.getParticleEmitterType(isPointSprite),
                (particlesNumber > 0 && particlesNumber <= MAX_PARTICLES_NUMBER_ ? 
                    particlesNumber : 
                    EXHAUST_PARTICLES_NUMBER_));
            ENGINE_EFFECT_.peExhaustTwo_.setSelectRandomImage(true);
            ENGINE_EFFECT_.peExhaustTwo_.setRandomAngle(true);
            ENGINE_EFFECT_.peExhaustTwo_.getParticleInfluencer().setVelocityVariation(1.0f);
            //ENGINE_EFFECT_.peExhaustTwo_.setStartColor(EXHAUST_START_COLOR_);
            //ENGINE_EFFECT_.peExhaustTwo_.setEndColor(EXHAUST_END_COLOR_);
            //ENGINE_EFFECT_.peExhaustTwo_.setStartSize(EXHAUST_START_SIZE_);
            //ENGINE_EFFECT_.peExhaustTwo_.setEndSize(EXHAUST_END_SIZE_);
            //ENGINE_EFFECT_.peExhaustTwo_.setShape(new EmitterSphereShape(Vector3f.ZERO, EXHAUST_EMITTER_RADIUS_));
            //ENGINE_EFFECT_.peExhaustTwo_.setParticlesPerSec(0f);
            //ENGINE_EFFECT_.peExhaustTwo_.setParticlesPerSec(0f);
            //ENGINE_EFFECT_.peExhaustTwo_.setGravity(EXHAUST_GRAVITY_);
            //ENGINE_EFFECT_.peExhaustTwo_.setLowLife(EXHAUST_LOW_LIFE_);
            //ENGINE_EFFECT_.peExhaustTwo_.setHighLife(EXHAUST_HIGH_LIFE_);
            //ENGINE_EFFECT_.peExhaustTwo_.getParticleInfluencer().setInitialVelocity(new Vector3f(0f, 5f, 0f));
            ENGINE_EFFECT_.peExhaustTwo_.getParticleInfluencer().setVelocityVariation(1);
            ENGINE_EFFECT_.peExhaustTwo_.setImagesX(EXHAUST_IMAGES_X_);
            ENGINE_EFFECT_.peExhaustTwo_.setImagesY(EXHAUST_IMAGES_Y_);
            //ENGINE_EFFECT_.peExhaustTwo_.setLocalTranslation(EXHAUST_LOCAL_TRANSLATION_);
            Material matExhaustTwo_ = new Material(assetManager_, "Common/MatDefs/Misc/Particle.j3md");
            matExhaustTwo_.setTexture("Texture", assetManager_.loadTexture("Effects/Smoke/Smoke3.png"));
            matExhaustTwo_.setBoolean("PointSprite", isPointSprite);
            ENGINE_EFFECT_.peExhaustTwo_.setMaterial(matExhaustTwo_);
            NODE_ENGINE_EFFECT_.attachChild(ENGINE_EFFECT_.peExhaustTwo_);
            ENGINE_EFFECT_.addVisualEffectEmit(ENGINE_EFFECT_.peExhaustTwo_);
            ENGINE_EFFECT_.addVisualEffectKill(ENGINE_EFFECT_.peExhaustTwo_);
            return this;
        }
        
        /**
         * The method sets the second exhaust point's start size.
         * 
         * @param exhaustStartSize The exhaust's start size.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setExhaustTwoStartSize(final float exhaustStartSize) {
            if (ENGINE_EFFECT_.peExhaustTwo_ == null) {
                this.addExhaustTwo(false, EXHAUST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peExhaustTwo_.setStartSize(exhaustStartSize);
            return this;
        }
        
        /**
         * The method sets the second exhaust point's end size.
         * 
         * @param exhaustEndSize The exhaust's end size.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setExhaustTwoEndSize(final float exhaustEndSize) {
            if (ENGINE_EFFECT_.peExhaustTwo_ == null) {
                this.addExhaustTwo(false, EXHAUST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peExhaustTwo_.setEndSize(exhaustEndSize);
            return this;
        }
        
        /**
         * The method sets the second exhaust point's emitter radius.
         * 
         * @param exhaustEmitterRadius The exhaust's emitter radius.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setExhaustTwoEmitterRadius(final float exhaustEmitterRadius) {
            if (ENGINE_EFFECT_.peExhaustTwo_ == null) {
                this.addExhaustTwo(false, EXHAUST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peExhaustTwo_.setShape(
                new EmitterSphereShape(Vector3f.ZERO, exhaustEmitterRadius));
            return this;
        }
        
        /**
         * The method sets the second exhaust point's gravity.
         * 
         * @param exhaustGravity The exhaust's gravity.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setExhaustTwoGravity(final Vector3f exhaustGravity) {
            if (ENGINE_EFFECT_.peExhaustTwo_ == null) {
                this.addExhaustTwo(false, EXHAUST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peExhaustTwo_.setGravity(exhaustGravity);
            return this;
        }
        
        /**
         * The method sets the second exhaust point's low life.
         * 
         * @param exhaustLowLife The exhaust's low life.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setExhaustTwoLowLife(final float exhaustLowLife) {
            if (ENGINE_EFFECT_.peExhaustTwo_ == null) {
                this.addExhaustTwo(false, EXHAUST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peExhaustTwo_.setLowLife(exhaustLowLife);
            return this;
        }
        
        /**
         * The method set the second exhaust point's high life.
         * 
         * @param exhaustHighLife The exhaust's high life.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setExhaustTwoHighLife(final float exhaustHighLife) {
            if (ENGINE_EFFECT_.peExhaustTwo_ == null) {
                this.addExhaustTwo(false, EXHAUST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peExhaustTwo_.setHighLife(exhaustHighLife);
            return this;
        }
        
        /**
         * The method sets the second exhaust point's quantity of images along the X axis.
         * 
         * @param exhaustImagesX The exhaust's quantity of images along the X axis.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setExhaustTwoImagesX(final int exhaustImagesX) {
            if (ENGINE_EFFECT_.peExhaustTwo_ == null) {
                this.addExhaustTwo(false, EXHAUST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peExhaustTwo_.setImagesX(exhaustImagesX);
            return this;
        }
        
        /**
         * The method sets the second exhaust point's quantity of images along the Y axis.
         * 
         * @param exhaustImagesY The exhaust's quantity of images along the Y axis.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setExhaustTwoImagesY(final int exhaustImagesY) {
            if (ENGINE_EFFECT_.peExhaustTwo_ == null) {
                this.addExhaustTwo(false, EXHAUST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peExhaustTwo_.setImagesY(exhaustImagesY);
            return this;
        }
        
        /**
         * The method sets the second exhaust point's local translation.
         * 
         * @param exhaustLocalTranslation The exhaust's local translation.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setExhaustTwoLocalTranslation(final Vector3f exhaustLocalTranslation) {
            if (ENGINE_EFFECT_.peExhaustTwo_ == null) {
                this.addExhaustTwo(false, EXHAUST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peExhaustTwo_.setLocalTranslation(exhaustLocalTranslation);
            return this;
        }
        //
        // *********************************************************************
        //
        /**
         * The method adds the first blast point into the engine effect.
         * 
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder addBlastOne() {
            return this.addBlastOne(false, BLAST_PARTICLES_NUMBER_);
        }
        
        /**
         * The method adds the first blast point into the engine effect.
         * 
         * @param isPointSprite The flag whether the blast is made of points or of triangles.
         * @param particlesNumber The number of particles.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder addBlastOne(final boolean isPointSprite, final int particlesNumber) {
            ENGINE_EFFECT_.peBlastOne_ = new ParticleEmitter(
                "BlastOne",
                DgaJmeUtils.getParticleEmitterType(isPointSprite),
                (particlesNumber > 0 && particlesNumber <= MAX_PARTICLES_NUMBER_ ? 
                    particlesNumber : 
                    BLAST_PARTICLES_NUMBER_));
            ENGINE_EFFECT_.peBlastOne_.setSelectRandomImage(true);
            ENGINE_EFFECT_.peBlastOne_.setRandomAngle(true);
            ENGINE_EFFECT_.peBlastOne_.getParticleInfluencer().setVelocityVariation(1.0f);
            //ENGINE_EFFECT_.peBlastOne_.setStartColor(BLAST_START_COLOR_);
            //ENGINE_EFFECT_.peBlastOne_.setEndColor(BLAST_END_COLOR_);
            //ENGINE_EFFECT_.peBlastOne_.setStartSize(BLAST_START_SIZE_);
            //ENGINE_EFFECT_.peBlastOne_.setEndSize(BLAST_END_SIZE_);
            //ENGINE_EFFECT_.peBlastOne_.setShape(new EmitterSphereShape(Vector3f.ZERO, BLAST_EMITTER_RADIUS_));
            //ENGINE_EFFECT_.peBlastOne_.setParticlesPerSec(0f);
            //ENGINE_EFFECT_.peBlastOne_.setParticlesPerSec(0f);
            //ENGINE_EFFECT_.peBlastOne_.setGravity(BLAST_GRAVITY_);
            //ENGINE_EFFECT_.peBlastOne_.setLowLife(BLAST_LOW_LIFE_);
            //ENGINE_EFFECT_.peBlastOne_.setHighLife(BLAST_HIGH_LIFE_);
            //ENGINE_EFFECT_.peBlastOne_.getParticleInfluencer().setInitialVelocity(new Vector3f(0f, 5f, 0f));
            ENGINE_EFFECT_.peBlastOne_.getParticleInfluencer().setVelocityVariation(1);
            ENGINE_EFFECT_.peBlastOne_.setImagesX(BLAST_IMAGES_X_);
            ENGINE_EFFECT_.peBlastOne_.setImagesY(BLAST_IMAGES_Y_);
            //ENGINE_EFFECT_.peBlastOne_.setLocalTranslation(BLAST_LOCAL_TRANSLATION_);
            Material matBlastOne_ = new Material(assetManager_, "Common/MatDefs/Misc/Particle.j3md");
            matBlastOne_.setTexture("Texture", assetManager_.loadTexture("Effects/Smoke/Smoke1.png"));
            matBlastOne_.setBoolean("PointSprite", isPointSprite);
            ENGINE_EFFECT_.peBlastOne_.setMaterial(matBlastOne_);
            NODE_ENGINE_EFFECT_.attachChild(ENGINE_EFFECT_.peBlastOne_);
            ENGINE_EFFECT_.addVisualEffectEmit(ENGINE_EFFECT_.peBlastOne_);
            ENGINE_EFFECT_.addVisualEffectKill(ENGINE_EFFECT_.peBlastOne_);
            return this;
        }
        
        /**
         * The method sets the first blast point's start size.
         * 
         * @param blastStartSize The blast's start size.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setBlastOneStartSize(final float blastStartSize) {
            if (ENGINE_EFFECT_.peBlastOne_ == null) {
                this.addBlastOne(false, BLAST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peBlastOne_.setStartSize(blastStartSize);
            return this;
        }
        
        /**
         * The method sets the first blast point's end size.
         * 
         * @param blastEndSize The blast's end size.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setBlastOneEndSize(final float blastEndSize) {
            if (ENGINE_EFFECT_.peBlastOne_ == null) {
                this.addBlastOne(false, BLAST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peBlastOne_.setEndSize(blastEndSize);
            return this;
        }
        
        /**
         * The method sets the first blast point's emitter radius.
         * 
         * @param blastEmitterRadius The blast's emitter radius.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setBlastOneEmitterRadius(final float blastEmitterRadius) {
            if (ENGINE_EFFECT_.peBlastOne_ == null) {
                this.addBlastOne(false, BLAST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peBlastOne_.setShape(
                new EmitterSphereShape(Vector3f.ZERO, blastEmitterRadius));
            return this;
        }
        
        /**
         * The method sets the first blast point's gravity.
         * 
         * @param blastGravity The blast's gravity.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setBlastOneGravity(final Vector3f blastGravity) {
            if (ENGINE_EFFECT_.peBlastOne_ == null) {
                this.addBlastOne(false, BLAST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peBlastOne_.setGravity(blastGravity);
            return this;
        }
        
        /**
         * The method sets the first blast point's low life.
         * 
         * @param blastLowLife The blast's low life.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setBlastOneLowLife(final float blastLowLife) {
            if (ENGINE_EFFECT_.peBlastOne_ == null) {
                this.addBlastOne(false, BLAST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peBlastOne_.setLowLife(blastLowLife);
            return this;
        }
        
        /**
         * The method set the first blast point's high life.
         * 
         * @param blastHighLife The blast's high life.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setBlastOneHighLife(final float blastHighLife) {
            if (ENGINE_EFFECT_.peBlastOne_ == null) {
                this.addBlastOne(false, BLAST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peBlastOne_.setHighLife(blastHighLife);
            return this;
        }
        
        /**
         * The method sets the first blast point's quantity of images along the X axis.
         * 
         * @param blastImagesX The blast's quantity of images along the X axis.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setBlastOneImagesX(final int blastImagesX) {
            if (ENGINE_EFFECT_.peBlastOne_ == null) {
                this.addBlastOne(false, BLAST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peBlastOne_.setImagesX(blastImagesX);
            return this;
        }
        
        /**
         * The method sets the first blast point's quantity of images along the Y axis.
         * 
         * @param blastImagesY The blast's quantity of images along the Y axis.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setBlastOneImagesY(final int blastImagesY) {
            if (ENGINE_EFFECT_.peBlastOne_ == null) {
                this.addBlastOne(false, BLAST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peBlastOne_.setImagesY(blastImagesY);
            return this;
        }
        
        /**
         * The method sets the first blast point's local translation.
         * 
         * @param blastLocalTranslation The blast's local translation.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setBlastOneLocalTranslation(final Vector3f blastLocalTranslation) {
            if (ENGINE_EFFECT_.peBlastOne_ == null) {
                this.addBlastOne(false, BLAST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peBlastOne_.setLocalTranslation(blastLocalTranslation);
            return this;
        }
        //
        // *********************************************************************
        //
        /**
         * The method adds the second blast point into the engine effect.
         * 
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder addBlastTwo() {
            return this.addBlastTwo(false, BLAST_PARTICLES_NUMBER_);
        }
        
        /**
         * The method adds the second blast point into the engine effect.
         * 
         * @param isPointSprite The flag whether the BlastOne is made of points or of triangles.
         * @param particlesNumber The number of particles.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder addBlastTwo(final boolean isPointSprite, final int particlesNumber) {
            ENGINE_EFFECT_.peBlastTwo_ = new ParticleEmitter(
                "BlastTwo",
                DgaJmeUtils.getParticleEmitterType(isPointSprite),
                (particlesNumber > 0 && particlesNumber <= MAX_PARTICLES_NUMBER_ ? 
                    particlesNumber : 
                    BLAST_PARTICLES_NUMBER_));
            ENGINE_EFFECT_.peBlastTwo_.setSelectRandomImage(true);
            ENGINE_EFFECT_.peBlastTwo_.setRandomAngle(true);
            ENGINE_EFFECT_.peBlastTwo_.getParticleInfluencer().setVelocityVariation(1.0f);
            ENGINE_EFFECT_.peBlastTwo_.setStartColor(BLAST_START_COLOR_);
            ENGINE_EFFECT_.peBlastTwo_.setEndColor(BLAST_END_COLOR_);
            //ENGINE_EFFECT_.peBlastTwo_.setStartSize(BLAST_START_SIZE_);
            //ENGINE_EFFECT_.peBlastTwo_.setEndSize(BLAST_END_SIZE_);
            //ENGINE_EFFECT_.peBlastTwo_.setShape(new EmitterSphereShape(Vector3f.ZERO, BLAST_EMITTER_RADIUS_));
            //ENGINE_EFFECT_.peBlastTwo_.setParticlesPerSec(0f);
            //ENGINE_EFFECT_.peBlastTwo_.setParticlesPerSec(0f);
            //ENGINE_EFFECT_.peBlastTwo_.setGravity(BLAST_GRAVITY_);
            //ENGINE_EFFECT_.peBlastTwo_.setLowLife(BLAST_LOW_LIFE_);
            //ENGINE_EFFECT_.peBlastTwo_.setHighLife(BLAST_HIGH_LIFE_);
            //ENGINE_EFFECT_.peBlastTwo_.getParticleInfluencer().setInitialVelocity(new Vector3f(0f, 5f, 0f));
            ENGINE_EFFECT_.peBlastTwo_.getParticleInfluencer().setVelocityVariation(1);
            ENGINE_EFFECT_.peBlastTwo_.setImagesX(BLAST_IMAGES_X_);
            ENGINE_EFFECT_.peBlastTwo_.setImagesY(BLAST_IMAGES_Y_);
            //ENGINE_EFFECT_.peBlastTwo_.setLocalTranslation(BLAST_LOCAL_TRANSLATION_);
            Material matBlastTwo_ = new Material(assetManager_, "Common/MatDefs/Misc/Particle.j3md");
            matBlastTwo_.setTexture("Texture", assetManager_.loadTexture("Effects/Smoke/Smoke1.png"));
            matBlastTwo_.setBoolean("PointSprite", isPointSprite);
            ENGINE_EFFECT_.peBlastTwo_.setMaterial(matBlastTwo_);
            NODE_ENGINE_EFFECT_.attachChild(ENGINE_EFFECT_.peBlastTwo_);
            ENGINE_EFFECT_.addVisualEffectEmit(ENGINE_EFFECT_.peBlastTwo_);
            ENGINE_EFFECT_.addVisualEffectKill(ENGINE_EFFECT_.peBlastTwo_);
            return this;
        }
        
        /**
         * The method sets the second blast point's start size.
         * 
         * @param blastStartSize The blast's start size.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setBlastTwoStartSize(final float blastStartSize) {
            if (ENGINE_EFFECT_.peBlastTwo_ == null) {
                this.addBlastTwo(false, BLAST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peBlastTwo_.setStartSize(blastStartSize);
            return this;
        }
        
        /**
         * The method sets the second blast point's end size.
         * 
         * @param blastEndSize The blast's end size.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setBlastTwoEndSize(final float blastEndSize) {
            if (ENGINE_EFFECT_.peBlastTwo_ == null) {
                this.addBlastTwo(false, BLAST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peBlastTwo_.setEndSize(blastEndSize);
            return this;
        }
        
        /**
         * The method sets the second blast point's emitter radius.
         * 
         * @param blastEmitterRadius The blast's emitter radius.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setBlastTwoEmitterRadius(final float blastEmitterRadius) {
            if (ENGINE_EFFECT_.peBlastTwo_ == null) {
                this.addBlastTwo(false, BLAST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peBlastTwo_.setShape(
                new EmitterSphereShape(Vector3f.ZERO, blastEmitterRadius));
            return this;
        }
        
        /**
         * The method sets the second blast point's gravity.
         * 
         * @param blastGravity The blast's gravity.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setBlastTwoGravity(final Vector3f blastGravity) {
            if (ENGINE_EFFECT_.peBlastTwo_ == null) {
                this.addBlastTwo(false, BLAST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peBlastTwo_.setGravity(blastGravity);
            return this;
        }
        
        /**
         * The method sets the second blast point's low life.
         * 
         * @param blastLowLife The blast's low life.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setBlastTwoLowLife(final float blastLowLife) {
            if (ENGINE_EFFECT_.peBlastTwo_ == null) {
                this.addBlastTwo(false, BLAST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peBlastTwo_.setLowLife(blastLowLife);
            return this;
        }
        
        /**
         * The method set the second blast point's high life.
         * 
         * @param blastHighLife The blast's high life.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setBlastTwoHighLife(final float blastHighLife) {
            if (ENGINE_EFFECT_.peBlastTwo_ == null) {
                this.addBlastTwo(false, BLAST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peBlastTwo_.setHighLife(blastHighLife);
            return this;
        }
        
        /**
         * The method sets the second blast point's quantity of images along the X axis.
         * 
         * @param blastImagesX The blast's quantity of images along the X axis.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setBlastTwoImagesX(final int blastImagesX) {
            if (ENGINE_EFFECT_.peBlastTwo_ == null) {
                this.addBlastTwo(false, BLAST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peBlastTwo_.setImagesX(blastImagesX);
            return this;
        }
        
        /**
         * The method sets the second blast point's quantity of images along the Y axis.
         * 
         * @param blastImagesY The blast's quantity of images along the Y axis.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setBlastTwoImagesY(final int blastImagesY) {
            if (ENGINE_EFFECT_.peBlastTwo_ == null) {
                this.addBlastTwo(false, BLAST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peBlastTwo_.setImagesY(blastImagesY);
            return this;
        }
        
        /**
         * The method sets the second blast point's local translation.
         * 
         * @param blastLocalTranslation The blast's local translation.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setBlastTwoLocalTranslation(final Vector3f blastLocalTranslation) {
            if (ENGINE_EFFECT_.peBlastTwo_ == null) {
                this.addBlastTwo(false, BLAST_PARTICLES_NUMBER_);
            }
            ENGINE_EFFECT_.peBlastTwo_.setLocalTranslation(blastLocalTranslation);
            return this;
        }
        //
        // *********************************************************************
        //
        /**
         * The method adds the standing sound into the engine effect.
         * 
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder addStandingSound() {
            // If the method addStandingSound(AudioNode) was called previously.
            this.cleanStandingSound();
            //
            ENGINE_EFFECT_.anodeStanding_ = new AudioNode(
                assetManager_, "Sounds/EngineStanding.wav", AudioData.DataType.Buffer);
            ENGINE_EFFECT_.anodeStanding_.setPositional(STANDING_SOUND_IS_POSITIONAL_);
            ENGINE_EFFECT_.anodeStanding_.setVolume(STANDING_SOUND_VOLUME_);
            ENGINE_EFFECT_.anodeStanding_.setLooping(STANDING_SOUND_IS_LOOPING_);
            NODE_ENGINE_EFFECT_.attachChild(ENGINE_EFFECT_.anodeStanding_);
            ENGINE_EFFECT_.addAudioEffectStart(ENGINE_EFFECT_.anodeStanding_);
            ENGINE_EFFECT_.addAudioEffectStop(ENGINE_EFFECT_.anodeStanding_);
            return this;
        }
        
        /**
         * The method sets a new standing audio node for the engine effect's sound.
         * 
         * @param standingEngineAudio A new standing audio node for the engine effect's sound.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder addStandingSound(final AudioNode standingEngineAudio) {
            // If the method addStandingSound() was called previously.
            this.cleanStandingSound();
            //
            ENGINE_EFFECT_.anodeStanding_ = standingEngineAudio;
            ENGINE_EFFECT_.anodeStanding_.setPositional(STANDING_SOUND_IS_POSITIONAL_);
            ENGINE_EFFECT_.anodeStanding_.setVolume(STANDING_SOUND_VOLUME_);
            ENGINE_EFFECT_.anodeStanding_.setLooping(STANDING_SOUND_IS_LOOPING_);
            NODE_ENGINE_EFFECT_.attachChild(ENGINE_EFFECT_.anodeStanding_);
            ENGINE_EFFECT_.addAudioEffectStart(ENGINE_EFFECT_.anodeStanding_);
            ENGINE_EFFECT_.addAudioEffectStop(ENGINE_EFFECT_.anodeStanding_);
            return this;
        }
        
        /**
         * The method sets whether the standing sound is positional or not.
         * 
         * @param isPositional The flag whether the standing sound is positional or not.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setStandingAudioIsPositional(final boolean isPositional) {
            if (ENGINE_EFFECT_.anodeStanding_ == null) {
                this.addStandingSound();
            }
            ENGINE_EFFECT_.anodeStanding_.setPositional(isPositional);
            return this;
        }
        
        /**
         * The method sets the standing sound's volume.
         * 
         * @param soundVolume The standing sound's volume.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setStandingSoundVolume(final float soundVolume) {
            if (ENGINE_EFFECT_.anodeStanding_ == null) {
                this.addStandingSound();
            }
            ENGINE_EFFECT_.anodeStanding_.setVolume(soundVolume);
            return this;
        }
        
        /**
         * The method sets whether the standing sound is looping or not.
         * 
         * @param isLooping The flag whether the standing sound is looping or not.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setStandingAudioIsLooping(final boolean isLooping) {
            if (ENGINE_EFFECT_.anodeStanding_ == null) {
                this.addStandingSound();
            }
            ENGINE_EFFECT_.anodeStanding_.setLooping(isLooping);
            return this;
        }
        //
        // *********************************************************************
        //
        /**
         * The method adds the running sound into the engine effect.
         * 
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder addRunningSound() {
            // If the method addRunningSound(AudioNode) was called previously.
            this.cleanRunningSound();
            //
            ENGINE_EFFECT_.anodeRunning_ = new AudioNode(
                assetManager_, "Sounds/EngineRunning.wav", AudioData.DataType.Buffer);
            ENGINE_EFFECT_.anodeRunning_.setPositional(RUNNING_SOUND_IS_POSITIONAL_);
            ENGINE_EFFECT_.anodeRunning_.setVolume(RUNNING_SOUND_VOLUME_);
            ENGINE_EFFECT_.anodeRunning_.setLooping(RUNNING_SOUND_IS_LOOPING_);
            NODE_ENGINE_EFFECT_.attachChild(ENGINE_EFFECT_.anodeRunning_);
            ENGINE_EFFECT_.addAudioEffectStart(ENGINE_EFFECT_.anodeRunning_);
            ENGINE_EFFECT_.addAudioEffectStop(ENGINE_EFFECT_.anodeRunning_);
            return this;
        }
        
        /**
         * The method sets a new running audio node for the engine effect's sound.
         * 
         * @param runningEngineAudio A new running audio node for the engine effect's sound.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder addRunningSound(final AudioNode runningEngineAudio) {
            // If the method addRunningSound() was called previously.
            this.cleanRunningSound();
            //
            ENGINE_EFFECT_.anodeRunning_ = runningEngineAudio;
            ENGINE_EFFECT_.anodeRunning_.setPositional(RUNNING_SOUND_IS_POSITIONAL_);
            ENGINE_EFFECT_.anodeRunning_.setVolume(RUNNING_SOUND_VOLUME_);
            ENGINE_EFFECT_.anodeRunning_.setLooping(RUNNING_SOUND_IS_LOOPING_);
            NODE_ENGINE_EFFECT_.attachChild(ENGINE_EFFECT_.anodeRunning_);
            ENGINE_EFFECT_.addAudioEffectStart(ENGINE_EFFECT_.anodeRunning_);
            ENGINE_EFFECT_.addAudioEffectStop(ENGINE_EFFECT_.anodeRunning_);
            return this;
        }
        
        /**
         * The method sets whether the running sound is positional or not.
         * 
         * @param isPositional The flag whether the running sound is positional or not.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setRunningAudioIsPositional(final boolean isPositional) {
            if (ENGINE_EFFECT_.anodeRunning_ == null) {
                this.addRunningSound();
            }
            ENGINE_EFFECT_.anodeRunning_.setPositional(isPositional);
            return this;
        }
        
        /**
         * The method sets the running sound's volume.
         * 
         * @param soundVolume The running sound's volume.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setRunningSoundVolume(final float soundVolume) {
            if (ENGINE_EFFECT_.anodeRunning_ == null) {
                this.addRunningSound();
            }
            ENGINE_EFFECT_.anodeRunning_.setVolume(soundVolume);
            return this;
        }
        
        /**
         * The method sets whether the running sound is looping or not.
         * 
         * @param isLooping The flag whether the running sound is looping or not.
         * @return EngineBuilder This instance of the builder.
         */
        public EngineBuilder setRunningAudioIsLooping(final boolean isLooping) {
            if (ENGINE_EFFECT_.anodeRunning_ == null) {
                this.addRunningSound();
            }
            ENGINE_EFFECT_.anodeRunning_.setLooping(isLooping);
            return this;
        }
        //
        // *********************************************************************
        //
        /**
         * The method executes the engine effect's final build and returns its instance.
         * 
         * @return EngineEffect The engine effect's instance.
         */
        public EngineEffect build() {
            // If the engine effect is built by default.
            if (ENGINE_EFFECT_.peExhaustOne_ == null && 
                ENGINE_EFFECT_.peExhaustTwo_ == null &&
                ENGINE_EFFECT_.peBlastOne_ == null &&
                ENGINE_EFFECT_.peBlastTwo_ == null &&
                ENGINE_EFFECT_.anodeStanding_ == null && 
                ENGINE_EFFECT_.anodeRunning_ == null) {
                this.addExhaustOne(false, EXHAUST_PARTICLES_NUMBER_);
                this.addExhaustTwo(false, EXHAUST_PARTICLES_NUMBER_);
                this.addBlastOne(false, BLAST_PARTICLES_NUMBER_);
                this.addBlastTwo(false, BLAST_PARTICLES_NUMBER_);
                this.addStandingSound();
                this.addRunningSound();
            }
            return ENGINE_EFFECT_;
        }
        //
        // ************************* Package Methods ***************************
        //
        //
        // ************************* Protected Methods *************************
        //
        //
        // ************************* Private Methods ***************************
        //
        /**
         * The method cleans the standing sound effect from the engine effect.
         */
        private void cleanStandingSound() {
            if (ENGINE_EFFECT_.anodeStanding_ != null) {
                ENGINE_EFFECT_.removeAudioEffectStart(ENGINE_EFFECT_.anodeStanding_);
                ENGINE_EFFECT_.removeAudioEffectStop(ENGINE_EFFECT_.anodeStanding_);
                if (NODE_ENGINE_EFFECT_.hasChild(ENGINE_EFFECT_.anodeStanding_)) {
                    NODE_ENGINE_EFFECT_.detachChild(ENGINE_EFFECT_.anodeStanding_);
                }
            }
        }
        
        /**
         * The method cleans the running sound effect from the engine effect.
         */
        private void cleanRunningSound() {
            if (ENGINE_EFFECT_.anodeRunning_ != null) {
                ENGINE_EFFECT_.removeAudioEffectStart(ENGINE_EFFECT_.anodeRunning_);
                ENGINE_EFFECT_.removeAudioEffectStop(ENGINE_EFFECT_.anodeRunning_);
                if (NODE_ENGINE_EFFECT_.hasChild(ENGINE_EFFECT_.anodeRunning_)) {
                    NODE_ENGINE_EFFECT_.detachChild(ENGINE_EFFECT_.anodeRunning_);
                }
            }
        }
        //
        // ************************* Inner Classes *****************************
        //
        //
        // *********************************************************************
    }
    //
    // *************************************************************************
}