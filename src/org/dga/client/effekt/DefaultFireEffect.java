/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.effekt;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioData;
import com.jme3.audio.AudioNode;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.shapes.EmitterSphereShape;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import org.dga.client.DgaJmeCommonConstant;
import org.dga.client.DgaJmeUtils;

/**
 * The default implementation of fire effects provided by a shooting game object.
 * 
 * @extends AbstractFireEffect
 * @implements FireEffect
 * @pattern Adapter
 * @pattern Tempate Method
 * @pattern Builder
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 24.08.2018
 */
public final class DefaultFireEffect extends AbstractFireEffect implements FireEffect {
    private ParticleEmitter peFlame_ = null;
    private ParticleEmitter peFlash_ = null;
    private ParticleEmitter peSmoke_ = null;
    private AudioNode anodeFire_ = null;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new instance.
     */
    public DefaultFireEffect() {
        super();
    }
    
    /**
     * The constructor creates a new instance with the supplied properties.
     * 
     * @param fireEffectNode The fire effect's node.
     */
    public DefaultFireEffect(final Node fireEffectNode) {
        super(fireEffectNode);
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns a new instance of the builder for a fire effect.
     * 
     * @param assetManager The game application's asset manager.
     * @return FireBuilder A new instance of the builder for a fire effect.
     */
    public static final FireBuilder newBuilder(final AssetManager assetManager) {
        return new FireBuilder(assetManager);
    }
    
    /**
     * The method returns the flame effect.
     * 
     * @return ParticleEmitter The flame effect.
     */
    @Override
    public ParticleEmitter getFlame() {
        return peFlame_;
    }
    
    /**
     * The method returns the flash effect.
     * 
     * @return ParticleEmitter The flash effect.
     */
    @Override
    public ParticleEmitter getFlash() {
        return peFlash_;
    }
    
    /**
     * The method returns the smoke effect.
     * 
     * @return ParticleEmitter The smoke effect.
     */
    @Override
    public ParticleEmitter getSmoke() {
        return peSmoke_;
    }
    
    /**
     * The method returns the fire sound.
     * 
     * @return ParticleEmitter The fire sound.
     */
    @Override
    public AudioNode getFireSound() {
        return anodeFire_;
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    /**
     * The builder for a fire effect provided by a shooting game object.
     * 
     * @extends java.lang.Object
     * @pattern Builder
     * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
     * @version 0.1
     * @date 26.08.2018
     */
    public static final class FireBuilder extends Object {
        private static final int MAX_PARTICLES_NUMBER_ = 
            DgaJmeCommonConstant.OPTIMAL_MAXIMUM_PARTICLES_NUMBER.getIntValue();
        //
        private static final int FLAME_PARTICLES_NUMBER_ = 32;
        private static final ColorRGBA FLAME_START_COLOR_ = new ColorRGBA(1.0f, 0.4f, 0.05f, 0.5f);
        private static final ColorRGBA FLAME_END_COLOR_ = new ColorRGBA(0.4f, 0.22f, 0.12f, 0.5f);
        private static final float FLAME_START_SIZE_ = 1.0f;
        private static final float FLAME_END_SIZE_ = 1.5f;
        private static final float FLAME_EMITTER_RADIUS_ = 1.0f;
        private static final Vector3f FLAME_GRAVITY_ = new Vector3f(0f, -5.0f, 0f);
        private static final float FLAME_LOW_LIFE_ = 0.2f;
        private static final float FLAME_HIGH_LIFE_ = 0.4f;
        private static final int FLAME_IMAGES_X_ = 2;
        private static final int FLAME_IMAGES_Y_ = 2;
        //
        private static final int FLASH_PARTICLES_NUMBER_ = 128;
        private static final ColorRGBA FLASH_START_COLOR_ = new ColorRGBA(0.2f, 0.2f, 0.2f, 0.05f);
        private static final ColorRGBA FLASH_END_COLOR_ = new ColorRGBA(0.4f, 0.4f, 0.4f, 0.05f);
        private static final float FLASH_START_SIZE_ = 1.0f;
        private static final float FLASH_END_SIZE_ = 3.0f;
        private static final float FLASH_EMITTER_RADIUS_ = 2.0f;
        private static final Vector3f FLASH_GRAVITY_ = new Vector3f(0f, 0f, 0f);
        private static final float FLASH_LOW_LIFE_ = 1.0f;
        private static final float FLASH_HIGH_LIFE_ = 2.0f;
        private static final int FLASH_IMAGES_X_ = 2;
        private static final int FLASH_IMAGES_Y_ = 2;
        //
        private static final int SMOKE_PARTICLES_NUMBER_ = 16;
        private static final ColorRGBA SMOKE_START_COLOR_ = new ColorRGBA(0.1f, 0.1f, 0.1f, 0.5f);
        private static final ColorRGBA SMOKE_END_COLOR_ = new ColorRGBA(0.2f, 0.2f, 0.2f, 0.5f);
        private static final float SMOKE_START_SIZE_ = 1.3f;
        private static final float SMOKE_END_SIZE_ = 2.0f;
        private static final float SMOKE_EMITTER_RADIUS_ = 2.0f;
        private static final Vector3f SMOKE_GRAVITY_ = new Vector3f(0f, 0f, 0f);
        private static final float SMOKE_LOW_LIFE_ = 0.4f;
        private static final float SMOKE_HIGH_LIFE_ = 0.5f;
        private static final int SMOKE_IMAGES_X_ = 2;
        private static final int SMOKE_IMAGES_Y_ = 2;
        //
        private static final boolean FIRE_SOUND_IS_POSITIONAL_ = true;
        private static final float FIRE_SOUND_VOLUME_ = DgaJmeCommonConstant.DEFAULT_VOLUME_LEVEL.getFloatValue();
        private static final boolean FIRE_SOUND_IS_LOOPING_ = false;
        //
        private AssetManager assetManager_ = null;
        private final Node NODE_FIRE_EFFECT_ = new Node("FireEffect");
        private final DefaultFireEffect FIRE_EFFECT_ = new DefaultFireEffect(NODE_FIRE_EFFECT_);
        //
        // ************************* Constructors ******************************
        //
        /**
         * The constructor creates a new instance.
         * 
         * @param assetManager The game application's asset manager.
         */
        private FireBuilder(final AssetManager assetManager) {
            super();
            assetManager_ = assetManager;
        }
        //
        // ************************* Public Methods ****************************
        //
        /**
         * The method adds a flame into the fire effect.
         * 
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder addFlame() {
            return this.addFlame(false, FLAME_PARTICLES_NUMBER_);
        }
        
        /**
         * The method adds a flame into the fire effect.
         * 
         * @param isPointSprite The flag whether the flame is made of points or of triangles.
         * @param particlesNumber The number of particles.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder addFlame(final boolean isPointSprite, final int particlesNumber) {
            FIRE_EFFECT_.peFlame_ = new ParticleEmitter(
                "Flame",
                DgaJmeUtils.getParticleEmitterType(isPointSprite),
                (particlesNumber > 0 && particlesNumber <= MAX_PARTICLES_NUMBER_ ? 
                    particlesNumber : 
                    FLAME_PARTICLES_NUMBER_));
            FIRE_EFFECT_.peFlame_.setSelectRandomImage(true);
            FIRE_EFFECT_.peFlame_.setStartColor(FLAME_START_COLOR_);
            FIRE_EFFECT_.peFlame_.setEndColor(FLAME_END_COLOR_);
            FIRE_EFFECT_.peFlame_.setParticlesPerSec(0f);
            FIRE_EFFECT_.peFlame_.setStartSize(FLAME_START_SIZE_);
            FIRE_EFFECT_.peFlame_.setEndSize(FLAME_END_SIZE_);
            FIRE_EFFECT_.peFlame_.setShape(new EmitterSphereShape(Vector3f.ZERO, FLAME_EMITTER_RADIUS_));
            FIRE_EFFECT_.peFlame_.setGravity(FLAME_GRAVITY_);
            FIRE_EFFECT_.peFlame_.setLowLife(FLAME_LOW_LIFE_);
            FIRE_EFFECT_.peFlame_.setHighLife(FLAME_HIGH_LIFE_);
            FIRE_EFFECT_.peFlame_.getParticleInfluencer().setInitialVelocity(new Vector3f(0f, 7.0f, 0f));
            FIRE_EFFECT_.peFlame_.getParticleInfluencer().setVelocityVariation(1.0f);
            FIRE_EFFECT_.peFlame_.setImagesX(FLAME_IMAGES_X_);
            FIRE_EFFECT_.peFlame_.setImagesY(FLAME_IMAGES_Y_);
            Material matFlame_ = new Material(assetManager_, "Common/MatDefs/Misc/Particle.j3md");
            matFlame_.setTexture("Texture", assetManager_.loadTexture("Effects/Explosion/Flame.png"));
            matFlame_.setBoolean("PointSprite", isPointSprite);
            FIRE_EFFECT_.peFlame_.setMaterial(matFlame_);
            NODE_FIRE_EFFECT_.attachChild(FIRE_EFFECT_.peFlame_);
            FIRE_EFFECT_.addVisualEffectEmit(FIRE_EFFECT_.peFlame_);
            FIRE_EFFECT_.addVisualEffectKill(FIRE_EFFECT_.peFlame_);
            return this;
        }
        
        /**
         * The method sets the flame's start size.
         * 
         * @param flameStartSize The flame's start size.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder setFlameStartSize(final float flameStartSize) {
            if (FIRE_EFFECT_.peFlame_ == null) {
                this.addFlame(false, FLAME_PARTICLES_NUMBER_);
            }
            FIRE_EFFECT_.peFlame_.setStartSize(flameStartSize);
            return this;
        }
        
        /**
         * The method sets the flame's end size.
         * 
         * @param flameEndSize The flame's end size.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder setFlameEndSize(final float flameEndSize) {
            if (FIRE_EFFECT_.peFlame_ == null) {
                this.addFlame(false, FLAME_PARTICLES_NUMBER_);
            }
            FIRE_EFFECT_.peFlame_.setEndSize(flameEndSize);
            return this;
        }
        
        /**
         * The method sets the flame's emitter radius.
         * 
         * @param flameEmitterRadius The flame's emitter radius.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder setFlameEmitterRadius(final float flameEmitterRadius) {
            if (FIRE_EFFECT_.peFlame_ == null) {
                this.addFlame(false, FLAME_PARTICLES_NUMBER_);
            }
            FIRE_EFFECT_.peFlame_.setShape(new EmitterSphereShape(Vector3f.ZERO, flameEmitterRadius));
            return this;
        }
        
        /**
         * The method sets the flame's gravity.
         * 
         * @param flameGravity The flame's gravity.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder setFlameGravity(final Vector3f flameGravity) {
            if (FIRE_EFFECT_.peFlame_ == null) {
                this.addFlame(false, FLAME_PARTICLES_NUMBER_);
            }
            FIRE_EFFECT_.peFlame_.setGravity(flameGravity);
            return this;
        }
        
        /**
         * The method sets the flame's low life.
         * 
         * @param flameLowLife The flame's low life.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder setFlameLowLife(final float flameLowLife) {
            if (FIRE_EFFECT_.peFlame_ == null) {
                this.addFlame(false, FLAME_PARTICLES_NUMBER_);
            }
            FIRE_EFFECT_.peFlame_.setLowLife(flameLowLife);
            return this;
        }
        
        /**
         * The method sets the flame's high life.
         * 
         * @param flameHighLife The flame's high life.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder setFlameHighLife(final float flameHighLife) {
            if (FIRE_EFFECT_.peFlame_ == null) {
                this.addFlame(false, FLAME_PARTICLES_NUMBER_);
            }
            FIRE_EFFECT_.peFlame_.setHighLife(flameHighLife);
            return this;
        }
        
        /**
         * The method sets the flame's quantity of images along the X axis.
         * 
         * @param flameImagesX The flame's quantity of images along the X axis.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder setFlameImagesX(final int flameImagesX) {
            if (FIRE_EFFECT_.peFlame_ == null) {
                this.addFlame(false, FLAME_PARTICLES_NUMBER_);
            }
            FIRE_EFFECT_.peFlame_.setImagesX(flameImagesX);
            return this;
        }
        
        /**
         * The method sets the flame's quantity of images along the Y axis.
         * 
         * @param flameImagesY The flame's quantity of images along the Y axis.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder setFlameImagesY(final int flameImagesY) {
            if (FIRE_EFFECT_.peFlame_ == null) {
                this.addFlame(false, FLAME_PARTICLES_NUMBER_);
            }
            FIRE_EFFECT_.peFlame_.setImagesY(flameImagesY);
            return this;
        }
        //
        // *********************************************************************
        //
        /**
         * The method adds a flash into the fire effect.
         * 
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder addFlash() {
            return this.addFlash(false, FLASH_PARTICLES_NUMBER_);
        }
        
        /**
         * The method adds a flash into the fire effect.
         * 
         * @param isPointSprite The flag whether the flash is made of points or of triangles.
         * @param particlesNumber The number of particles.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder addFlash(final boolean isPointSprite, final int particlesNumber) {
            FIRE_EFFECT_.peFlash_ = new ParticleEmitter(
                "Flash",
                DgaJmeUtils.getParticleEmitterType(isPointSprite),
                (particlesNumber > 0 && particlesNumber <= MAX_PARTICLES_NUMBER_ ? 
                    particlesNumber : 
                    FLASH_PARTICLES_NUMBER_));
            FIRE_EFFECT_.peFlash_.setSelectRandomImage(true);
            FIRE_EFFECT_.peFlash_.setStartColor(FLASH_START_COLOR_);
            FIRE_EFFECT_.peFlash_.setEndColor(FLASH_END_COLOR_);
            FIRE_EFFECT_.peFlash_.setStartSize(FLASH_START_SIZE_);
            FIRE_EFFECT_.peFlash_.setEndSize(FLASH_END_SIZE_);
            FIRE_EFFECT_.peFlash_.setShape(new EmitterSphereShape(Vector3f.ZERO, FLASH_EMITTER_RADIUS_));
            FIRE_EFFECT_.peFlash_.setParticlesPerSec(0f);
            FIRE_EFFECT_.peFlash_.setGravity(FLASH_GRAVITY_);
            FIRE_EFFECT_.peFlash_.setLowLife(FLASH_LOW_LIFE_);
            FIRE_EFFECT_.peFlash_.setHighLife(FLASH_HIGH_LIFE_);
            FIRE_EFFECT_.peFlash_.getParticleInfluencer().setInitialVelocity(new Vector3f(0f, 5f, 0f));
            FIRE_EFFECT_.peFlash_.getParticleInfluencer().setVelocityVariation(1);
            FIRE_EFFECT_.peFlash_.setImagesX(FLASH_IMAGES_X_);
            FIRE_EFFECT_.peFlash_.setImagesY(FLASH_IMAGES_Y_);
            Material matFlash_ = new Material(assetManager_, "Common/MatDefs/Misc/Particle.j3md");
            matFlash_.setTexture("Texture", assetManager_.loadTexture("Effects/Explosion/Flash.png"));
            matFlash_.setBoolean("PointSprite", isPointSprite);
            FIRE_EFFECT_.peFlash_.setMaterial(matFlash_);
            NODE_FIRE_EFFECT_.attachChild(FIRE_EFFECT_.peFlash_);
            FIRE_EFFECT_.addVisualEffectEmit(FIRE_EFFECT_.peFlash_);
            FIRE_EFFECT_.addVisualEffectKill(FIRE_EFFECT_.peFlash_);
            return this;
        }
        
        /**
         * The method sets the flash's start size.
         * 
         * @param flashStartSize The flash's start size.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder setFlashStartSize(final float flashStartSize) {
            if (FIRE_EFFECT_.peFlash_ == null) {
                this.addFlash(false, FLASH_PARTICLES_NUMBER_);
            }
            FIRE_EFFECT_.peFlash_.setStartSize(flashStartSize);
            return this;
        }
        
        /**
         * The method sets the flash's end size.
         * 
         * @param flashEndSize The flash's end size.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder setFlashEndSize(final float flashEndSize) {
            if (FIRE_EFFECT_.peFlash_ == null) {
                this.addFlash(false, FLASH_PARTICLES_NUMBER_);
            }
            FIRE_EFFECT_.peFlash_.setEndSize(flashEndSize);
            return this;
        }
        
        /**
         * The method sets the flash's emitter radius.
         * 
         * @param flashEmitterRadius The flash's emitter radius.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder setFlashEmitterRadius(final float flashEmitterRadius) {
            if (FIRE_EFFECT_.peFlash_ == null) {
                this.addFlash(false, FLASH_PARTICLES_NUMBER_);
            }
            FIRE_EFFECT_.peFlash_.setShape(new EmitterSphereShape(Vector3f.ZERO, flashEmitterRadius));
            return this;
        }
        
        /**
         * The method sets the flash's gravity.
         * 
         * @param flashGravity The flash's gravity.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder setFlashGravity(final Vector3f flashGravity) {
            if (FIRE_EFFECT_.peFlash_ == null) {
                this.addFlash(false, FLASH_PARTICLES_NUMBER_);
            }
            FIRE_EFFECT_.peFlash_.setGravity(flashGravity);
            return this;
        }
        
        /**
         * The method sets the flash's low life.
         * 
         * @param flashLowLife The flash's low life.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder setFlashLowLife(final float flashLowLife) {
            if (FIRE_EFFECT_.peFlash_ == null) {
                this.addFlash(false, FLASH_PARTICLES_NUMBER_);
            }
            FIRE_EFFECT_.peFlash_.setLowLife(flashLowLife);
            return this;
        }
        
        /**
         * The method sets the flash's high life.
         * 
         * @param flashHighLife The flash's high life.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder setFlashHighLife(final float flashHighLife) {
            if (FIRE_EFFECT_.peFlash_ == null) {
                this.addFlash(false, FLASH_PARTICLES_NUMBER_);
            }
            FIRE_EFFECT_.peFlash_.setHighLife(flashHighLife);
            return this;
        }
        
        /**
         * The method sets the flash's quantity of images along the X axis.
         * 
         * @param flashImagesX The flash's quantity of images along the X axis.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder setFlashImagesX(final int flashImagesX) {
            if (FIRE_EFFECT_.peFlash_ == null) {
                this.addFlash(false, FLASH_PARTICLES_NUMBER_);
            }
            FIRE_EFFECT_.peFlash_.setImagesX(flashImagesX);
            return this;
        }
        
        /**
         * The method sets the flash's quantity of images along the Y axis.
         * 
         * @param flashImagesY The flash's quantity of images along the Y axis.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder setFlashImagesY(final int flashImagesY) {
            if (FIRE_EFFECT_.peFlash_ == null) {
                this.addFlash(false, FLASH_PARTICLES_NUMBER_);
            }
            FIRE_EFFECT_.peFlash_.setImagesY(flashImagesY);
            return this;
        }
        //
        // *********************************************************************
        //
        /**
         * The method adds a smoke into the fire effect.
         * 
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder addSmoke() {
            return this.addSmoke(false, SMOKE_PARTICLES_NUMBER_);
        }
        
        /**
         * The method adds a smoke into the fire effect.
         * 
         * @param isPointSprite The flag whether the smoke is made of points or of triangles.
         * @param particlesNumber The number of particles.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder addSmoke(final boolean isPointSprite, final int particlesNumber) {
            FIRE_EFFECT_.peSmoke_ = new ParticleEmitter(
                "Smoke",
                DgaJmeUtils.getParticleEmitterType(isPointSprite),
                (particlesNumber > 0 && particlesNumber <= MAX_PARTICLES_NUMBER_ ? 
                    particlesNumber : 
                    SMOKE_PARTICLES_NUMBER_));
            FIRE_EFFECT_.peSmoke_.setSelectRandomImage(true);
            FIRE_EFFECT_.peSmoke_.setRandomAngle(true);
            FIRE_EFFECT_.peSmoke_.getParticleInfluencer().setVelocityVariation(1.0f);
            FIRE_EFFECT_.peSmoke_.setStartColor(SMOKE_START_COLOR_);
            FIRE_EFFECT_.peSmoke_.setEndColor(SMOKE_END_COLOR_);
            FIRE_EFFECT_.peSmoke_.setStartSize(SMOKE_START_SIZE_);
            FIRE_EFFECT_.peSmoke_.setEndSize(SMOKE_END_SIZE_);
            FIRE_EFFECT_.peSmoke_.setShape(new EmitterSphereShape(Vector3f.ZERO, SMOKE_EMITTER_RADIUS_));
            FIRE_EFFECT_.peSmoke_.setParticlesPerSec(0f);
            FIRE_EFFECT_.peSmoke_.setParticlesPerSec(0f);
            FIRE_EFFECT_.peSmoke_.setGravity(SMOKE_GRAVITY_);
            FIRE_EFFECT_.peSmoke_.setLowLife(SMOKE_LOW_LIFE_);
            FIRE_EFFECT_.peSmoke_.setHighLife(SMOKE_HIGH_LIFE_);
            FIRE_EFFECT_.peSmoke_.getParticleInfluencer().setInitialVelocity(new Vector3f(0f, 5f, 0f));
            FIRE_EFFECT_.peSmoke_.getParticleInfluencer().setVelocityVariation(1);
            FIRE_EFFECT_.peSmoke_.setImagesX(SMOKE_IMAGES_X_);
            FIRE_EFFECT_.peSmoke_.setImagesY(SMOKE_IMAGES_Y_);
            Material matSmoke_ = new Material(assetManager_, "Common/MatDefs/Misc/Particle.j3md");
            matSmoke_.setTexture("Texture", assetManager_.loadTexture("Effects/Explosion/Shockwave.png"));
            matSmoke_.setBoolean("PointSprite", isPointSprite);
            FIRE_EFFECT_.peSmoke_.setMaterial(matSmoke_);
            NODE_FIRE_EFFECT_.attachChild(FIRE_EFFECT_.peSmoke_);
            FIRE_EFFECT_.addVisualEffectEmit(FIRE_EFFECT_.peSmoke_);
            FIRE_EFFECT_.addVisualEffectKill(FIRE_EFFECT_.peSmoke_);
            return this;
        }
        
        /**
         * The method sets the smoke's start size.
         * 
         * @param smokeStartSize The smoke's start size.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder setSmokeStartSize(final float smokeStartSize) {
            if (FIRE_EFFECT_.peSmoke_ == null) {
                this.addSmoke(false, SMOKE_PARTICLES_NUMBER_);
            }
            FIRE_EFFECT_.peSmoke_.setStartSize(smokeStartSize);
            return this;
        }
        
        /**
         * The method sets the smoke's end size.
         * 
         * @param smokeEndSize The smoke's end size.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder setSmokeEndSize(final float smokeEndSize) {
            if (FIRE_EFFECT_.peSmoke_ == null) {
                this.addSmoke(false, SMOKE_PARTICLES_NUMBER_);
            }
            FIRE_EFFECT_.peSmoke_.setEndSize(smokeEndSize);
            return this;
        }
        
        /**
         * The method sets the smoke's emitter radius.
         * 
         * @param smokeEmitterRadius The smoke's emitter radius.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder setSmokeEmitterRadius(final float smokeEmitterRadius) {
            if (FIRE_EFFECT_.peSmoke_ == null) {
                this.addSmoke(false, SMOKE_PARTICLES_NUMBER_);
            }
            FIRE_EFFECT_.peSmoke_.setShape(new EmitterSphereShape(Vector3f.ZERO, smokeEmitterRadius));
            return this;
        }
        
        /**
         * The method sets the smoke's gravity.
         * 
         * @param smokeGravity The smoke's gravity.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder setSmokeGravity(final Vector3f smokeGravity) {
            if (FIRE_EFFECT_.peSmoke_ == null) {
                this.addSmoke(false, SMOKE_PARTICLES_NUMBER_);
            }
            FIRE_EFFECT_.peSmoke_.setGravity(smokeGravity);
            return this;
        }
        
        /**
         * The method sets the smoke's low life.
         * 
         * @param smokeLowLife The smoke's low life.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder setSmokeLowLife(final float smokeLowLife) {
            if (FIRE_EFFECT_.peSmoke_ == null) {
                this.addSmoke(false, SMOKE_PARTICLES_NUMBER_);
            }
            FIRE_EFFECT_.peSmoke_.setLowLife(smokeLowLife);
            return this;
        }
        
        /**
         * The method set the smoke's high life.
         * 
         * @param smokeHighLife The smoke's high life.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder setSmokeHighLife(final float smokeHighLife) {
            if (FIRE_EFFECT_.peSmoke_ == null) {
                this.addSmoke(false, SMOKE_PARTICLES_NUMBER_);
            }
            FIRE_EFFECT_.peSmoke_.setHighLife(smokeHighLife);
            return this;
        }
        
        /**
         * The method sets the smoke's quantity of images along the X axis.
         * 
         * @param smokeImagesX The smoke's quantity of images along the X axis.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder setSmokeImagesX(final int smokeImagesX) {
            if (FIRE_EFFECT_.peSmoke_ == null) {
                this.addSmoke(false, SMOKE_PARTICLES_NUMBER_);
            }
            FIRE_EFFECT_.peSmoke_.setImagesX(smokeImagesX);
            return this;
        }
        
        /**
         * The method sets the smoke's quantity of images along the Y axis.
         * 
         * @param smokeImagesY The smoke's quantity of images along the Y axis.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder setSmokeImagesY(final int smokeImagesY) {
            if (FIRE_EFFECT_.peSmoke_ == null) {
                this.addSmoke(false, SMOKE_PARTICLES_NUMBER_);
            }
            FIRE_EFFECT_.peSmoke_.setImagesY(smokeImagesY);
            return this;
        }
        //
        // *********************************************************************
        //
        /**
         * The method adds the sound into the fire effect.
         * 
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder addSound() {
            // If the method addSound(AudioNode) was called previously.
            this.cleanFireSound();
            //
            FIRE_EFFECT_.anodeFire_ = new AudioNode(
                assetManager_, "Sounds/CannonShot.wav", AudioData.DataType.Buffer);
            FIRE_EFFECT_.anodeFire_.setPositional(FIRE_SOUND_IS_POSITIONAL_);
            FIRE_EFFECT_.anodeFire_.setVolume(FIRE_SOUND_VOLUME_);
            FIRE_EFFECT_.anodeFire_.setLooping(FIRE_SOUND_IS_LOOPING_);
            NODE_FIRE_EFFECT_.attachChild(FIRE_EFFECT_.anodeFire_);
            FIRE_EFFECT_.addAudioEffectStart(FIRE_EFFECT_.anodeFire_);
            FIRE_EFFECT_.addAudioEffectStop(FIRE_EFFECT_.anodeFire_);
            return this;
        }
        
        /**
         * The method sets a new audio node for the fire effect's sound.
         * 
         * @param fireEffectAudio A new audio node for the fire effect's sound.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder addSound(final AudioNode fireEffectAudio) {
            // If the method addSound() was called previously.
            this.cleanFireSound();
            //
            FIRE_EFFECT_.anodeFire_ = fireEffectAudio;
            FIRE_EFFECT_.anodeFire_.setPositional(FIRE_SOUND_IS_POSITIONAL_);
            FIRE_EFFECT_.anodeFire_.setVolume(FIRE_SOUND_VOLUME_);
            FIRE_EFFECT_.anodeFire_.setLooping(FIRE_SOUND_IS_LOOPING_);
            NODE_FIRE_EFFECT_.attachChild(FIRE_EFFECT_.anodeFire_);
            FIRE_EFFECT_.addAudioEffectStart(FIRE_EFFECT_.anodeFire_);
            FIRE_EFFECT_.addAudioEffectStop(FIRE_EFFECT_.anodeFire_);
            return this;
        }
        
        /**
         * The method sets whether the sound is positional or not.
         * 
         * @param isPositional The flag whether the sound is positional or not.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder setAudioIsPositional(final boolean isPositional) {
            if (FIRE_EFFECT_.anodeFire_ == null) {
                this.addSound();
            }
            FIRE_EFFECT_.anodeFire_.setPositional(isPositional);
            return this;
        }
        
        /**
         * The method sets the sound's volume.
         * 
         * @param soundVolume The sound's volume.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder setSoundVolume(final float soundVolume) {
            if (FIRE_EFFECT_.anodeFire_ == null) {
                this.addSound();
            }
            FIRE_EFFECT_.anodeFire_.setVolume(soundVolume);
            return this;
        }
        
        /**
         * The method sets whether the sound is looping or not.
         * 
         * @param isLooping The flag whether the sound is looping or not.
         * @return FireBuilder This instance of the builder.
         */
        public FireBuilder setAudioIsLooping(final boolean isLooping) {
            if (FIRE_EFFECT_.anodeFire_ == null) {
                this.addSound();
            }
            FIRE_EFFECT_.anodeFire_.setLooping(isLooping);
            return this;
        }
        //
        // *********************************************************************
        //
        /**
         * The method executes the fire effect's final build and returns its instance.
         * 
         * @return FireEffect The fire effect's instance.
         */
        public FireEffect build() {
            // If the fire effect is built by default.
            if (FIRE_EFFECT_.peFlame_ == null &&
                FIRE_EFFECT_.peFlash_ == null &&
                FIRE_EFFECT_.peSmoke_ == null &&
                FIRE_EFFECT_.anodeFire_ == null) {
                this.addFlame(false, FLAME_PARTICLES_NUMBER_);
                this.addFlash(false, FLASH_PARTICLES_NUMBER_);
                this.addSmoke(false, SMOKE_PARTICLES_NUMBER_);
                this.addSound();
            }
            return FIRE_EFFECT_;
        }
        //
        // ************************* Package Methods ***************************
        //
        //
        // ************************* Protected Methods *************************
        //
        //
        // ************************* Private Methods ***************************
        //
        /**
         * The method cleans the fire sound effect from the fire effect.
         */
        private void cleanFireSound() {
            if (FIRE_EFFECT_.anodeFire_ != null) {
                FIRE_EFFECT_.removeAudioEffectStart(FIRE_EFFECT_.anodeFire_);
                FIRE_EFFECT_.removeAudioEffectStop(FIRE_EFFECT_.anodeFire_);
                if (NODE_FIRE_EFFECT_.hasChild(FIRE_EFFECT_.anodeFire_)) {
                    NODE_FIRE_EFFECT_.detachChild(FIRE_EFFECT_.anodeFire_);
                }
            }
        }
        //
        // ************************* Inner Classes *****************************
        //
        //
        // *********************************************************************
    }
    //
    // *************************************************************************
}