/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client;

import com.jme3.math.FastMath;

/**
 * The common constants for the overall game client.
 *
 * @extends java.lang.Object
 * @pattern Adapter
 * @pattern Chain of Responsibility
 * @pattern Typesafe Enum
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 30.4.2018
 */
public class DgaJmeCommonConstant extends Object {
    /**
     * The flag indicates that the game application is in weapon development mode.
     */
    public static final boolean IS_IN_WEAPON_DEV_MODE = true;
    /**
     * The flag indicating the game application is in collision development mode.
     */ 
    public static final boolean IS_IN_COLLISION_DEV_MODE = false;
    /**
     * The default volume level for the game application in whole.
     */
    public static final DgaJmeCommonConstant DEFAULT_VOLUME_LEVEL = new DgaJmeCommonConstant(0.5f);
    /**
     * The optimal maximum number of particles recommended for visual effects.
     */
    public static final DgaJmeCommonConstant OPTIMAL_MAXIMUM_PARTICLES_NUMBER = new DgaJmeCommonConstant(1024.0f);
    /**
     * The default time period a fire effect is run for.
     */
    public static final DgaJmeCommonConstant FIRE_EFFECT_MAX_EFFECTS_TIME = new DgaJmeCommonConstant(0.5f);
    //
    private float floatValue_ = 0f;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The float constructor.
     *
     * @param value The constant's float value.
     */
    protected DgaJmeCommonConstant(final float value) {
        floatValue_ = value;
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns a float value.
     *
     * @return float A float value.
     */
    public float getFloatValue() {
        return floatValue_;
    }

    /**
     * The method returns a value in radians.
     *
     * @return float A value in radians.
     */
    public float getRadiansValue() {
        return floatValue_ * FastMath.DEG_TO_RAD;
    }
    
    /**
     * The method returns an integer value.
     *
     * @return int An integer value.
     */
    public int getIntValue() {
        return Math.round(floatValue_);
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
