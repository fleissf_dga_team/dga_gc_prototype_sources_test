/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kampfplatz.battlefieldobject;

import com.jme3.bullet.control.PhysicsControl;
import com.jme3.util.clone.JmeCloneable;
import org.dga.client.DgaJmePhysicsControl;

/**
 * The common interface of a physics object located within a battlefield.
 *
 * @extends BattlefieldObject
 * @extends DgaJmePhysicsControl
 * @extends PhysicsControl
 * @extends JmeCloneable
 * @pattern Adapter
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 09.08.2018
 */
public interface BattlefieldPhysicsObject extends BattlefieldObject, 
    DgaJmePhysicsControl, PhysicsControl, JmeCloneable {
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method checks if this control is in kinematic spatial mode.
     *
     * @return boolean True if the spatial location is applied to this kinematic
     * rigidbody.
     */
    public boolean isKinematicSpatial();
    
    /**
     * The method sets the node to kinematic mode. in this mode the node is not 
     * affected by physics but affects other physics objects. Iits kinetic force 
     * is calculated by the amount of movement it is exposed to and its weight.
     * 
     * @param kinematic The flag of kinematic mode.
     */
    public void setKinematic(final boolean kinematic);
    
    /**
     * The method sets this control to kinematic spatial mode so that the
     * spatials transform will be applied to the rigidbody in kinematic mode,
     * defaults to true.
     *
     * @param kinematicSpatial The flag of kinematic spatial mode.
     */
    public void setKinematicSpatial(final boolean kinematicSpatial);

    /**
     * The method sets the collision group number for this physics object. 
     * <br>The groups are integer bit masks and some pre-made variables are 
     * available in CollisionObject. All physics objects are by default in 
     * COLLISION_GROUP_01.<br> Two object will collide when <b>one</b> of the 
     * partys has the collisionGroup of the other in its collideWithGroups set.
     * 
     * @param collisionGroup The collision group to set.
     */
    public void setCollisionGroup(final int collisionGroup);
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************************************************************
}