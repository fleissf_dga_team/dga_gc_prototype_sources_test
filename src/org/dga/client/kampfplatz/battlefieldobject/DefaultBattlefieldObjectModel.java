/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kampfplatz.battlefieldobject;

import org.dga.client.kampfplatz.Battlefield;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.scene.Spatial;
import org.dga.client.DgaJmeScale;

/**
 * The default implementation of a model for an object located within a battlefield.
 * 
 * @extends AbstractBattlefieldObjectModel
 * @implements BattlefieldObjectModel
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 10.08.2018
 */
public final class DefaultBattlefieldObjectModel extends AbstractBattlefieldObjectModel 
    implements BattlefieldObjectModel {
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new model with the supplied properties.
     *
     * @param battlefield The battlefield the object is created for.
     * @param objectSpatial The battlefield object's spatial.
     * @param objectScale The battlefield object's scale.
     * @param objectShape The battlefield object's collision shape.
     * @param objectName The battlefield object's name.
     */
    public DefaultBattlefieldObjectModel(final Battlefield battlefield, 
        final Spatial objectSpatial, final DgaJmeScale objectScale, 
        final CollisionShape objectShape, final String objectName) {
        super(battlefield, objectSpatial, objectScale, objectShape, objectName);
    }
    //
    // *************************** Public Methods ******************************
    //
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}