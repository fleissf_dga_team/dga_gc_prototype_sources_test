/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kampfplatz.battlefieldobject;

import org.dga.client.DefaultDgaJmeScale;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.GhostControl;
import com.jme3.export.InputCapsule;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.OutputCapsule;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.util.clone.Cloner;
import java.io.IOException;
import org.dga.client.DgaJmeScale;

/**
 * The abstract implementation of a ghost object located within a battlefield.
 * 
 * @extends com.jme3.bullet.control.GhostControl
 * @implements BattlefieldGhostObject
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 10.08.2018
 */
public abstract class AbstractBattlefieldGhostObject extends GhostControl 
    implements BattlefieldGhostObject {
    private BattlefieldObjectModel modelBO_ = null;
    private Spatial spatialBO_ = null;
    private CollisionShape cshBO_ = null;
    private boolean enabled_ = true;
    private boolean added_ = false;
    private PhysicsSpace physicsSpace_ = null;
    private DgaJmeScale scaleBO_ = DefaultDgaJmeScale.ORIGINAL_MODEL_SCALE;
    private boolean isApplyPhysicsLocal_ = false;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new control with the supplied properties.
     * 
     * @param objectModel The battlefield object's model.
     */
    public AbstractBattlefieldGhostObject(final BattlefieldObjectModel objectModel) {
        super(objectModel.getCollisionShape());
        modelBO_ = objectModel;
        spatialBO_ = objectModel.getSpatial();
        cshBO_ = modelBO_.getCollisionShape();
        scaleBO_ = modelBO_.getScale();
        // The jMonkeyEngine v.3.2 has a strange bug with scaling that appears
        // as scaling is applying twice for spatials however only once for 
        // collision shapes. Because of this bug the following method call is 
        // disabled at the moment.
        //this.applyScale();
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the spatial of the control.
     * 
     * @return Spatial The spatial of the control.
     */
    @Override
    public Spatial getSpatial() {
        return spatialBO_;
    }
   
    /**
     * The method returns the battlefield object's scale.
     * 
     * @return DgaJmeScale The battlefield object's scale.
     */
    @Override
    public DgaJmeScale getScale() {
        return scaleBO_;
    }
    
//    /**
//     * The method sets the battlefield object's scale.
//     * 
//     * @param modelScale The battlefield object's scale.
//     */
//    @Override
//    public void setScale(final DgaJmeScale modelScale) {
//        if (modelScale != null && scaleBO_.equals(modelScale) == false) {
//            scaleBO_ = modelScale;
//            this.applyScale();
//        }
//    }
    
    /**
     * The method returns the battlefield object attributed to the control.
     * 
     * @return BattlefieldObjectModel The battlefield object attributed to the control.
     */
    @Override
    public BattlefieldObjectModel getModel() {
        return modelBO_;
    }
    
    /**
     * The method returns the spatial's translation.
     * 
     * @return Vector3f The spatial's translation.
     */
    @Override
    public Vector3f getSpatialTranslation() {
        if (isApplyPhysicsLocal_ == true) {
            return spatialBO_.getLocalTranslation();
        }
        return spatialBO_.getWorldTranslation();
    }

    /**
     * The method returns the spatial's rotation.
     * 
     * @return Quaternion The spatial's rotation.
     */
    @Override
    public Quaternion getSpatialRotation() {
        if (isApplyPhysicsLocal_ == true) {
            return spatialBO_.getLocalRotation();
        }
        return spatialBO_.getWorldRotation();
    }
    
    /**
     * The method returns the battlefield object's collision shape.
     * 
     * @return CollisionShape The battlefield object's collision shape.
     */
    @Override
    public CollisionShape getCollisionShape() {
        return cshBO_;
    }
    //
    // *************************************************************************
    //
    /**
     * The method returns whether the physics of the control is applied locally.
     * 
     * @return boolean The apply of apply physics locally.
     */
    @Override
    public boolean isApplyPhysicsLocal() {
        return isApplyPhysicsLocal_;
    }

    /**
     * The method when set to true defines that the physics coordinates will be
     * applied to the local translation of the spatial instead of the world
     * translation.
     *
     * @param applyPhysicsLocal The flag of apply physics local translation.
     */
    @Override
    public void setApplyPhysicsLocal(final boolean applyPhysicsLocal) {
        isApplyPhysicsLocal_ = applyPhysicsLocal;
    }
    
    /**
     * The method returns the physics space of this control.
     *
     * @return PhysicsSpace The physics space of this control.
     */
    @Override
    public PhysicsSpace getPhysicsSpace() {
        return physicsSpace_;
    }

    /**
     * The method sets the physics space for this control. The method is only
     * used internally, should not be called from user code.
     *
     * @param space The physics space for this control.
     */
    @Override
    public void setPhysicsSpace(final PhysicsSpace space) {
        if (space == null) {
            if (physicsSpace_ != null) {
                physicsSpace_.removeCollisionObject(this);
                added_ = false;
            }
        } else {
            if (physicsSpace_ == space) {
                return;
            }
            if (this.isEnabled() == true) {
                space.addCollisionObject(this);
                added_ = true;
            }
        }
        physicsSpace_ = space;
    }
    
    /**
     * The method sets the spatial for this control. This should not be called
     * from user code.
     * 
     * @param spatial The spatial to be controlled.
     */
    @Override
    public void setSpatial(final Spatial spatial) {
        spatialBO_ = spatial;
        this.setUserObject(spatial);
        if (spatial == null) {
            return;
        }
        this.setPhysicsLocation(this.getSpatialTranslation());
        this.setPhysicsRotation(this.getSpatialRotation());
    }
    
    /**
     * The method sets whether the physics object is enabled or not. The physics
     * object is removed from the physics space when the control is disabled.
     * When the control is enabled again the physics object is moved to the
     * current location of the spatial and then added to the physics space. This
     * allows disabling/enabling physics to move the spatial freely.
     *
     * @param enabled The flag whether the physics object is enabled or not.
     */
    @Override
    public void setEnabled(final boolean enabled) {
        enabled_ = enabled;
        if (physicsSpace_ != null) {
            if (enabled == true && added_ == false) {
                if (spatialBO_ != null) {
                    this.setPhysicsLocation(getSpatialTranslation());
                    this.setPhysicsRotation(getSpatialRotation());
                }
                physicsSpace_.addCollisionObject(this);
                added_ = true;
            } else if (enabled == false && added_ == true) {
                physicsSpace_.removeCollisionObject(this);
                added_ = false;
            }
        }
    }

    /**
     * The method returns the current enabled state of the physics control.
     *
     * @return boolean The current enabled state.
     */
    @Override
    public boolean isEnabled() {
        return enabled_;
    }
    
    /**
     * The method updates the control. This should not be called from the user code.
     *
     * @param tpf Time per frame.
     */
    @Override
    public void update(final float tpf) {
        if (enabled_ == true && spatialBO_ != null) {
            super.setPhysicsLocation(this.getSpatialTranslation());
            super.setPhysicsRotation(this.getSpatialRotation());
        }
    }

    /**
     * The method is called by the RenderManager prior to queuing the spatial. 
     * This should not be called from user code.
     *
     * @param rm The render manager.
     * @param vp The view port.
     */
    @Override
    public void render(final RenderManager rm, final ViewPort vp) {
        // Nothing to render.
    }
    
    /**
     * The method reads the object from the importer.
     * 
     * @param im The importer.
     * 
     * @throws IOException If an input/output exceptions has occurred.
     */
    @Override
    public void read(final JmeImporter im) throws IOException {
        super.read(im);
        InputCapsule ic_ = im.getCapsule(this);
        enabled_ = ic_.readBoolean("enabled", true);
        spatialBO_ = (Spatial) ic_.readSavable("spatial", null);
        isApplyPhysicsLocal_ = ic_.readBoolean("applyLocalPhysics", false);
        this.setUserObject(spatialBO_);
    }

    /**
     * The method writes the object to the exporter.
     * 
     * @param ex The exporter.
     * 
     * @throws IOException If an input/output exceptions has occurred.
     */
    @Override
    public void write(final JmeExporter ex) throws IOException {
        super.write(ex);
        OutputCapsule oc_ = ex.getCapsule(this);
        oc_.write(enabled_, "enabled", true);
        oc_.write(isApplyPhysicsLocal_, "applyLocalPhysics", false);
        oc_.write(spatialBO_, "spatial", null);
    }
    
    /**
     * The method is implemented to perform deep cloning for this object,
     * resolving local cloned references using the specified cloner. The object
     * can call cloner.clone(fieldValue) to deep clone any of its fields.
     * <p>
     * <p>
     * Note: during normal clone operations the original object will not be
     * needed as the clone has already had all of the fields shallow copied.</p>
     *
     * @param cloner The cloner that is performing the cloning operation. The
     * cloneFields method can call back into the cloner to make clones if its
     * subordinate fields.
     * @param original The original object from which this object was cloned.
     * This is provided for the very rare case that this object needs to refer
     * to its original for some reason. In general, all of the relevant values
     * should have been transferred during the shallow clone and this object
     * need merely clone what it wants.
     */
    @Override
    public void cloneFields(final Cloner cloner, final Object original) {
        spatialBO_ = cloner.clone(spatialBO_);
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    /**
     * The method applies the saved scale to the models.
     */
    private void applyScale() {
        float scale_ = scaleBO_.getFloatValue();
        spatialBO_.scale(scale_);
        cshBO_.setScale(new Vector3f(scale_, scale_, scale_));
    }
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}