/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kampfplatz;

import org.dga.client.DgaJmeGameApplication;
import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.input.InputManager;
import com.jme3.scene.Node;
import org.dga.client.DgaJmeScale;

/**
 * The abstract implementation of a battlefield.
 * 
 * @extends AbstractAppState
 * @implements Battlefield
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 09.08.2018
 */
public abstract class AbstractBattlefield extends AbstractAppState implements Battlefield {
    private DgaJmeScale battlefieldScale_ = null;
    private DgaJmeGameApplication app_ = null;
    private Node rootNode_ = null;
    private AssetManager assetManager_ = null;
    private InputManager inputManager_ = null;
    private BulletAppState bulletAppState_ = null;
    private PhysicsSpace physicsSpace_ = null;
    private AppStateManager stateManager_ = null;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates an instance with the specified scale.
     * 
     * @param battlefieldScale The battlefield's scale.
     */
    public AbstractBattlefield(final DgaJmeScale battlefieldScale) {
        super();
        battlefieldScale_ = battlefieldScale;
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method is called by {@link AppStateManager} when transitioning this
     * {@code AppState} from <i>initializing</i> to <i>running</i>.<br>
     * This will happen on the next iteration through the update loop after
     * {@link AppStateManager#attach(com.jme3.app.state.AppState) } was called.
     * <p>
     * The <code>AppStateManager</code> will call this only from the update loop
     * inside the rendering thread. This means it is safe to modify the scene 
     * graph from this method.
     *
     * @param stateManager The state manager.
     * @param app The application.
     */
    @Override
    public void initialize(final AppStateManager stateManager, final Application app) {
        super.initialize(stateManager, app);
        stateManager_ = stateManager;
        app_ = (DgaJmeGameApplication) app;
        rootNode_ = app_.getRootNode();
        assetManager_ = app_.getAssetManager();
        inputManager_ = app_.getInputManager();
        bulletAppState_ = stateManager_.getState(BulletAppState.class);
        physicsSpace_ = bulletAppState_.getPhysicsSpace();
        //
        this.initModel();
        this.addToScene();
    }
    
    /**
     * The method initializes the battlefield's spatials and controls.
     */
    @Override
    public abstract void initModel();
    
    /**
     * The method adds nodes, controls, and other required parts to the scene.
     */
    @Override
    public abstract void addToScene();
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}