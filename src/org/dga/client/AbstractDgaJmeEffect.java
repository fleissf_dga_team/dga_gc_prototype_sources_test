/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client;

import com.jme3.audio.AudioNode;
import com.jme3.effect.ParticleEmitter;
import com.jme3.scene.Node;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * The abstract implementation for game objects receiving different types of effects.
 * 
 * @extends java.lang.Object
 * @implements DgaJmeEffect
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 26.08.2018
 */
public abstract class AbstractDgaJmeEffect extends Object implements DgaJmeEffect {
    private Node nodeEffect_ = new Node("Effect");
    private final List<ParticleEmitter> VISUAL_START_EFFECTS_ = 
        Collections.synchronizedList(new LinkedList<>());
    private final List<ParticleEmitter> VISUAL_END_EFFECTS_ = 
        Collections.synchronizedList(new LinkedList<>());
    private final List<AudioNode> AUDIO_START_EFFECTS_ = 
        Collections.synchronizedList(new LinkedList<>());
    private final List<AudioNode> AUDIO_END_EFFECTS_ = 
        Collections.synchronizedList(new LinkedList<>());
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new instance.
     */
    public AbstractDgaJmeEffect() {
        super();
    }
    
    /**
     * The constructor creates a new instance with the supplied properties.
     * 
     * @param effectNode The effect's node.
     */
    public AbstractDgaJmeEffect(final Node effectNode) {
        super();
        nodeEffect_ = effectNode;
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the action this effect is intended for.
     * 
     * @return DgaJmeAction The action this effect is intended for.
     */
    @Override
    public abstract DgaJmeAction getAction();
    
    /**
     * The method returns the effect's node.
     * 
     * @return Node The effect's node.
     */
    @Override
    public Node getNode() {
        return nodeEffect_;
    }
    
    /**
     * The method adds a visual start effect for the effect.
     * 
     * @param visualEffect A visual start effect to the effect.
     */
    @Override
    public void addVisualEffectEmit(final ParticleEmitter visualEffect) {
        VISUAL_START_EFFECTS_.add(visualEffect);
        if (this.getNode().hasChild(visualEffect) == false) {
            this.getNode().attachChild(visualEffect);
        }
    }
    
    /**
     * The method removes a visual start effect from the effect.
     * 
     * @param visualEffect A visual start effect to be removed from the effect.
     */
    @Override
    public void removeVisualEffectEmit(final ParticleEmitter visualEffect) {
        // If the visual effect is in the list of the start effects.
        if (VISUAL_START_EFFECTS_.contains(visualEffect) == true) {
            VISUAL_START_EFFECTS_.remove(visualEffect);
        }
        // If the visual effect is not in the list of the end effects.
        if (VISUAL_END_EFFECTS_.contains(visualEffect) == false) {
            this.getNode().detachChild(visualEffect);
        }
    }
    
    /**
     * The method adds a visual end effect for the effect.
     * 
     * @param visualEffect A visual end effect to the effect.
     */
    @Override
    public void addVisualEffectKill(final ParticleEmitter visualEffect) {
        VISUAL_END_EFFECTS_.add(visualEffect);
        if (this.getNode().hasChild(visualEffect) == false) {
            this.getNode().attachChild(visualEffect);
        }
    }
    
    /**
     * The method removes a visual end effect from the effect.
     * 
     * @param visualEffect A visual end effect to be removed from the effect.
     */
    @Override
    public void removeVisualEffectKill(final ParticleEmitter visualEffect) {
        // If the visual effect is in the list of the end effects.
        if (VISUAL_END_EFFECTS_.contains(visualEffect) == true) {
            VISUAL_END_EFFECTS_.remove(visualEffect);
        }
        // If the visual effect is not in the list of the start effects.
        if (VISUAL_START_EFFECTS_.contains(visualEffect) == false) {
            this.getNode().detachChild(visualEffect);
        }
    }
    
    /**
     * The method adds an audio start effect to the effect.
     * 
     * @param audioEffect An audio start effect to the effect.
     */
    @Override
    public void addAudioEffectStart(final AudioNode audioEffect) {
        AUDIO_START_EFFECTS_.add(audioEffect);
        if (this.getNode().hasChild(audioEffect) == false) {
            this.getNode().attachChild(audioEffect);
        }
    }
    
    /**
     * The method removes an audio start effect from the effect.
     * 
     * @param audioEffect An audio start effect to be removed from the effect.
     */
    @Override
    public void removeAudioEffectStart(final AudioNode audioEffect) {
        // If the audio effect is in the list of the start effects.
        if (AUDIO_START_EFFECTS_.contains(audioEffect) == true) {
            AUDIO_START_EFFECTS_.remove(audioEffect);
        }
        // If the audio effect is not in the list of the end effects.
        if (AUDIO_END_EFFECTS_.contains(audioEffect) == false) {
            this.getNode().detachChild(audioEffect);
        }
    }
    
    /**
     * The method adds an audio end effect to the effect.
     * 
     * @param audioEffect An audio end effect to the effect.
     */
    @Override
    public void addAudioEffectStop(final AudioNode audioEffect) {
        AUDIO_END_EFFECTS_.add(audioEffect);
        if (this.getNode().hasChild(audioEffect) == false) {
            this.getNode().attachChild(audioEffect);
        }
    }
    
    /**
     * The method removes an audio end effect from the effect.
     * 
     * @param audioEffect An audio end effect to be removed from the effect.
     */
    @Override
    public void removeAudioEffectStop(final AudioNode audioEffect) {
        // If the audio effect is in the list of the end effects.
        if (AUDIO_END_EFFECTS_.contains(audioEffect) == true) {
            AUDIO_END_EFFECTS_.remove(audioEffect);
        }
        // If the audio effect is not in the list of the start effects.
        if (AUDIO_START_EFFECTS_.contains(audioEffect) == false) {
            this.getNode().detachChild(audioEffect);
        }
    }
    
    /**
     * The method returns the list of visual effects provided by the effect 
     * and executed at the beginning of the action. The list is sorted in 
     * the order in which the visual effects should be executed.
     * 
     * @return List<ParticleEmitter> The list of visual effects provided by the 
     * effect and executed at the beginning of the action. The list is 
     * sorted in the order in which the visual effects should be executed.
     */
    @Override
    public List<ParticleEmitter> getVisualEffectEmitList() {
        return VISUAL_START_EFFECTS_;
    }
    
    /**
     * The method returns the list of visual effects provided by the effect 
     * and executed at the end of the action. The list is sorted in the 
     * order in which the visual effects should be executed. Please notice that 
     * the lists of the start effects and end effects can differ.
     * 
     * @return List<ParticleEmitter> The list of visual effects provided by the 
     * effect and executed at the end of the action. The list is sorted 
     * in the order in which the visual effects should be executed. Please notice 
     * that the lists of the start effects and end effects can differ.
     */
    @Override
    public List<ParticleEmitter> getVisualEffectKillList() {
        return VISUAL_END_EFFECTS_;
    }
    
    /**
     * The method returns the list of audio effects provided by the effect 
     * and executed at the beginning of the action. The list is sorted in 
     * the order in which the audio effects should be executed.
     * 
     * @return List<ParticleEmitter> The list of audio effects provided by the 
     * effect and executed at the beginning of the action. The list is 
     * sorted in the order in which the audio effects should be executed.
     * 
     * @return List<AudioNode> .
     */
    @Override
    public List<AudioNode> getAudioEffectStartList() {
        return AUDIO_START_EFFECTS_;
    }
    
    /**
     * The method returns the list of audio effects provided by the effect 
     * and executed at the end of the action. The list is sorted in the 
     * order in which the audio effects should be executed. Please notice that 
     * the lists of the start effects and end effects can differ.
     * 
     * @return List<ParticleEmitter> The list of audio effects provided by the 
     * effect and executed at the end of the action. The list is sorted 
     * in the order in which the audio effects should be executed. Please notice 
     * that the lists of the start effects and end effects can differ.
     */
    @Override
    public List<AudioNode> getAudioEffectStopList() {
        return AUDIO_END_EFFECTS_;
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}