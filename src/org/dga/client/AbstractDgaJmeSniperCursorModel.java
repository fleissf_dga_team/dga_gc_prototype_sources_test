/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client;

import com.jme3.scene.Node;

/**
 * The abstract implementation for sniper cursors' models.
 * 
 * @extends AbstractDgaJmeModel
 * @implements DgaJmeSniperCursorModel
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 22.08.2018
 */
public abstract class AbstractDgaJmeSniperCursorModel extends AbstractDgaJmeModel 
    implements DgaJmeSniperCursorModel {
    private Node nodeModel_ = null;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new model with the supplied properties.
     * 
     * @param modelNode The model's node.
     * @param scale The model's scale.
     */
    public AbstractDgaJmeSniperCursorModel(final Node modelNode, final DgaJmeScale scale) {
        super(modelNode, scale);
        nodeModel_ = modelNode;
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the model's node.
     * 
     * @return Node The model's node.
     */
    @Override
    public Node getNode() {
        return nodeModel_;
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}