/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client;

import com.jme3.bounding.BoundingBox;
import com.jme3.bullet.collision.shapes.BoxCollisionShape;
import com.jme3.bullet.collision.shapes.HullCollisionShape;
import com.jme3.effect.ParticleMesh;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

/**
 * The utility class for common methods and functions.
 * 
 * @extends java.lang.Object
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 4.5.2018
 */
public final class DgaJmeUtils extends Object {
    //
    // *************************** Constructors ********************************
    //
    /**
     * The default constructor.
     */
    private DgaJmeUtils() {
        super();
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns a spatial found by name within the spatial passed as
     * the argument, or a null value if there is no spatial with such name.
     * 
     * @param spatial The root spatial.
     * @param name The name of the required spatial.
     * 
     * @return Spatial A spatial found by name within the spatial passed as
     * the argument, or a null value if there is no spatial with such name.
     */
    public static Spatial getSpatialByName(final Spatial spatial, final String name) {
        if (spatial instanceof Node) {
            Node node_ = (Node) spatial;
            for (int i_ = 0; i_ < node_.getQuantity(); i_++) {
                Spatial child_ = node_.getChild(i_);
                if (child_.getName().startsWith(name)) {
                    return child_;
                } else {
                    Spatial result_ = DgaJmeUtils.getSpatialByName(child_, name);
                    if (result_ != null) {
                        return result_;
                    }
                }
            }
        }
        return null;
    }
    
    /**
     * The method returns a geometry found by name within the spatial passed as
     * the argument, or a null value if there is no geometry with such name.
     * 
     * @param spatial The root spatial.
     * @param name The name of the required geometry.
     * 
     * @return Geometry A geometry found by name within the spatial passed as
     * the argument, or a null value if there is no geometry with such name.
     */
    public static Geometry getGeometryFromSpatialByName(final Spatial spatial, final String name) {
        if (spatial instanceof Node) {
            Node node_ = (Node) spatial;
            for (int i_ = 0; i_ < node_.getQuantity(); i_++) {
                Spatial child_ = node_.getChild(i_);
                Geometry result_ = DgaJmeUtils.getGeometryFromSpatialByName(child_, name);
                if (result_ != null) {
                    return result_;
                }
            }
        } else if (spatial instanceof Geometry) {
            if (spatial.getName().startsWith(name)) {
                return (Geometry) spatial;
            }
        }
        return null;
    }
    
    /**
     * The method converts local coordinates to world coordinates for the node
     * passed as the argument and returns its direction in the following format:
     * (0,0,1) is forward, (0,0,-1) is backward, (1,0,0) is right, and (0,1,0)
     * is upward.
     *
     * @param node A node.
     * 
     * @return Vector3f World coordinates of the node passed as the argument and
     * in the following format: (0,0,1) is forward, (0,0,-1) is backward, (1,0,0)
     * is right, and (0,1,0) is upward.
     */
    public static Vector3f getNodeForwardDirection(final Node node) {
        Vector3f frwd_ = new Vector3f(0, 0, 1);
        node.localToWorld(frwd_, frwd_);
        return frwd_;
    }
    
    /**
     * The method returns the offset (distance) on X-direction between the given spatials.
     * 
     * @param begin The start spatial.
     * @param end The end spatial.
     * @return float The offset (distance).
     */
    public static float calculateOffsetX(final Spatial begin, final Spatial end) {
        float offsetX_ = begin.getLocalTranslation().getX() - end.getLocalTranslation().getX();
        return offsetX_;
    }
    
    /**
     * The method returns the offset (distance) on Y-direction between the given spatials.
     * 
     * @param begin The start spatial.
     * @param end The end spatial.
     * @return float The offset (distance).
     */
    public static float calculateOffsetY(final Spatial begin, final Spatial end) {
        float offsetY_ = begin.getLocalTranslation().getY() - end.getLocalTranslation().getY();
        return offsetY_;
    }
    
    /**
     * The method returns the offset (distance) on Z-direction between the given spatials.
     * 
     * @param begin The start spatial.
     * @param end The end spatial.
     * @return float The offset (distance).
     */
    public static float calculateOffsetZ(final Spatial begin, final Spatial end) {
        float offsetZ_ = begin.getLocalTranslation().getZ() - end.getLocalTranslation().getZ();
        return offsetZ_;
    }

    /**
     * The method rotates a spatial around a pivot point on a distance by an 
     * angle and axis, and returns the initial spatial.
     * 
     * @param rotatable A spatial to rotate around.
     * @param pivotPoint A pivot point the spatial is going to be rotated around.
     * @param isChild The flag whether the spatial is a child of the pivot point or not.
     * @param distance A distance between the spatial and pivot point.
     * @param angle An angle by which the spatial is going to be rotated around.
     * @param axis An axis around which the spatial is going to be rotated.
     * @return Spatial The rotated spatial.
     */
    public static Spatial rotateAround(final Spatial rotatable, final Spatial pivotPoint, 
        final boolean isChild, final float distance, final float angle, final DgaJmeAxis axis) {
        float offset1_ = distance * FastMath.sin(angle);
        float offset2_ = distance * FastMath.cos(angle);
        float trans1_ = 0f;
        float trans2_ = 0f;
        switch (axis) {
            case AXIS_X:
                trans2_ = pivotPoint.getLocalTranslation().getZ();
                if (isChild == true) {
                    rotatable.getLocalTranslation().setY(offset1_);
                } else {
                    trans1_ = pivotPoint.getLocalTranslation().getY();
                    rotatable.getLocalTranslation().setY(trans1_ + offset1_);
                }
                rotatable.getLocalTranslation().setZ(trans2_ + offset2_);
                break;
            case AXIS_Y:
                //rotatable.getLocalTranslation().setX(pivotPoint.getLocalTranslation().getX() + offset1_);
                //rotatable.getLocalTranslation().setZ(pivotPoint.getLocalTranslation().getZ() + offset2_);
                break;
            case AXIS_Z:
                //rotatable.getLocalTranslation().setY(pivotPoint.getLocalTranslation().getY() + offset1_);
                //rotatable.getLocalTranslation().setX(pivotPoint.getLocalTranslation().getX() + offset2_);
                break;
            default:
                trans2_ = pivotPoint.getLocalTranslation().getZ();
                if (isChild == true) {
                    rotatable.getLocalTranslation().setY(offset1_);
                } else {
                    trans1_ = pivotPoint.getLocalTranslation().getY();
                    rotatable.getLocalTranslation().setY(trans1_ + offset1_);
                }
                rotatable.getLocalTranslation().setZ(trans2_ + offset2_);
                break;
        }
        return rotatable;
    }
    //
    // *************************************************************************
    //
    /**
     * The method creates a new box collision shape around the provided spatial. 
     * If the spatial parameter has the <code>null</code> value then an empty box 
     * collision shape will be returned.
     * 
     * @param spatial The spatial.
     * @return BoxCollisionShape A new box collision shape created around the spatial.
     */
    public static BoxCollisionShape createBoxCollisionShape(final Spatial spatial) {
        if (spatial != null) {
            BoundingBox bb_ = (BoundingBox) spatial.getWorldBound();
            BoxCollisionShape bcsh_ = new BoxCollisionShape(bb_.getExtent(null));
            return bcsh_;
        }
        return new BoxCollisionShape();
    }
    
    /**
     * The method creates a new hull collision shape around the geometry located 
     * inside the provided spatial with the provided name. If one or more 
     * parameters have the <code>null</code> value then an empty hull collision 
     * shape will be returned.
     * 
     * @param spatial The spatial.
     * @param name The name of the geometry located inside the spatial.
     * @return HullCollisionShape A new hull collision shape created around the 
     * geometry located inside the spatial or an empty hull collision shape if 
     * one or more parameters have the <code>null</code> value.
     */
    public static HullCollisionShape createHullCollisionShape(final Spatial spatial, 
        final String name) {
        if (spatial != null && name != null) {
            Geometry geometry_ = DgaJmeUtils.getGeometryFromSpatialByName(spatial, name);
            if (geometry_ != null) {
                HullCollisionShape hcsh_ = new HullCollisionShape(geometry_.getMesh());
                return hcsh_;
            }
        }
        return new HullCollisionShape();
    }
    
    /**
     * The method returns the type of particle emitter.
     *
     * @param isPointSprite The flag whether the particle emitter has to be of
     * the point type or of triangle type.
     *
     * @return ParticleMesh.Type The type of particle emitter.
     */
    public static ParticleMesh.Type getParticleEmitterType(final boolean isPointSprite) {
        if (isPointSprite == true) {
            return ParticleMesh.Type.Point;
        } else {
            return ParticleMesh.Type.Triangle;
        }
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
