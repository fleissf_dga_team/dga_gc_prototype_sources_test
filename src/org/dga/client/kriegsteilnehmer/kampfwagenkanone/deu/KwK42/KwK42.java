/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsteilnehmer.kampfwagenkanone.deu.KwK42;

import static org.dga.client.DgaJmeCommonConstant.IS_IN_WEAPON_DEV_MODE;

import org.dga.client.DefaultDgaJmeScale;
import org.dga.client.kombattant.Kombattant;
import org.dga.client.kombattant.kampffahrzeug.artillerie.Kanonenpatrone;
import org.dga.client.kriegsteilnehmer.kampfwagenkanone.deu.AbstractDeuPanzerkanone;
import org.dga.client.kriegsteilnehmer.kampfwagenkanone.deu.patrone.mm75.Pzgr39_42;
import org.dga.client.kriegsteilnehmer.kampfwagenkanone.deu.patrone.mm75.Pzgr40_42;
import org.dga.client.kriegsteilnehmer.kampfwagenkanone.deu.patrone.mm75.Sprgr42;
import org.dga.client.kombattant.kampffahrzeug.artillerie.kampfwagenkanone.Panzerkanone;
import org.dga.client.kombattant.kampffahrzeug.artillerie.kampfwagenkanone.PanzerkanoneModel;
import org.dga.client.kriegsmittel.projektil.granate.Granate;
import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.effect.ParticleEmitter;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import com.jme3.renderer.RenderManager;
import org.dga.client.DgaJmeCommonConstant;
import org.dga.client.effekt.FireEffect;
import org.dga.client.DgaJmeScale;

/**
 * The german battle tank cannon 7,5 cm Kampfwagenkanone Kw.K. 42 L/70 (75mm KwK L/70).
 *
 * @extends AbstractDeuPanzerkanone
 * @implements Panzerkanone
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 7.5.2018
 */
public final class KwK42 extends AbstractDeuPanzerkanone implements Panzerkanone {
    // The weapon's name.
    private static final String MODEL_NAME_ = "7,5 cm Kampfwagenkanone Kw.K. 42 L/70";
    // The cannon's mass in kilograms.
    private static final float MODEL_MASS_ = 1000.0f;
    // The cannon's ammunition list.
    private static final List<Kanonenpatrone> MUNITION_LIST_ = Collections.synchronizedList(new LinkedList<>());
    // The time the gun needs to be loaded with a round.
    private static final float LOAD_TIME_ = 10f;
    // The gun's cartridge capacity.
    private static final int CARTRIDGE_CAPACITY_ = 1;
    //
    private final float FIRE_EFFECT_MAX_TIME_ = 
        DgaJmeCommonConstant.FIRE_EFFECT_MAX_EFFECTS_TIME.getFloatValue();
    private Kombattant kombattant_ = null;
    private PanzerkanoneModel modelKfpkt_ = null;
    private Node nodeKPivotPoint_ = null;
    private AssetManager assetManager_ = null;
    private Node rootNode_ = null;
    private RenderManager renderManager_ = null;
    private PhysicsSpace physicsSpace_ = null;
    private CollisionShape kfpktShape_ = null;
    private Spatial kfpktSpatial_ = null;
    private DgaJmeScale kScale_ = DefaultDgaJmeScale.ORIGINAL_MODEL_SCALE;
    private Kanonenpatrone currentPatrone_ = Kanonenpatrone.PANZERGRANATE;
    private int fireCounter_ = 0;
    private Node nodeShootingPoint_ = null;
    private FireEffect effectFire_ = null;
    private Node nodeFireEffect_ = null;
    private float timer_ = -0.1f;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new physics control with the supplied properties.
     * 
     * @param panzerkanonenfeuerpunktModel The cannon's firing point model.
     */
    public KwK42(final PanzerkanoneModel panzerkanonenfeuerpunktModel) {
        super(panzerkanonenfeuerpunktModel);
        super.setLoadTime(LOAD_TIME_);
        modelKfpkt_ = panzerkanonenfeuerpunktModel;
        assetManager_ = modelKfpkt_.getAssetManager();
        physicsSpace_ = modelKfpkt_.getPhysicsSpace();
        rootNode_ = modelKfpkt_.getRootNode();
        renderManager_ = modelKfpkt_.getRenderManager();
        kombattant_ = modelKfpkt_.getKombattant();
        nodeKPivotPoint_ = modelKfpkt_.getPivotPoint();
        kfpktSpatial_ = modelKfpkt_.getSpatial();
        kfpktShape_ = modelKfpkt_.getCollisionShape();
        kScale_ = modelKfpkt_.getScale();
        nodeShootingPoint_ = modelKfpkt_.getShootingPoint();
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the cannon's name.
     *
     * @return String The cannon's name.
     */
    @Override
    public String getName() {
        return MODEL_NAME_;
    }
    
    /**
     * The method returns the cannon's mass in kilograms.
     * 
     * @return float The cannon's mass in kilograms.
     */
    @Override
    public float getMass() {
        return MODEL_MASS_;
    }
    
    /**
     * The method returns the capacity of the shooting armament's cartridge.
     * 
     * @return int The capacity of the shooting armament's cartridge.
     */
    @Override
    public int getCartridgeCapacity() {
        return CARTRIDGE_CAPACITY_;
    }

    /**
     * The method returns a list of ammunition of the cannon (gun).
     *
     * @return List<Kanonenpatrone> The list of ammunition of the cannon (gun).
     */
    @Override
    public List<Kanonenpatrone> getMunitionList() {
        if (MUNITION_LIST_.isEmpty() == true) {
            MUNITION_LIST_.add(Kanonenpatrone.PANZERGRANATE);
            MUNITION_LIST_.add(Kanonenpatrone.PANZERHARTKERNGRANATE);
            MUNITION_LIST_.add(Kanonenpatrone.SPRENGGRANATE);
        }
        return MUNITION_LIST_;
    }

    /**
     * The method loads the cannon with the specified type of rounds
     * (cartridges).
     *
     * @param patroneType The type of the cannon's rounds (cartridges).
     */
    @Override
    public void load(final Kanonenpatrone patroneType) {
        switch (patroneType) {
            case PANZERGRANATE:
                currentPatrone_ = patroneType;
                break;
            case PANZERHARTKERNGRANATE:
                currentPatrone_ = patroneType;
                break;
            case SPRENGGRANATE:
                currentPatrone_ = patroneType;
                break;
            default:
                currentPatrone_ = Kanonenpatrone.PANZERGRANATE;
                break;
        }
    }

    /**
     * The method realizes a shot from the firearm.
     */
    @Override
    public void fire() {
        if (this.isFireReady() == true) {
            //Vector3f granatePosition_ = this.getWaffeWorldTranslation().add(0f, 0f, 0f);
            Vector3f granatePosition_ = this.getWaffeWorldTranslation();
            Quaternion granateRotation_ = this.getWaffeWorldRotation();
            Vector3f granateDirection_ = granateRotation_.getRotationColumn(2);
            Granate granate_ = null;
            switch (currentPatrone_) {
                case PANZERGRANATE:
                    granate_ = new Pzgr39_42(assetManager_, kombattant_, this.getMunitionScale());
                    break;
                case PANZERHARTKERNGRANATE:
                    granate_ = new Pzgr40_42(assetManager_, kombattant_, this.getMunitionScale());
                    break;
                case SPRENGGRANATE:
                    granate_ = new Sprgr42(assetManager_, kombattant_, this.getMunitionScale());
                    break;
                default:
                    granate_ = new Pzgr39_42(assetManager_, kombattant_, this.getMunitionScale());
                    break;
            }
            granate_.setLinearVelocity(granateDirection_.mult(granate_.getVelocity()));
            // The class hierarchy and the distribution of responsibility among classes
            // here are determined by the jME requirements; other variants did not work.
            Spatial spatialGranate_ = granate_.getSpatial();
            spatialGranate_.rotate(granateRotation_);
            spatialGranate_.setLocalTranslation(granatePosition_.addLocal(0f, 0f, 0f));
            spatialGranate_.setLocalRotation(granateRotation_);
            spatialGranate_.setShadowMode(RenderQueue.ShadowMode.Cast);
            //BoundingBox bbGranate_ = (BoundingBox) spatialGranate_.getWorldBound();
            //BoxCollisionShape cshGranate_ = new BoxCollisionShape(bbGranate_.getExtent(null));
            //granate_.setCollisionGroup(PhysicsCollisionObject.COLLISION_GROUP_02);
            spatialGranate_.addControl(granate_);
            rootNode_.attachChild(spatialGranate_);
            physicsSpace_.add(spatialGranate_);
            //
            effectFire_ = this.getFireEffect();
            nodeFireEffect_ = effectFire_.getNode();
            nodeFireEffect_.setLocalTranslation(nodeShootingPoint_.getLocalTranslation());
            nodeShootingPoint_.attachChild(nodeFireEffect_);
            timer_ = 0.1f;
            //
            // Switch the gun to load.
            if (IS_IN_WEAPON_DEV_MODE == false) {
                this.setFireReady(false);
            }
        }
    }
    
    /**
     * The method updates the control. This should not be called from the user code.
     *
     * @param tpf Time per frame.
     */
    @Override
    public void update(final float tpf) {
        super.update(tpf);
        if (this.isEnabled() == true && nodeFireEffect_ != null) {
            timer_ += tpf;
            if (timer_ > 0f && timer_ < FIRE_EFFECT_MAX_TIME_) {
                for (ParticleEmitter pe_ : effectFire_.getVisualEffectEmitList()) {
                    pe_.emitAllParticles();
                }
                for (AudioNode an_ : effectFire_.getAudioEffectStartList()) {
                    an_.play();
                }
            } else {
                timer_ = -0.1f;
                for (ParticleEmitter pe_ : effectFire_.getVisualEffectKillList()) {
                    pe_.killAllParticles();
                }
                for (AudioNode an_ : effectFire_.getAudioEffectStopList()) {
                    an_.stop();
                }
            }
        }
    }
    //
    // *************************************************************************
    //
    /**
     * The method creates a clone of the control, the given spatial is the
     * cloned version of the spatial to which this control is attached to.
     *
     * @param spatial The spatial.
     *
     * @return Control A clone of this control for the spatial.
     */
    @Override
    public Control cloneForSpatial(final Spatial spatial) {
        KwK42 control_ = new KwK42(modelKfpkt_);
//        control_.setAngularFactor(getAngularFactor());
//        control_.setAngularSleepingThreshold(getAngularSleepingThreshold());
//        control_.setCcdMotionThreshold(getCcdMotionThreshold());
//        control_.setCcdSweptSphereRadius(getCcdSweptSphereRadius());
//        control_.setCollideWithGroups(getCollideWithGroups());
//        control_.setCollisionGroup(getCollisionGroup());
//        control_.setDamping(getLinearDamping(), getAngularDamping());
//        control_.setFriction(getFriction());
//        control_.setGravity(getGravity());
//        control_.setKinematic(isKinematic());
//        control_.setKinematicSpatial(isKinematicSpatial());
//        control_.setLinearSleepingThreshold(getLinearSleepingThreshold());
//        control_.setPhysicsLocation(getPhysicsLocation(null));
//        control_.setPhysicsRotation(getPhysicsRotationMatrix(null));
//        control_.setRestitution(getRestitution());
//        if (mass > 0) {
//            control_.setAngularVelocity(getAngularVelocity());
//            control_.setLinearVelocity(getLinearVelocity());
//        }
//        control_.setApplyPhysicsLocal(isApplyPhysicsLocal());
        return control_;
    }

    /**
     * The method performs a regular shallow clone of the object. Some fields
     * may also be cloned but generally only if they will never be shared with
     * other objects. (For example, local Vector3fs and so on.)
     * <p>
     * <p>
     * This method is separate from the regular clone() method so that objects
     * might still maintain their own regular java clone() semantics (perhaps
     * even using Cloner for those methods). However, because Java's clone() has
     * specific features in the sense of Object's clone() implementation, it's
     * usually best to have some path for subclasses to bypass the public
     * clone() method that might be cloning fields and instead get at the
     * superclass protected clone() methods. For example, through
     * super.jmeClone() or another protected clone method that some base class
     * eventually calls super.clone() in.</p>
     *
     * @return Object The clone of the object.
     */
    @Override
    public Object jmeClone() {
        KwK42 control_ = new KwK42(modelKfpkt_);
//        control_.setAngularFactor(getAngularFactor());
//        control_.setAngularSleepingThreshold(getAngularSleepingThreshold());
//        control_.setCcdMotionThreshold(getCcdMotionThreshold());
//        control_.setCcdSweptSphereRadius(getCcdSweptSphereRadius());
//        control_.setCollideWithGroups(getCollideWithGroups());
//        control_.setCollisionGroup(getCollisionGroup());
//        control_.setDamping(getLinearDamping(), getAngularDamping());
//        control_.setFriction(getFriction());
//        control_.setGravity(getGravity());
//        control_.setKinematic(isKinematic());
//        control_.setKinematicSpatial(isKinematicSpatial());
//        control_.setLinearSleepingThreshold(getLinearSleepingThreshold());
//        control_.setPhysicsLocation(getPhysicsLocation(null));
//        control_.setPhysicsRotation(getPhysicsRotationMatrix(null));
//        control_.setRestitution(getRestitution());
//        if (mass > 0) {
//            control_.setAngularVelocity(getAngularVelocity());
//            control_.setLinearVelocity(getLinearVelocity());
//        }
//        control_.setApplyPhysicsLocal(isApplyPhysicsLocal());
//        control_.spatial_ = this.spatial_;
//        control_.setEnabled(isEnabled());
        return control_;
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
