/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsteilnehmer.kampfwagenkanone.deu;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioData;
import com.jme3.audio.AudioNode;
import org.dga.client.kombattant.kampffahrzeug.artillerie.kampfwagenkanone.AbstractPanzerkanone;
import org.dga.client.kombattant.kampffahrzeug.artillerie.kampfwagenkanone.PanzerkanoneModel;
import org.dga.client.effekt.DefaultFireEffect;
import org.dga.client.effekt.FireEffect;

/**
 * The abstract implementation of a german battle tank's cannon (gun).
 *
 * @extends AbstractPanzerkanone
 * @implements DeuPanzerkanone
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 7.5.2018
 */
public abstract class AbstractDeuPanzerkanone extends AbstractPanzerkanone 
    implements DeuPanzerkanone {
    private AssetManager assetManager_ = null;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new control with the supplied properties.
     * 
     * @param kanoneModel The cannon's model.
     */
    public AbstractDeuPanzerkanone(final PanzerkanoneModel kanoneModel) {
        super(kanoneModel);
        assetManager_ = kanoneModel.getAssetManager();
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns an effect provided by the shooting armament during a shot.
     * 
     * @return FireEffect An effect provided by the shooting armament during a shot.
     */
    @Override
    public FireEffect getFireEffect() {
        return DefaultFireEffect.newBuilder(assetManager_)
            .addFlame()
            .addFlash()
            .addSmoke()
            .addSound(
                new AudioNode(
                    assetManager_,
                    "Models/Kriegsteilnehmer/Kampfwagenkanone/DEU/Sounds/GermanBattleTankCannonShot.wav",
                    AudioData.DataType.Buffer)
            )
            .build();
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
