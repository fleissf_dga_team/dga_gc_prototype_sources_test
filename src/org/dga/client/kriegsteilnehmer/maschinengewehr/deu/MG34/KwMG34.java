/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsteilnehmer.maschinengewehr.deu.MG34;

import static org.dga.client.DgaJmeCommonConstant.IS_IN_WEAPON_DEV_MODE;

import org.dga.client.DefaultDgaJmeScale;
import org.dga.client.kombattant.Kombattant;
import org.dga.client.kombattant.infanterie.handwaffe.Schiesspatrone;
import org.dga.client.kriegsteilnehmer.maschinengewehr.deu.AbstractDeuMaschinengewehr;
import org.dga.client.kriegsteilnehmer.maschinengewehr.deu.patrone.mm7_92x57.Patrone7_92x57mmPmK;
import org.dga.client.kriegsteilnehmer.maschinengewehr.deu.patrone.mm7_92x57.Patrone7_92x57mmSS;
import org.dga.client.kriegsteilnehmer.maschinengewehr.deu.patrone.mm7_92x57.Patrone7_92x57mmSmK;
import org.dga.client.kriegsteilnehmer.maschinengewehr.deu.patrone.mm7_92x57.Patrone7_92x57mmSmKH;
import org.dga.client.kombattant.infanterie.handwaffe.maschinengewehr.Maschinengewehr;
import org.dga.client.kombattant.infanterie.handwaffe.maschinengewehr.MaschinengewehrModel;
import org.dga.client.kriegsmittel.projektil.geschoss.Geschoss;
import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.effect.ParticleEmitter;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.dga.client.DgaJmeCommonConstant;
import org.dga.client.effekt.FireEffect;
import org.dga.client.DgaJmeScale;

/**
 * The german battle tanks' machine gun 7,92 mm Maschinengewehr MG 34 Panzerlauf (Kw.MG 34).
 *
 * @extends AbstractDeutschesMaschinengewehr
 * @extends com.jme3.bullet.objects.PhysicsRigidBody
 * @implements Maschinengewehr
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 7.5.2018
 */
public final class KwMG34 extends AbstractDeuMaschinengewehr implements Maschinengewehr {
    // The weapon's name.
    private static final String MODEL_NAME_ = "MG 34 Panzerlauf Kw.MG 34";
    // The machine gun's mass in kilograms.
    private static final float MODEL_MASS_ = 12.1f;
    // The machine gun's ammunition list.
    private static final List<Schiesspatrone> MUNITION_LIST_ = Collections.synchronizedList(new LinkedList<>());
    // The time the machine gun needs to be loaded with a new cartridge.
    private static final float LOAD_TIME_ = 10f;
    // The gun's cartridge capacity.
    private static final int CARTRIDGE_CAPACITY_ = 150;
    //
    private final float FIRE_EFFECT_MAX_TIME_ = 
        DgaJmeCommonConstant.FIRE_EFFECT_MAX_EFFECTS_TIME.getFloatValue();
    private Kombattant kombattant_ = null;
    private MaschinengewehrModel modelMGfpkt_ = null;
    private AssetManager assetManager_ = null;
    private Node rootNode_ = null;
    private PhysicsSpace physicsSpace_ = null;
    private CollisionShape mgfpktShape_ = null;
    private Spatial mgfpktSpatial_ = null;
    private Node mgPivotPoint_ = null;
    private DgaJmeScale mgScale_ = DefaultDgaJmeScale.ORIGINAL_MODEL_SCALE;
    private Schiesspatrone currentPatrone_ = Schiesspatrone.SCHWERESSPITZGESCHOSS;
    private int fireCounter_ = 0;
    private Node nodeShootingPoint_ = null;
    private FireEffect effectFire_ = null;
    private Node nodeFireEffect_ = null;
    private float timer_ = -0.1f;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new physics control with the supplied properties.
     * 
     * @param maschinengewehrfeuerpunktModel The machine gun's firing point model.
     */
    public KwMG34(final MaschinengewehrModel maschinengewehrfeuerpunktModel) {
        super(maschinengewehrfeuerpunktModel);
        super.setLoadTime(LOAD_TIME_);
        modelMGfpkt_ = maschinengewehrfeuerpunktModel;
        assetManager_ = modelMGfpkt_.getAssetManager();
        physicsSpace_ = modelMGfpkt_.getPhysicsSpace();
        rootNode_ = modelMGfpkt_.getRootNode();
        kombattant_ = modelMGfpkt_.getKombattant();
        mgfpktSpatial_ = modelMGfpkt_.getSpatial();
        mgfpktShape_ = modelMGfpkt_.getCollisionShape();
        mgPivotPoint_ = modelMGfpkt_.getPivotPoint();
        mgScale_ = modelMGfpkt_.getScale();
        nodeShootingPoint_ = modelMGfpkt_.getShootingPoint();
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the machine gun's name.
     *
     * @return String The machine gun's name.
     */
    @Override
    public String getName() {
        return MODEL_NAME_;
    }
    
    /**
     * The method returns the machine gun's mass in kilograms.
     * 
     * @return float The machine gun's mass in kilograms.
     */
    @Override
    public float getMass() {
        return MODEL_MASS_;
    }
    
    /**
     * The method returns the capacity of the shooting armament's cartridge.
     * 
     * @return int The capacity of the shooting armament's cartridge.
     */
    @Override
    public int getCartridgeCapacity() {
        return CARTRIDGE_CAPACITY_;
    }

    /**
     * The method returns a list of ammunition of the cannon (gun).
     *
     * @return List<Schiesspatrone> The list of ammunition of the cannon (gun).
     */
    @Override
    public List<Schiesspatrone> getMunitionList() {
        if (MUNITION_LIST_.isEmpty() == true) {
            MUNITION_LIST_.add(Schiesspatrone.SCHWERESSPITZGESCHOSS);
            MUNITION_LIST_.add(Schiesspatrone.STAHLKERNSPITZGESCHOSS);
            MUNITION_LIST_.add(Schiesspatrone.HARTKERNSPITZGESCHOSS);
            MUNITION_LIST_.add(Schiesspatrone.PHOSPHORKERNSPITZGESCHOSS);
        }
        return MUNITION_LIST_;
    }

    /**
     * The method loads the firearm with the specified type of rounds
     * (cartridges).
     *
     * @param patroneType The type of the firearm's rounds (cartridges).
     */
    @Override
    public void load(final Schiesspatrone patroneType) {
        switch (patroneType) {
            case SCHWERESSPITZGESCHOSS:
                currentPatrone_ = patroneType;
                break;
            case STAHLKERNSPITZGESCHOSS:
                currentPatrone_ = patroneType;
                break;
            case HARTKERNSPITZGESCHOSS:
                currentPatrone_ = patroneType;
                break;
            case PHOSPHORKERNSPITZGESCHOSS:
                currentPatrone_ = patroneType;
                break;
            default:
                currentPatrone_ = Schiesspatrone.SCHWERESSPITZGESCHOSS;
                break;
        }
    }

    /**
     * The method realizes a shot from the firearm.
     */
    @Override
    public void fire() {
        if (this.isFireReady() == true) {
            Vector3f geschossPosition_ = this.getWaffeWorldTranslation().addLocal(-2.0f, 2.0f, 0f);
            Quaternion geschossRotation_ = this.getWaffeWorldRotation();
            Vector3f geschossDirection_ = geschossRotation_.getRotationColumn(2);
            Geschoss geschoss_ = null;
            switch (currentPatrone_) {
                case SCHWERESSPITZGESCHOSS:
                    geschoss_ = new Patrone7_92x57mmSS(
                        assetManager_, kombattant_, this.getMunitionScale());
                    break;
                case STAHLKERNSPITZGESCHOSS:
                    geschoss_ = new Patrone7_92x57mmSmK(
                        assetManager_, kombattant_, this.getMunitionScale());
                    break;
                case HARTKERNSPITZGESCHOSS:
                    geschoss_ = new Patrone7_92x57mmSmKH(
                        assetManager_, kombattant_, this.getMunitionScale());
                    break;
                case PHOSPHORKERNSPITZGESCHOSS:
                    geschoss_ = new Patrone7_92x57mmPmK(
                        assetManager_, kombattant_, this.getMunitionScale());
                    break;
                default:
                    geschoss_ = new Patrone7_92x57mmSS(
                        assetManager_, kombattant_, this.getMunitionScale());
                    break;
            }
            geschoss_.setLinearVelocity(geschossDirection_.mult(geschoss_.getVelocity()));
            // The class hierarchy and the distribution of responsibility among classes
            // here are determined by the jME requirements; other variants did not work.
            Spatial spatialGeschoss_ = geschoss_.getSpatial();
            spatialGeschoss_.rotate(geschossRotation_);
            spatialGeschoss_.setLocalTranslation(geschossPosition_.addLocal(0f, 0f, 0f));
            spatialGeschoss_.setLocalRotation(geschossRotation_);
            spatialGeschoss_.setShadowMode(RenderQueue.ShadowMode.Cast);
            //BoundingBox bbGeschoss_ = (BoundingBox) spatialGeschoss_.getWorldBound();
            //BoxCollisionShape cshGeschoss_ = new BoxCollisionShape(bbGeschoss_.getExtent(null));
            //geschoss_.setCollisionGroup(PhysicsCollisionObject.COLLISION_GROUP_01);
            spatialGeschoss_.addControl(geschoss_);
            rootNode_.attachChild(spatialGeschoss_);
            physicsSpace_.add(spatialGeschoss_);
            //
            effectFire_ = this.getFireEffect();
            nodeFireEffect_ = effectFire_.getNode();
            nodeFireEffect_.setLocalTranslation(nodeShootingPoint_.getLocalTranslation());
            nodeShootingPoint_.attachChild(nodeFireEffect_);
            timer_ = 0.1f;
            // Count the fire.
            fireCounter_++;
            if (fireCounter_ == CARTRIDGE_CAPACITY_) {
                fireCounter_ = 0;
                // Switch the machine gun to load.
                if (IS_IN_WEAPON_DEV_MODE == false) {
                    this.setFireReady(false);
                }
            }
        }
    }
    
    /**
     * The method updates the control. This should not be called from the user code.
     *
     * @param tpf Time per frame.
     */
    @Override
    public void update(final float tpf) {
        super.update(tpf);
        if (this.isEnabled() == true && nodeFireEffect_ != null) {
            timer_ += tpf;
            if (timer_ > 0f && timer_ < FIRE_EFFECT_MAX_TIME_) {
                for (ParticleEmitter pe_ : effectFire_.getVisualEffectEmitList()) {
                    pe_.emitAllParticles();
                }
                for (AudioNode an_ : effectFire_.getAudioEffectStartList()) {
                    an_.play();
                }
            } else {
                timer_ = -0.1f;
                for (ParticleEmitter pe_ : effectFire_.getVisualEffectKillList()) {
                    pe_.killAllParticles();
                }
                for (AudioNode an_ : effectFire_.getAudioEffectStopList()) {
                    an_.stop();
                }
            }
        }
    }
    //
    // *************************************************************************
    //
    /**
     * The method creates a clone of the control, the given spatial is the
     * cloned version of the spatial to which this control is attached to.
     *
     * @param spatial The spatial.
     *
     * @return Control A clone of this control for the spatial.
     */
    @Override
    public Control cloneForSpatial(final Spatial spatial) {
        KwMG34 control_ = new KwMG34(modelMGfpkt_);
//        control_.setAngularFactor(getAngularFactor());
//        control_.setAngularSleepingThreshold(getAngularSleepingThreshold());
//        control_.setCcdMotionThreshold(getCcdMotionThreshold());
//        control_.setCcdSweptSphereRadius(getCcdSweptSphereRadius());
//        control_.setCollideWithGroups(getCollideWithGroups());
//        control_.setCollisionGroup(getCollisionGroup());
//        control_.setDamping(getLinearDamping(), getAngularDamping());
//        control_.setFriction(getFriction());
//        control_.setGravity(getGravity());
//        control_.setKinematic(isKinematic());
//        control_.setKinematicSpatial(isKinematicSpatial());
//        control_.setLinearSleepingThreshold(getLinearSleepingThreshold());
//        control_.setPhysicsLocation(getPhysicsLocation(null));
//        control_.setPhysicsRotation(getPhysicsRotationMatrix(null));
//        control_.setRestitution(getRestitution());
//        if (mass > 0) {
//            control_.setAngularVelocity(getAngularVelocity());
//            control_.setLinearVelocity(getLinearVelocity());
//        }
//        control_.setApplyPhysicsLocal(isApplyPhysicsLocal());
        return control_;
    }

    /**
     * The method performs a regular shallow clone of the object. Some fields
     * may also be cloned but generally only if they will never be shared with
     * other objects. (For example, local Vector3fs and so on.)
     * <p>
     * <p>
     * This method is separate from the regular clone() method so that objects
     * might still maintain their own regular java clone() semantics (perhaps
     * even using Cloner for those methods). However, because Java's clone() has
     * specific features in the sense of Object's clone() implementation, it's
     * usually best to have some path for subclasses to bypass the public
     * clone() method that might be cloning fields and instead get at the
     * superclass protected clone() methods. For example, through
     * super.jmeClone() or another protected clone method that some base class
     * eventually calls super.clone() in.</p>
     *
     * @return Object The clone of the object.
     */
    @Override
    public Object jmeClone() {
        KwMG34 control_ = new KwMG34(modelMGfpkt_);
//        control_.setAngularFactor(getAngularFactor());
//        control_.setAngularSleepingThreshold(getAngularSleepingThreshold());
//        control_.setCcdMotionThreshold(getCcdMotionThreshold());
//        control_.setCcdSweptSphereRadius(getCcdSweptSphereRadius());
//        control_.setCollideWithGroups(getCollideWithGroups());
//        control_.setCollisionGroup(getCollisionGroup());
//        control_.setDamping(getLinearDamping(), getAngularDamping());
//        control_.setFriction(getFriction());
//        control_.setGravity(getGravity());
//        control_.setKinematic(isKinematic());
//        control_.setKinematicSpatial(isKinematicSpatial());
//        control_.setLinearSleepingThreshold(getLinearSleepingThreshold());
//        control_.setPhysicsLocation(getPhysicsLocation(null));
//        control_.setPhysicsRotation(getPhysicsRotationMatrix(null));
//        control_.setRestitution(getRestitution());
//        if (mass > 0) {
//            control_.setAngularVelocity(getAngularVelocity());
//            control_.setLinearVelocity(getLinearVelocity());
//        }
//        control_.setApplyPhysicsLocal(isApplyPhysicsLocal());
//        control_.spatial_ = this.spatial_;
//        control_.setEnabled(isEnabled());
        return control_;
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
