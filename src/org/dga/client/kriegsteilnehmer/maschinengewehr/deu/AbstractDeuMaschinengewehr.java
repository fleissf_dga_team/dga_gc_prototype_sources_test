/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsteilnehmer.maschinengewehr.deu;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioData;
import com.jme3.audio.AudioNode;
import org.dga.client.kombattant.infanterie.handwaffe.maschinengewehr.AbstractMaschinengewehr;
import org.dga.client.kombattant.infanterie.handwaffe.maschinengewehr.MaschinengewehrModel;
import org.dga.client.effekt.DefaultFireEffect;
import org.dga.client.effekt.FireEffect;

/**
 * The abstract implementation of german machine guns.
 *
 * @extends AbstractMaschinengewehr
 * @implements DeuMaschinengewehr
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 7.5.2018
 */
public abstract class AbstractDeuMaschinengewehr extends AbstractMaschinengewehr
    implements DeuMaschinengewehr {
    private AssetManager assetManager_ = null;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new control with the supplied properties.
     * 
     * @param maschinengewehrModel The machine gun's model.
     */
    public AbstractDeuMaschinengewehr(final MaschinengewehrModel maschinengewehrModel) {
        super(maschinengewehrModel);
        assetManager_ = maschinengewehrModel.getAssetManager();
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns an effect provided by the shooting armament during a shot.
     * 
     * @return FireEffect An effect provided by the shooting armament during a shot.
     */
    @Override
    public FireEffect getFireEffect() {
        return DefaultFireEffect.newBuilder(assetManager_)
            
//            .addFlame(true, 1).setFlameStartSize(0.02f).setFlameEndSize(0.04f)
//            .setFlameEmitterRadius(0.1f).setFlameLowLife(0.02f).setFlameHighLife(0.04f)
            
//            .addFlash(true, 1).setFlashStartSize(0.02f).setFlashEndSize(0.04f)
//            .setFlashEmitterRadius(0.1f).setFlashLowLife(0.02f).setFlashHighLife(0.04f)
            
            .addSmoke(false, 8).setSmokeStartSize(0.2f).setSmokeEndSize(0.4f)
            .setSmokeEmitterRadius(0.4f).setSmokeLowLife(0.1f).setSmokeHighLife(0.2f)
            
            .addSound(
                new AudioNode(
                    assetManager_,
                    "Models/Kriegsteilnehmer/Maschinengewehr/DEU/Sounds/GermanMachineGunShot.wav",
                    AudioData.DataType.Buffer))
            .build();
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
