/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client;

import com.jme3.input.KeyInput;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.Trigger;

/**
 * The control bindings for the overall game client.
 *
 * @param <T> The type of a game control trigger.
 * @extends java.lang.Object
 * @implements JmeControlBinding
 * @pattern Adapter
 * @pattern Chain of Responsibility
 * @pattern Typesafe Enum
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 30.4.2018
 */
public class DefaultDgaJmeControlBinding<T extends Trigger> extends Object implements DgaJmeControlBinding {
    //
    // Keyboard controls
    //
    /**
     * Exit the game.
     */
    public static final DefaultDgaJmeControlBinding EXIT_GAME_KEY = 
        new DefaultDgaJmeControlBinding("ExitGame_Key", new KeyTrigger(KeyInput.KEY_ESCAPE));
    /**
     * Enable X-Ray testing.
     */
    public static final DefaultDgaJmeControlBinding X_RAY_KEY = 
        new DefaultDgaJmeControlBinding("XRay_Key", new KeyTrigger(KeyInput.KEY_F10));
    //
    // Mouse controls
    //
    
    //
    private String name_ = null;
    private T trigger_ = null;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The default constructor.
     * 
     * @param name The name of a binding.
     * @param trigger The trigger of a binding.
     */
    protected DefaultDgaJmeControlBinding(final String name, final T trigger) {
        name_ = name;
        trigger_ = trigger;
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the binding's name.
     * 
     * @return String The binding's name.
     */
    @Override
    public String getName() {
        return name_;
    }
    
    /**
     * The method returns the binding's trigger.
     * 
     * @return T The binding's trigger.
     */
    @Override
    public T getTrigger() {
        return trigger_;
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
